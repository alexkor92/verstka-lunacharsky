<?php

namespace Application\Sonata\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;


/**
 * Class RegistrationController
 */
class RegistrationController extends BaseController
{

    public function registerAction()
    {
        $response = parent::registerAction();
      
        return $response;
    }
}
