<?php

namespace Application\Sonata\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\ChangePasswordController as BaseController;


/**
 * Class RegistrationController
 */
class ChangePasswordController extends BaseController
{

    public function changePasswordAction()
    {
        $response = parent::changePasswordAction();
      
        return $response;
    }
}
