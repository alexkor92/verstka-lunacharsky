<?php
/**
 * Created by Michael A. Sivolobov
 * Date: 15.03.13
 */

namespace Luny\PartnerBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{
    const ROLE_MANAGER = 'ROLE_MANAGER';
    const ROLE_PARTNER = 'ROLE_PARTNER';

    const TYPE_MANAGER = 'manager';
    const TYPE_PARTNER = 'partner';
    const TYPE_ADMIN   = 'admin';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     *
     * @Assert\NotBlank(message="user.notblank", groups={"Registration", "Profile"})
     * @Assert\Length(min="3", max="255", minMessage="user.tooshort", maxMessage="user.toolong", groups={"Registration", "Profile"})
     *
     * @var string Полное имя пользователя
     */
    protected $name;

    /**
     * @ORM\Column(name="contacts", type="string", nullable=true)
     * @var string Контактная информация
     */
    protected $contacts;


    /**
     * @ORM\Column(name="settings", type="array")
     * @var array
     */
    protected $settings = array(
        'price_discount' => 20,
    );


    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="\Luny\PartnerBundle\Entity\Website", mappedBy="user", cascade={"all"})
     * @ORM\JoinColumn(name="partner_id", referencedColumnName="id", nullable=false)
     */
    protected $websites;



    public function __construct()
    {
        parent::__construct();
        $this->ordersRelatedByManagerId = new ArrayCollection();
        $this->ordersRelatedByPartnerId = new ArrayCollection();
        $this->rewardRequests = new ArrayCollection();
        $this->websites = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getSalt() {
     return 'sugar';
    }
    /**
     * Set usertype
     *
     * @param string $usertype
     * @return User
     */
    public function setUsertype($usertype)
    {
        $this->usertype = $usertype;

        return $this;
    }

    /**
     * Get usertype
     *
     * @return string
     */
    public function getUsertype()
    {
        return $this->usertype;
    }

    /**
     * Set settings
     *
     * @param array $settings
     * @return User
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * Get settings
     *
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

  

    /**
     * Add website
     *
     * @param \Luny\PartnerBundle\Entity\Partner\Website $website
     * @return User
     */
    public function addWebsite(\Luny\PartnerBundle\Entity\Website $website)
    {
        $website->setPartner($this);
        $this->websites[] = $website;

        return $this;
    }

    /**
     * Remove websites
     *
     * @param \Luny\PartnerBundle\Entity\Partner\Website $websites
     */
    public function removeWebsite(\Luny\PartnerBundle\Entity\Website $websites)
    {
        $this->websites->removeElement($websites);
    }

    /**
     * Get websites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWebsites()
    {
        return $this->websites;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateUserType()
    {
        if (!$this->hasRole(self::ROLE_SUPER_ADMIN)) {
            $this->roles = array();
        }
        if ($this->usertype == self::TYPE_PARTNER) {
            $this->addRole(self::ROLE_PARTNER);
        } elseif ($this->usertype == self::TYPE_MANAGER) {
            $this->addRole(self::ROLE_MANAGER);
        }
    }

    /**
     * @param string $role
     * @return string
     */
    public static function typeFromRole($role)
    {
        if ($role == static::ROLE_SUPER_ADMIN) {
            $role = self::TYPE_ADMIN;
        } else if ($role == static::ROLE_MANAGER) {
            $role = self::TYPE_MANAGER;
        } else {
            $role = self::TYPE_PARTNER;
        }
        return $role;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set contacts
     *
     * @param string $contacts
     * @return User
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * Get contacts
     *
     * @return string
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @return int
     */
    public function getDiscountCoefficient()
    {
        return $this->settings['discount_coefficient'];
    }

    public function setDiscountCoefficient($coefficient)
    {
        $this->settings['discount_coefficient'] = $coefficient;
        return $this;
    }

    /**
     * Имеет ли пользователь хоть один website
     */
    public function hasWebsites() {
        return !$this->websites->isEmpty();
    }

}
