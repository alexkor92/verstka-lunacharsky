<?php

namespace Luny\PartnerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Luny\PartnerBundle\Entity\Message
 *
 * @ORM\Entity
 * @ORM\Table(name="social_person")
 */
class SocPerson
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var Integer VK ID автора сообщения
     * @ORM\Column(type="bigint", length=25, unique=true)
     */
    protected $user_id; 
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $fname;    
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $lname;    
    
    /**
     * @var String path to avatar
     * @ORM\Column(type="string")
     */
    protected $avatar;        

    /**
     * VK или FB
     * 
     * @ORM\Column(type="soc")
     */
    protected $type;
    
// связи у сущностей SocPerson и Messages реализовывать не стал, т.к. сначала я сохраняю в базу сообщения, 
// а потом уже ищу IDs недобавленные в SocPerson, при наличии свзязи мне бы пришлось передавать в setAuthor уже созданный объект для ID,
// что во-первых приведет к доп. запросам к БД (предварительная выборка объектов по ID), 
// а во-вторых (и это стало основной причиной отказа от явной связи), чтобы создать несуществующих ещё объект ID,
// надо делать запрос к api соц. сети, чтобы получить информацию об авторе сообщения
//    /**
//     * @ORM\OneToMany(targetEntity="Message", mappedBy="author", cascade={"all"})
//     * @ORM\JoinColumn(name="author", referencedColumnName="id")
//     */
//    protected $messages;        
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return SocPerson
     */
    public function setFname($fname)
    {
        $this->fname = $fname;

        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return SocPerson
     */
    public function setLname($lname)
    {
        $this->lname = $lname;

        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return SocPerson
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return SocPerson
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add messages
     *
     * @param \Luny\PartnerBundle\Entity\Message $messages
     * @return SocPerson
     */
    public function addMessage(\Luny\PartnerBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Luny\PartnerBundle\Entity\Message $messages
     */
    public function removeMessage(\Luny\PartnerBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return SocPerson
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string 
     */
    public function getAvatar()
    {
        return $this->avatar;
    }
}
