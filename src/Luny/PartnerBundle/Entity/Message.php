<?php

namespace Luny\PartnerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Luny\PartnerBundle\Entity\Message
 *
 * @ORM\Entity
 * @ORM\Table(name="social_message")
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

//    /**
//     * @ORM\Column(type="string", length=50)
//     */
//    protected $fname;    
//    
//    /**
//     * @ORM\Column(type="string", length=50)
//     */
//    protected $lname;    
    
    /**
     * ORM\ManyToOne(targetEntity="SocPerson", inversedBy="messages")
     * ORM\JoinColumn(name="author", referencedColumnName="id", nullable=false)
     * @ORM\Column(type="bigint", length=25)
     */
    protected $author;    

    /**
     * @var Integer ID диалога, обычно совпадает с user_id
     * @ORM\Column(type="bigint", length=25, nullable=true)
     */
    protected $chat_id;     
    
    /**
     * @ORM\Column(type="text")
     */
    protected $text;

    /**
     * @ORM\Column(type="bool", nullable=true)
     */
    protected $read_state;

    /**
     * @ORM\ManyToOne(targetEntity="Website", inversedBy="messages")
     * @ORM\JoinColumn(name="website", referencedColumnName="id", nullable=false)
     */
    protected $website;
    
    /**
     * @ORM\Column(type="soc")
     */
    protected $social;
    
    /**
     * @var Integer unixttime время ответа (преобразовывать в "human-дату" особого смысла нет, т.к. используется лишь для ORDER BY)
     * @ORM\Column(type="integer")
     */
    protected $time;
    
    public function __construct() 
    {
        $this->time = time();
    }
  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return Message
     */
    public function setFname($fname)
    {
        $this->fname = $fname;

        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return Message
     */
    public function setLname($lname)
    {
        $this->lname = $lname;

        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set author
     *
     * @param integer $author
     * @return Message
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return integer 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set time
     *
     * @param Integer $time
     * @return Message
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return Integer 
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return Message
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string 
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set website
     *
     * @param \Luny\PartnerBundle\Entity\Website $website
     * @return Message
     */
    public function setWebsite(\Luny\PartnerBundle\Entity\Website $website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return \Luny\PartnerBundle\Entity\Website 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set chat_id
     *
     * @param integer $chatId
     * @return Message
     */
    public function setChatId($chatId)
    {
        $this->chat_id = $chatId;

        return $this;
    }

    /**
     * Get chat_id
     *
     * @return integer 
     */
    public function getChatId()
    {
        return $this->chat_id;
    }

    /**
     * Set read_state
     *
     * @param bool $readState
     * @return Message
     */
    public function setReadState($readState)
    {
        $this->read_state = $readState;

        return $this;
    }

    /**
     * Get read_state
     *
     * @return bool 
     */
    public function getReadState()
    {
        return $this->read_state;
    }

    /**
     * Set social
     *
     * @param string $social
     * @return Message
     */
    public function setSocial($social)
    {
        $this->social = $social;

        return $this;
    }

    /**
     * Get social
     *
     * @return string 
     */
    public function getSocial()
    {
        return $this->social;
    }
}
