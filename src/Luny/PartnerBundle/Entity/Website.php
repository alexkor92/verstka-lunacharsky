<?php

namespace Luny\PartnerBundle\Entity; // удалил \Partner

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Luny\PartnerBundle\Entity\Website
 *
 * @ORM\Entity
 * @ORM\Table(name="website")
 */
class Website
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    protected $domain;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $success_url;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $fail_url;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $result_url;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="websites")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $partner;

    // bool -> integer , doctrine ругается
    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var boolean Пользуется ли сайт нашей прокси-платежной системой
     */
    protected $payment_service = false;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @var integer использовать ли vk_api? 0 - нет, 1 - да
     */
    protected $vk_use;    
    

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @var integer использовать ли fb_api? 0 - нет, 1 - да
     */
    protected $fb_use;  
    
    /**
     * @ORM\Column(type="integer", length=25, options={"unsigned"=true})
     * @var integer ID группы Вконтакте
     */
    protected $vk_group;

    /**
     * @ORM\Column(type="string", length=50, options={"unsigned"=true})
     * @var integer ID группы на facebook
     */
    protected $fb_group;
    
    /**
     * @ORM\Column(type="string", length=50)
     * @var string email или номер телефона для авторизации Вконтакте
     */
    protected $vk_login;
    
    /**
     * @ORM\Column(type="string", length=50)
     * @var string пароль для авторизации Вконтакте
     */
    protected $vk_pass;
    
    /**
     * @ORM\Column(type="string", length=50)
     * @var string логин для авторизации на facebook
     */
    protected $fb_login;
    
    /**
     * @ORM\Column(type="string", length=50)
     * @var string пароль для авторизации на facebook
     */
    protected $fb_pass;

    /**
     * @ORM\Column(type="string")
     * @var string facebook access user token
     */
    protected $fb_token;    
    
    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="website", cascade={"all"})
     * @ORM\JoinColumn(name="domain", referencedColumnName="id")
     */
    protected $categories;
 
    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="website", cascade={"all"})
     * @ORM\JoinColumn(name="website", referencedColumnName="id")
     */
    protected $products;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="website", cascade={"all"})
     * @ORM\JoinColumn(name="website", referencedColumnName="id")
     */
    protected $messages;    
    
    public function __construct()
    {
       // $this->orders = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Luny\PartnerBundle\Entity\Partner\Website
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of domain.
     *
     * @param string $domain
     * @return \Luny\PartnerBundle\Entity\Partner\Website
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get the value of domain.
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set the value of success_url.
     *
     * @param string $success_url
     * @return \Luny\PartnerBundle\Entity\Partner\Website
     */
    public function setSuccessUrl($success_url)
    {
        $this->success_url = $success_url;

        return $this;
    }

    /**
     * Get the value of success_url.
     *
     * @return string
     */
    public function getSuccessUrl()
    {
        return $this->success_url;
    }

    /**
     * Set the value of fail_url.
     *
     * @param string $fail_url
     * @return \Luny\PartnerBundle\Entity\Partner\Website
     */
    public function setFailUrl($fail_url)
    {
        $this->fail_url = $fail_url;

        return $this;
    }

    /**
     * Get the value of fail_url.
     *
     * @return string
     */
    public function getFailUrl()
    {
        return $this->fail_url;
    }

    /**
     * Set the value of result_url.
     *
     * @param string $result_url
     * @return \Luny\PartnerBundle\Entity\Partner\Website
     */
    public function setResultUrl($result_url)
    {
        $this->result_url = $result_url;

        return $this;
    }
    
    /**
     * Get the value of result_url.
     *
     * @return string
     */
    public function getResultUrl()
    {
        return $this->result_url;
    }


    /**
     * Set User entity (many to one).
     *
     * @param \Luny\UserBundle\Entity\User $partner
     * @return \Luny\PartnerBundle\Entity\Partner\Website
     */
    public function setPartner(User $partner = null)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * Get User entity (many to one).
     *
     * @return \Luny\UserBundle\Entity\User
     */
    public function getPartner()
    {
        return $this->partner;
    }

    public function setVkGroup($vk_group)
    {
        $this->vk_group = $vk_group;
    }
    
    public function getVkGroup()
    {
        return $this->vk_group;
    }
 
    public function getVkLogin() 
    {
        return $this->vk_login;
    }

    public function getVkPass() 
    {
        return $this->vk_pass;
    }

    public function getFbLogin() 
    {
        return $this->fb_login;
    }

    public function getFbPass() 
    {
        return $this->fb_pass;
    }
    
    public function getFbGroup() 
    {
        return "$this->fb_group";
    }    

    public function setVkLogin($vk_login)
    {
        $this->vk_login = $vk_login;
    }

    public function setVkPass($vk_pass) 
    {
        $this->vk_pass = $vk_pass;
    }

    public function setFbLogin($fb_login) 
    {
        $this->fb_login = $fb_login;
    }

    public function setFbPass($fb_pass) 
    {
        $this->fb_pass = $fb_pass;
    }
    
    public function getVkUse()  
    {
        if ($this->vk_use == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getFbUse()  
    {
        if ($this->fb_use == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function setVkUse($vk_use)  
    {
        $this->vk_use = $vk_use;
    }

    public function getFbToken()  
    {
        return $this->fb_token;
    }

    public function setFbToken($fb_token)  
    {
        $this->fb_token = $fb_token;
    }
    
    public function setFbUse($fb_use) 
    {
        $this->fb_use = $fb_use;
    }

        
    public function __sleep()
    {
        return array('id', 'partner_id', 'domain', 'success_url', 'fail_url', 'result_url');
    }


    public function __toString()
    {
        return $this->domain;
    }


    /**
     * Set fb_group
     *
     * @param integer $fbGroup
     * @return Website
     */
    public function setFbGroup($fbGroup)
    {
        $this->fb_group = $fbGroup;

        return $this;
    }

    /**
     * Add categories
     *
     * @param \Luny\PartnerBundle\Entity\Category $categories
     * @return Website
     */
    public function addCategory(\Luny\PartnerBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Luny\PartnerBundle\Entity\Category $categories
     */
    public function removeCategory(\Luny\PartnerBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set payment_service
     *
     * @param integer $paymentService
     * @return Website
     */
    public function setPaymentService($paymentService)
    {
        $this->payment_service = $paymentService;

        return $this;
    }

    /**
     * Get payment_service
     *
     * @return integer 
     */
    public function getPaymentService()
    {
        return $this->payment_service;
    }

    /**
     * Add products
     *
     * @param \Luny\PartnerBundle\Entity\Product $products
     * @return Website
     */
    public function addProduct(\Luny\PartnerBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \Luny\PartnerBundle\Entity\Product $products
     */
    public function removeProduct(\Luny\PartnerBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add messages
     *
     * @param \Luny\PartnerBundle\Entity\Message $messages
     * @return Website
     */
    public function addMessage(\Luny\PartnerBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Luny\PartnerBundle\Entity\Message $messages
     */
    public function removeMessage(\Luny\PartnerBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
