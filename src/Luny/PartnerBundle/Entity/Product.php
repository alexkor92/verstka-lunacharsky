<?php

namespace Luny\PartnerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity\Product
 *
 * @ORM\Entity(repositoryClass="ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true})
     */    
    protected $ext_id;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */    
    protected $descr;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true, options={"unsigned"=true})
     */    
    protected $fb_img_id;    

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true}, nullable=true)
     */    
    protected $vk_img_id;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */    
    protected $vk_img_hash;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="Website", inversedBy="products")
     * @ORM\JoinColumn(name="website", referencedColumnName="id", nullable=false)
     */
    protected $website;
    
    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }   
    
    public function getId() 
    {
        return $this->id;
    }

    public function getExtId() 
    {
        return $this->ext_id;
    }
    
    public function getDescr() 
    {
        return $this->descr;
    }

    public function getVkImgId() 
    {
        return $this->vk_img_id;
    }

    public function getVkImgHash() 
    {
        return $this->vk_img_hash;
    }

    public function getCategories() 
    {
        return $this->categories;
    }

    public function setId($id) 
    {
        $this->id = $id;
    }

    public function setExtId($ext_id) 
    {
        $this->ext_id = $ext_id;
    }

    public function setDescr($descr) 
    {
        $this->descr = $descr;
    }

    public function setVkImgId($vk_img_id) 
    {
        $this->vk_img_id = $vk_img_id;
    }

    public function setVkImgHash($vk_img_hash) 
    {
        $this->vk_img_hash = $vk_img_hash;
    }

    public function setCategories($categories) 
    {
        $this->categories = $categories;
    }

    public function __sleep()
    {
        return array('id', 'ext_id', 'descr', 'vk_img_hash', 'vk_img_id');
    }
    

    /**
     * Set category
     *
     * @param \Luny\PartnerBundle\Entity\Category $category
     * @return Product
     */
    public function setCategory(\Luny\PartnerBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Luny\PartnerBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set fb_img_id
     *
     * @param integer $fbImgId
     * @return Product
     */
    public function setFbImgId($fbImgId)
    {
        $this->fb_img_id = $fbImgId;

        return $this;
    }

    /**
     * Get fb_img_id
     *
     * @return string 
     */
    public function getFbImgId()
    {
        return "$this->fb_img_id";
    }

    /**
     * Set website
     *
     * @param \Luny\PartnerBundle\Entity\Website $website
     * @return Product
     */
    public function setWebsite(\Luny\PartnerBundle\Entity\Website $website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return \Luny\PartnerBundle\Entity\Website 
     */
    public function getWebsite()
    {
        return $this->website;
    }
}
