<?php

namespace Luny\PartnerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity\Category
 *
 * @ORM\Entity(repositoryClass="CategoryRepository")
 * @ORM\Table(name="category")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $ext_id;

    /**
     * @ORM\Column(type="integer", length=25, options={"unsigned"=true})
     */
    protected $vk_album;

    /**
     * @ORM\Column(type="string", length=50, options={"unsigned"=true})
     */
    protected $fb_album;
    
    /**
     * ORM\ManyToOne(targetEntity="Website", inversedBy="categories")
     * ORM\JoinColumn(name="website_domain", referencedColumnName="id", nullable=false)
     */
    /**
     * @ORM\ManyToOne(targetEntity="Website", inversedBy="categories")
     * @ORM\JoinColumn(name="domain", referencedColumnName="id", nullable=false)
     */    
    protected $domain;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="categories")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    protected $products;

    public function __construct()
    {     
        $this->products = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Entity\Page
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    public function getExtId() 
    {
        return $this->ext_id;
    }

    public function getDomain() 
    {
        return $this->domain;
    }

    public function getVkAlbum() 
    {
        return $this->vk_album;
    }

    public function getFbAlbum() 
    {
        return "$this->fb_album";
    }

    public function setExtId($ext_id) 
    {
        $this->ext_id = $ext_id;
    }

    public function setDomain($domain) 
    {
        $this->domain = $domain;
    }

    public function setVkAlbum($vk_album) 
    {
        $this->vk_album = $vk_album;
    }

    public function setFbAlbum($fb_album) 
    {
        $this->fb_album = $fb_album;
    }

      

    public function __sleep()
    {
        return array('id', 'ext_id', 'website', 'vk_album', 'fb_album');
    }

    /**
     * Add products
     *
     * @param \Luny\PartnerBundle\Entity\Product $products
     * @return Category
     */
    public function addProduct(\Luny\PartnerBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \Luny\PartnerBundle\Entity\Product $products
     */
    public function removeProduct(\Luny\PartnerBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }
}
