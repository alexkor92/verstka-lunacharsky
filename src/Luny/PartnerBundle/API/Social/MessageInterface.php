<?php

namespace Luny\PartnerBundle\API\Social;

interface MessageInterface
{
    public function send(Array $sendParams);
    public function show(Array $showParams);
}