<?php

namespace Luny\PartnerBundle\API\Social;

interface MainInterface
{
    public function photo();
    public function message();
    public function person();
}