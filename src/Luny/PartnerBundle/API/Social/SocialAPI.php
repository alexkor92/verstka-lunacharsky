<?php

namespace Luny\PartnerBundle\API\Social;


use Luny\PartnerBundle\Exception\ApiException;
use Luny\PartnerBundle\API\Social\VK\MainVkAPI;
use Luny\PartnerBundle\API\Social\FB\MainFbAPI;

ini_set('max_execution_time', 180);


/**
 * Класс, представляющий сервис для работы с соц. сетями
 * Общая схема работы с API следующая:
 * перед взаимодействием с соц. сетями надо с помощью метода setParams() установить параметры;
 * после установки параметров, можно обращаться к соц.сетям, с помощью методов, 
 * заименованных основными сущностями характерные для соц. сетей (photo, message и т.п.),
 * методы имеют общую структуру сигнатуры - entity($action, array $parameters),
 * где $action - строка, в которой описано действие над сущностью entity (например: add, delete и т.п.), $parameters - массив параметров, 
 * характерных и необходимых именно для действия $action над сущностью entity.
 * Подробнее о существующих сущностях и действиях над ними смотрите: ... TODO: список сущностей и действий взять из интерфейсов...
 */
class SocialAPI  {

    /**
     * @var Array массив, перечисляющий какие из соц. сетей надо использовать и параметры к ним
     * пример: array('vk' => array('login' => '{номер телефона}', 'password' => 'abrakadabra', 'group' => '{ID_группы}'))
     */
    protected $params;

    /**
     * @var Array массив, содержащий объекты используемых соц. сетей
     */
    protected $socials;
    
    protected $cache_dir;

    /**
     * @var Boolean Флаг установлены ли параметры, true если вызывался метод setParams()
     */
    private $parFlag = false;
    

    public function __construct($container)
    {
        $this->cache_dir = $container->getParameter('kernel.cache_dir');

    }
    
    
    
    /**
     * Возвращает массив данных, полученных от закпросов к API-классам соц. сетей.
     * Например: array('fb' =>  array ('img_id' => 1488610518018880), 'vk' => array ('img_id' => 328648654, 'hash' => 4416d530125555dab7))
     * 
     * @param String $method вызываемый метод-сущность
     * @param Array $arguments первый (индекс 0) элемент массива - действие над сущностью, второй элемент (индекс 1) массив аргументов,
     * передаваемые действию
     * @return Array ассоциативный массив, ключи которого характеризуют соц. сеть, API которой вернула значения, занесееные в массив, 
     * который здесь содержится в качестве значения.
     */
    public function __call($method, array $arguments) 
    {
        if ($this->parFlag === false) {
                throw new ApiException('usage', 'It is necessary to call setParams() before the request to API');
        }
        
        if (!isset($arguments[0])) {
                throw new ApiException('usage', 'The entity must have the action method');
        }
        $action = $arguments[0];
        
        if (isset($arguments[1])) {
            $args = $arguments[1];
        } else {
            $args = array();
        }

        // ниже комментарий - "мысли вслух"...
        /*
          надо вызвать:
          photoAdd
          messageSend
          ...
          дано: объекты MainVkAPI, MainFbAPI
          в них методы photo, message, person
         
         реализация:
         $soc->photo('add', array());
        */

        /*
        общие параметры?
         */
        $res = array();
        foreach ($this->socials as $socObj) {
           $apiEntity = $socObj->$method();
         //$apiEntity->call_user_func_array($action, $args);
            $res[(string)$socObj] = $apiEntity->$action($args);
       }
       return $res;
    }

    public function setParams($params)
    {
        
        if (empty($params)) {
                throw new ApiException('usage', 'Parameters cannot be blank');
        }        
        
        $this->params = $params;
        
        $socials = array();
        
        foreach ($params as $soc => $pars) {
            /* pars: для авторизации и т.п. */
            
            $cookie_dir = $this->cache_dir.'/SocilalAPI/'.$soc.'/cookies';
            $file_dir = $this->cache_dir.'/SocilalAPI/'.$soc.'/temp_files';
            
            $this->createPath($cookie_dir);
            $this->createPath($file_dir);
            
           $pars['cookie_dir'] = $cookie_dir;
           $pars['file_dir'] = $file_dir;
            
           if ($soc == 'vk') {$socials[] = new MainVkAPI($pars);}
           if ($soc == 'fb') {$socials[] = new MainFbAPI($pars);}
            
        }

        $this->socials = $socials;
        $this->parFlag = true;
        
        return true;
    }
        
    private function createPath($path) 
    {
        if (is_dir($path)) return true;
        
        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
        $return = $this->createPath($prev_path);
        
        return ($return && is_writable($prev_path)) ? mkdir($path) : false;
    }
    
}