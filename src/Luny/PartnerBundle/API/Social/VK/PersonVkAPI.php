<?php

namespace Luny\PartnerBundle\API\Social\VK;

use Luny\PartnerBundle\API\Social\PersonInterface;
use Luny\PartnerBundle\Exception\ApiException;


class PersonVkAPI implements PersonInterface {
    
    protected $mid;

    /**
     * @var String ID of VK application "Luny Service" (type must be string...) string пока под вопросом
     */
    //private $appId = '4257347'; // другое приложения
    private $appId = '4264008'; 
    
    /**
     * @var String VK application secret key
     */
    //private $appSecret = 'RFzyMLDuiltEXspwk4gE';  // другое приложения
    private $appSecret = 'sqEzMwTNyQnbcgAJFOuc‏';    
    
    /**
     * @var Float VK API version
     */
    private $appVers = 5.15;    
    
    private $appToken;    
    
    public function __construct($mid, $appToken) 
    {
        $this->mid = $mid;
        $this->appToken = $appToken;
        
    }
    


    /**
     * 
     * @param mixed $users массив из ID юзеров (или Integer, если для одного ID), данные которых необходимо вернуть методу
     * @return json
     */
    public function getInfo($users) 
    {
        if (empty($this->appToken)) {$this->setAppToken();}         
        if (is_array($users)) {$users = implode(',', $users);}
        
        $result = json_decode(
                    file_get_contents('https://api.vk.com/method/users.get?v='.$this->appVers
                                     .'&access_token='.$this->appToken
                                     .'&user_ids='.$users
                                     .'&fields=photo_50'
                                    )
                   , true);

/*
array (size=1)
  'response' => 
    array (size=1)
      0 => 
        array (size=4)
          'id' => int 242587873
          'first_name' => string 'аЁбаАб' (length=8)
          'last_name' => string 'аЂаЕббаИаЛаА' (length=14)
          'photo_50' => string 'http://cs608624.vk.me/v608624873/58b9/ggeRPejKBvU.jpg' (length=53)        
*/
        $info['id'] = $result['response'][0]['id'];
        $info['fname'] = $result['response'][0]['first_name'];
        $info['lname'] = $result['response'][0]['last_name'];
        $info['avatar'] = $result['response'][0]['photo_50'];
        
        return $info;
    }

    public function adminInfo() 
    {
        return $this->getInfo($this->mid);
    }
    
}