<?php


namespace Luny\PartnerBundle\API\Social\VK;


use Luny\PartnerBundle\API\Social\PhotoInterface;
use Luny\PartnerBundle\Exception\ApiException;

class PhotoVkAPI implements PhotoInterface {
    
    /**
     * @var String url of image 
     */
    protected $upload_url;
     
    /**
     * @var Integer ID of user
     */
    protected $mid;

    /**
     * @var Integer ID of group
     */
    protected $gid;
    
    /**
     * @var Integer ID of photo-album
     */
    protected $album;

    protected $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36';
    
    /**
     * @var String path to the temporary file 
     */
    protected $file;
        
    private $file_dir;
    
    private $cookie_dir;
    
    
    public function __construct($login, $mid, $gid, $cookie_dir, $file_dir) 
    {
        $this->login = $login;
        $this->mid = $mid;
        $this->gid = $gid;
        $this->file_dir = $file_dir;        
        $this->cookie_dir = $cookie_dir;        
//        $this->album = $albumParams['album'];
//        $this->mid = $albumParams['mid'];
    }


    /**
     * @param String $photo_url image address
     * @param String $descr description of image on the VK-page
     * @return Array img_id, hash => ID and hash of loaded image
     */
    public function add($photoParams) 
    {
        $descr = isset($photoParams['descr']) ? $photoParams['descr'] : null; 
        //$this->gid = $photoParams['vk_gid'];
        $this->album = $photoParams['vk_album'];
       // $this->mid = $photoParams['vk_mid'];
        $photo_url = $photoParams['photo_url'];
        
//        if (exif_imagetype($photo_url) === false) { 
//            throw new ApiException('url', 'Incorrect image url');
//        }
        
        $this->prepare($photo_url);
        
        $post_part = array( 
                    'oid' => '-'.$this->gid,
                    'aid' => $this->album,
                    'gid' => $this->gid,
                    'mid' => $this->mid,
                    'act' => 'do_add',
                    'ajx' => '1',
                    'photo' => '@'.$this->file
        );
        
        $hash = $this->addParam();
        $post = array_merge($post_part, $hash);
        
        /*
         извлекаем host для headers из URLa для загрузки фото
         Пример: из http://cs123456.vk.com/upload.php получаем cs123456.vk.com
         Данная регулярка должна давать нужный результат и в других случаях: 
             другой домен (.me, .cc и т.д.); 
             изменился путь загрузки upload.php на какой-нибудь abraKaDabra/op/up/load123.php;
             другой протокол;
         */
        $host = preg_replace('/[\w\/:]+\/([\d\w\.]+\.vk\.[\w]{2,4})\/[\d\w\.-_\/]*/', '${1}', $this->upload_url);
        $options = array(
                CURLOPT_URL         => $this->upload_url,
                CURLOPT_TIMEOUT     => 120,
                CURLOPT_POST        => 1,
                CURLOPT_POSTFIELDS  => $post,
                CURLOPT_USERAGENT   => $this->user_agent,
                CURLOPT_HTTPHEADER  => array(
                    'Host: '.$host,
                    'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                    'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                    'Keep-Alive: 300',
                    'Connection: keep-alive',
                    'Origin: http://vk.com',
                    'Referer: http://vk.com/album-'.$this->gid.'_'.$this->album
                   ),
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_FOLLOWLOCATION  => true
         );

        $ch = curl_init();
        curl_setopt_array($ch, $options); 
        $json = curl_exec($ch);  
            $ch_error = curl_error($ch);
            if ($ch_error) {
                throw new ApiException('curl', 'CURL error ('.$this->upload_url.'): '.$ch_error);
            }
        curl_close($ch);
       
        $inf = $this->save($json);

        unset($json);
        // не if isset так как м.б., что descr = ''
        if (!empty($descr)) {
            $this->changeDescr(array('vk_img_id' => $inf['vk_img_id'],
                                     'vk_img_hash' => $inf['vk_img_hash'],
                                     'descr' => $descr,
                                ));
        }
        
        unlink($this->file); // удаляем временный файл
        
        return array('vk_img_id' => $inf['vk_img_id'], 'vk_img_hash' => $inf['vk_img_hash']);        
    }
 
    /**
     * @param Integer $id
     * @param String $hash
     * @param String $descr description of image on the VK-page
     */
    public function changeDescr($descrParams) 
    {
        $descr = isset($descrParams['descr']) ? $descrParams['descr'] : null;       
        $id = $descrParams['vk_img_id'];
        $hash = $descrParams['vk_img_hash'];
       
        
                $post = array(
                            'act' => 'save_desc',
                            'al' => '1',
                            'photo' => '-'.$this->gid.'_'.$id,
                            'hash' => $hash,
                            'text' => $descr
                );

                $options = array(
                        CURLOPT_URL         => 'http://vk.com/al_photos.php',
                        CURLOPT_POST        => 1,
                        CURLOPT_POSTFIELDS  => $post,
                        CURLOPT_USERAGENT   => $this->user_agent,
                        CURLOPT_COOKIEFILE  => $this->cookie_dir.'/'.$this->login.'_cookie.txt',
                        CURLOPT_HTTPHEADER  => array(
                            'Host: vk.com',
                            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                            'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                            'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                            'Connection: keep-alive',
                            'Origin: http://vk.com',
                        ),
                        CURLOPT_RETURNTRANSFER  => true,
                 );

                $ch = curl_init();
                curl_setopt_array($ch, $options);
                curl_exec($ch);  
                    $ch_error = curl_error($ch);
                    if ($ch_error) {
                        throw new ApiException('curl', 'CURL error (http://vk.com/al_photos.php?act=save_desc): '.$ch_error);
                    }                  
                curl_close($ch);
                
    }
    
    
    public function delete($deleteParams/*$id, $hash*/) 
    {
       $id = $deleteParams['vk_img_id'];
       $hash = $deleteParams['vk_img_hash'];
        
       $post = array(
                    'act' => 'delete_photo',
                    'al' => '1',
                    'photo' => '-'.$this->gid.'_'.$id,
                    'hash' => $hash,
                    'set_prev' => '',
                    'sure' => 0
        );

        $options = array(
                CURLOPT_URL         => 'http://vk.com/al_photos.php',
                CURLOPT_POST        => 1,
                CURLOPT_POSTFIELDS  => $post,
                CURLOPT_USERAGENT   => $this->user_agent,
                CURLOPT_COOKIEFILE  => $this->cookie_dir.'/'.$this->login.'_cookie.txt',
                CURLOPT_HTTPHEADER  => array(
                    'Host: vk.com',
                    'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                    'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                    'Connection: keep-alive',
                    'Origin: http://vk.com',
                ),
                CURLOPT_RETURNTRANSFER  => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options); 
        curl_exec($ch);
            $ch_error = curl_error($ch);
            if ($ch_error) {
                throw new ApiException('curl', 'CURL error (http://vk.com/al_photos.php?act=delete_photo): '.$ch_error);
            }        
        curl_close($ch);
    }
    
    private function addParam()
    {
        $options = array(
                CURLOPT_URL             => 'http://vk.com/album-'.$this->gid.'_'.$this->album.'?act=add',
                CURLOPT_USERAGENT       => $this->user_agent,
                CURLOPT_COOKIEFILE      => $this->cookie_dir.'/'.$this->login.'_cookie.txt',
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_FOLLOWLOCATION  => true
         );

        $ch = curl_init();
        curl_setopt_array($ch, $options); 
        $page = curl_exec($ch);
            $ch_error = curl_error($ch);
            if ($ch_error) {
                throw new ApiException('curl', 'CURL error (http://vk.com/album-'.$this->gid.'_'.$this->album.'?act=add): '.$ch_error);
            }         
        curl_close($ch);
        
        /*
         Пример получаемых данных:
         $param[1] - http://cs608623.vk.com/upload.php 
         (символы - и _ в REGEXP-шаблоне для подстраховки при изменении vk.com url загрузки)
         
         хэши, которые надо передать в запросе, иначе ответ от VK будет "Security Breach 2"
         $param[2] - 19ed53b945db68d2c4f3ea7dca89c50f
         $param[3] - 100ef6ebc0005a9625ac77f70986199f
         */               
        preg_match('/checkVars: \{(?:.*?) upload_url: \'([\d\w\/:\.-_]+)\'(?:.*?) hash: \'([A-Fa-f0-9]+)\'(?:.*?) rhash: \'([A-Fa-f0-9]+)\'(?:.*?)\}/s', $page, $param);

        $this->upload_url = $param[1]; 

        /* если хэши длиной не в 32 символа, то скорее всего парсинг выполнился некорректно => записываем в лог */
            if ((strlen($param[2]) != 32) || (strlen($param[3]) != 32)) {
                throw new ApiException('parse', 'Photo hash parsing problem');
            }
            
        unset($page);
         
        return array('hash' => $param[2], 'rhash' => $param[3]);       
    }
    
    private function prepare($url)
    {
      $file = $this->file_dir.'/'.$this->mid.'_'.basename($url);
        $fp = fopen($file, 'w+b');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
            curl_setopt($ch, CURLOPT_TIMEOUT, 120); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_exec($ch);
                $ch_error = curl_error($ch);
                if ($ch_error) {
                    throw new ApiException('curl', 'CURL error ('.$url.'): '.$ch_error);
                }
            curl_close($ch);
        fclose($fp);
      $this->file = $file;
    }
    
    private function save($json)
    {

       /*
        парсинг данных из json: ID-фото вконтакте и его хэша
       */         
            preg_match('/\"photos\":\"(.*?)\",\"hash\":\"([A-Fa-f0-9]+)\"/s', $json, $sec);
                if (!isset($sec[1])) {
                    throw new ApiException('parse', 'Cannot get photos JSON', $json."\n\n".var_export($sec, true));
                }
       /*
         в json-ответе содержится hash и photos, 
         которые необходимо передать в запросе для сохранения фото в альбом
        */
                if (!isset($sec[2])) {
                    throw new ApiException('parse', 'Cannot get photo hash (photoSave)', 
                                           $json."\n\n************\n",var_export($sec, true)
                                          );
                }
      
        /*
         чтобы фото сохранилось в альбом необходимо сделать запрос на vk, 
         который содержал бы в себе json-пакет photos
        */
        $photos = stripslashes($sec[1]);
        /*
         также нам нужен номер сервера, на который загрузилось фото 
         (параметр server в запросе)
        */
        preg_match('/\[\"m\",\"(\d+)\",\"([A-Fa-f0-9]+)\",\"(.*?)\",(?:\d+),(?:\d+)\]/', $photos, $img);
                if (!isset($img[1])) {
                    throw new ApiException('parse', 'Cannot get photos server (photoSave)');
                }
            $post = array(
                        'act' => 'done_add',
                        'aid' => $this->album,
                        'al' => '1',
                        'context' => '1',
                        'from' => 'html5',
                        'geo' => '1',
                        'gid' => $this->gid,
                        'hash' => $sec[2],
                        'mid' => $this->mid,
                        'photos' => $photos,
                        'server' => substr($img[1], 0, 6)
            );

            $options = array(
                    CURLOPT_URL         => 'http://vk.com/al_photos.php',
                    CURLOPT_POST        => 1,
                    CURLOPT_POSTFIELDS  => $post,
                    CURLOPT_USERAGENT   => $this->user_agent,
                    CURLOPT_COOKIEFILE  => $this->cookie_dir.'/'.$this->login.'_cookie.txt',
                    CURLOPT_HTTPHEADER  => array(
                        'Host: vk.com',
                        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                        'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                        'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                        'Connection: keep-alive',
                        'Origin: http://vk.com',
                        'Referer: http://vk.com/album-'.$this->gid.'_'.$this->album
                    ),
                    CURLOPT_RETURNTRANSFER  => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options); 
            $res = curl_exec($ch);
                $ch_error = curl_error($ch);
                if ($ch_error) {
                        throw new ApiException('curl', 'CURL error (http://vk.com/al_photos.php?act=done_add): '.$ch_error);
                }
            curl_close($ch);
            
  
        /*
         json ответ содержит нужную нам информацию
         кроме ID фотки удобно сразу сохранить hash, который также характеризует фото
         при измении описания к фото или её удалении в запрос приходится передавать и ID и этот hash
        */
        preg_match('/photos\.saveDesc\(\'-'.$this->gid.'_([\d]+)\', \'([A-Fa-f0-9]+)\'\)/', $res, $par);
      
        unset($res);
        
        return array('vk_img_id' => $par[1], 'vk_img_hash' => $par[2]);
    }
}