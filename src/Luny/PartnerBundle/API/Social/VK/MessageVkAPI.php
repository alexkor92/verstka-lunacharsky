<?php


namespace Luny\PartnerBundle\API\Social\VK;


use Luny\PartnerBundle\API\Social\MessageInterface;
use Luny\PartnerBundle\Exception\ApiException;

class MessageVkAPI implements MessageInterface {
    
    protected $appToken;

    private $appVers = 5.15; 
    
    public function __construct($token) 
    {
        $this->appToken = $token;
        
    }
    
    /**
     * Получение списка диалогов
     * 
     * @return String json
     */
    public function allDialogs() 
    {
        //if (empty($this->appToken)) {$this->setAppToken();}        
        return file_get_contents('https://api.vk.com/method/messages.getDialogs?v='.$this->appVers.'&access_token='.$this->appToken);
    }
    
    /**
     * Просмотра диалога от пользователя 
     * 
     * @param String $user_id идентификатор пользователя, историю переписки с которым необходимо вернуть (строка, обязательный параметр).
     * @return String json
     */
    public function showDialog($user_id) 
    {
        //if (empty($this->appToken)) {$this->setAppToken();}
        return file_get_contents('https://api.vk.com/method/messages.getHistory?v='.$this->appVers
                                    .'&access_token='.$this->appToken
                                    .'&user_id='.$user_id
                                );
    }    
    
    /**
     * Получение входящих сообщений
     * 
     * @param Integer $time_offset давность получемых сообщений (в секундах), ноль - любая давность
     * @param Integer $offset смещение, необходимое для выборки подмножества сообщений (в кол-ве сообщений)
     * @return String json
     */
    public function show(Array $showParams = array()/*$time_offset = 0, $offset = 0*/) 
    {
        $time_offset = isset($showParams['vk_time_offset']) === false ? 0 : $showParams['vk_time_offset'];
        $offset = isset($showParams['vk_offset']) === false ? 0 : $showParams['vk_offset'];
        
        //if (empty($this->appToken)) {$this->setAppToken();}
        return file_get_contents('https://api.vk.com/method/messages.get?v='.$this->appVers
                                    .'&access_token='.$this->appToken
                                    .'&count=200'
                                    .'&offset='.$offset
                                    .'&time_offset='.$time_offset
                                );        
    }

    /**
     * Отправка сообщений пользователю
     * 
     * @param String $text текст сообщения
     * @param Integer $user_id ID пользователя, которому адресовано сообщение
     * @param Integer $chat_id ID диалога (не обязательный параметр)
     * 
     * @return String json ID добавленного сообщения (response: message_ID)
     */
    public function send(Array $sendParams/*$text, $user_id, $chat_id = null*/) 
    {
        //if (empty($this->appToken)) {$this->setAppToken();}
        $text = isset($sendParams['text']) === false ? 0 : $sendParams['text'];
        $user_id = isset($sendParams['user_id']) === false ? 0 : $sendParams['user_id'];
        $chat_id = isset($sendParams['chat_id']) === false ? null : $sendParams['chat_id'];
        
        
        /*
         Если  {"error_code":7,"error_msg":"Permission to perform this action denied, то это значит что:
         "Вы не можете отправить сообщение этому пользователю, поскольку он ограничивает круг лиц, которые могут присылать ему сообщения."
         */
        return file_get_contents('https://api.vk.com/method/messages.send?v='.$this->appVers
                                    .'&access_token='.$this->appToken
                                    .'&user_id='.$user_id
                                    .'&message='.urlencode($text)
                                );
    }    
}