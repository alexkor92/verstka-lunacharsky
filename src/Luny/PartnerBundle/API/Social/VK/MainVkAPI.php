<?php

namespace Luny\PartnerBundle\API\Social\VK;


use Luny\PartnerBundle\API\Social\MainInterface;
use Luny\PartnerBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Основной класс для работы с соц. сетью vk.com
 * Все типы действий (photo, message, person и т.п.) в VKontakte, осуществляемые через данный сервис, 
 * должны быть описаны в этом классе public методом, который возвращает объект класса, содержащий конечные методы для взаимодействия с VKontakte.
 * Например: мы имеем класс PhotoVkAPI для работы с фотографиями, поэтому здесь в текущем классе объявлен public метод photo(), 
 * который возвращает экземпляр класса PhotoVkAPI, который в свою очередь уже содержит такие методы как add(), delete() и др.
 */
class MainVkAPI implements MainInterface {

    /**
     * @var String login of user on vk
     */
    protected $login;
    
    /**
     * @var Integer ID of user
     */
    protected $mid;

    protected $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36';
    
    /**
     * @var String ID of VK application "Luny Service" (type must be string...) string пока под вопросом
     */
    //private $appId = '4257347';
    private $appId = '4264008'; 
    
    /**
     * @var String VK application secret key
     */
    //private $appSecret = 'RFzyMLDuiltEXspwk4gE';    
    private $appSecret = 'sqEzMwTNyQnbcgAJFOuc‏';    
    
    /**
     * @var Float VK API version
     */
    private $appVers = 5.15;    
    
    private $appToken;    
    
    private $cookie_dir;
    
    private $file_dir;
    
    private $cookie_file_path;


    public function __construct($params) 
    {
        /* TODO: проверка структуры переданного массива и валидация переданных значений */
       $login = $params['login'];
       $password = $params['password'];
       $this->gid = $params['gid'];        
        
       $this->cookie_dir = $params['cookie_dir'];
       $this->file_dir = $params['file_dir'];
       $this->login = $login;
        
       $this->cookie_file_path = $this->cookie_dir.'/'.$this->login.'_cookie.txt';
               
        
        $session_active = $this->authCheck();

        if ($session_active === false) {
             $this->authMake($login, $password);
        }
    }
    
    public function __toString() 
    {
        return 'vk';
    }    
    /**
     * 
     * @param Array $photoParam
     * @return \Luny\PartnerBundle\API\Social\VK\PhotoVkAPI
     */
    public function photo()
    {
       $photo = new PhotoVkAPI($this->login, $this->mid, $this->gid, $this->cookie_dir, $this->file_dir);
       
       return $photo;
    }

    public function message()
    {
        $this->setAppToken();
        $message = new MessageVkAPI($this->appToken);

        return $message;
    }
    
    public function person()
    {
       $this->setAppToken();
       $person = new PersonVkAPI($this->mid, $this->appToken);
       
       return $person;
    }
    
   /**
     * @return boolean
     */
    private function authCheck()
    {
        $options = array(
                CURLOPT_URL             => 'http://vk.com/settings',
                CURLOPT_USERAGENT       => $this->user_agent,
                CURLOPT_COOKIEFILE      => $this->cookie_file_path,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_FOLLOWLOCATION  => true
         );

        $ch = curl_init();
        curl_setopt_array($ch, $options); 
        $page = curl_exec($ch);
            $ch_error = curl_error($ch);
             /*
              закомментировано, так как когда сессия устаревает, то curl выдает ошибку сертификата,
              логирование другой ошибки CURL реализовано чуть ниже
             */
            //            if ($ch_error) {
            //                    throw new ApiException('curl', 'CURL error (http://vk.com/feed): '.$ch_error);
            //            }
        curl_close($ch);
        
        /* 
         всегда ли при устаревшей сессии CURL выдаст ошибку (- не знаю)
         и всегда ли любая ошибка CURL означает, что сессия устарела? (- очевидно, не всегда)
         заодно парсингом смотрим id пользователя, если id не получен, то видимо
         сессия истекла, но может быть это ошибка самого парсинга...
         ВЫВОД: подумать, по возможности поменять логику на более строгую и однозначную
        */
        preg_match('/ href=\"\/audios(\d+)/', $page, $mid);
        unset($page);
            if (!isset($mid[1]) || $ch_error) {
                /* если это не ошибка сертификата, то вероятно проблема не в устаревшей сессии */
                if (strpos($ch_error, 'SSL certificate problem') === false) {
                    throw new ApiException('curl', 'Problem with authCheck (http://vk.com/settings)', $ch_error);
                }
                
                return false; // сессия устарела, надо авторизоваться -> authMake()
            } else {
                $this->mid = $mid[1];
                return true; // сессия активна, снова логиниться не надо
            }
    }

    private function authMake($login, $password)
    {
       $req = new Request(); // Symfony
       
       $post = array(
               'act'          => 'login',
               'q'            => '1',
               'al_frame'     => '1',
               'expire'       => '',
               'captcha_sid'  => '',
               'from_host'    => 'vk.com',
               'from_protocol'=> 'http',
               //'ip_h'         => md5($_SERVER['REMOTE_ADDR']),
               'ip_h'         => md5($req->getClientIp()), // Symfony
               'email'        => $login,
               'pass'         => $password           
       );

      $options = array(
                CURLOPT_URL            => 'https://login.vk.com/?act=login',
                CURLOPT_HEADER         => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERAGENT      => $this->user_agent,
                CURLOPT_SSL_VERIFYPEER => false, 
                CURLOPT_SSL_VERIFYHOST => false,  
                CURLOPT_FOLLOWLOCATION => true,  
                CURLOPT_POST           => true,  
                CURLOPT_POSTFIELDS     => $post, 
                CURLOPT_COOKIEJAR      => $this->cookie_file_path
      );

        $ch = curl_init();
        curl_setopt_array($ch, $options); 
        $page = curl_exec($ch);  
            $ch_error = curl_error($ch);
            if ($ch_error) {
                    throw new ApiException('curl', 'CURL error (https://login.vk.com/?act=login): '.$ch_error);
            }
        curl_close($ch);      
        
        /* 
         если в ответ получили страницу содержащую login.vk.com 
         значит авторизация не прошла, выдадим ошибку
        */
        if (preg_match('/ action=\"https:\/\/login\.vk\.com/', $page)) {
            throw new ApiException('auth', 'VK-authorize error');
        }
              /* Extract the user ID */
              /* l cookie is ID of user */
              preg_match("/Set-Cookie: l=(\d+);/", $page, $cookie); 
              if (!isset($cookie[1])) {
                  throw new ApiException('parse', 'Problem with id cookie parsing', $page);
              }
              $this->mid = $cookie[1];
        
        unset($page);
    }
    
    /**
     * @return String
     */
    protected function setAppToken() 
    {
       $options = array(
                    CURLOPT_URL => 'https://oauth.vk.com/authorize?client_id='.$this->appId
                                    .'&scope=messages&redirect_uri=https://oauth.vk.com/blank.html&response_type=token&v='.$this->appVers,
                    CURLOPT_TIMEOUT     => 120,
                    CURLOPT_HEADER         => true,                
                    CURLOPT_SSL_VERIFYPEER  => false,  
                    CURLOPT_SSL_VERIFYHOST  => false,
                    CURLOPT_COOKIEFILE  => $this->cookie_file_path,
                    CURLOPT_USERAGENT   => 'Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.2.13) ' .
                                                                               'Gecko/20101203 Firefox/3.6.13 ( .NET CLR 3.5.30729)',
                    CURLOPT_HTTPHEADER  => array(
                        'Host: oauth.vk.com',
                        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                        'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                        'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                        'Connection: keep-alive',
                        'Cache-Control: max-age=0',
                       ),
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_FOLLOWLOCATION  => true,
             );

      $ch = curl_init();
      curl_setopt_array($ch, $options); 
      $res = curl_exec($ch);  
      curl_close($ch);
      
      preg_match('/Location: https:\/\/oauth\.vk\.com\/blank\.html#access_token=([A-Fa-f0-9]+)&/is', $res, $match);
     
      if (!empty($match)) {
          $this->appToken = $match[1];
          return $match[1];
      }
      
      preg_match('/location\.href = \"https:\/\/login\.vk\.com\/\?act=(.*?)\"/is', $res, $match);

       $options = array(
                    CURLOPT_URL => 'https://login.vk.com/?act='.$match[1],
                    CURLOPT_TIMEOUT     => 120,
                    CURLOPT_HEADER         => true,                
                    CURLOPT_SSL_VERIFYPEER  => false,  
                    CURLOPT_SSL_VERIFYHOST  => false,
                    CURLOPT_COOKIEFILE  => $this->cookie_file_path,
                    CURLOPT_USERAGENT   => 'Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.2.13) ' .
                                                                               'Gecko/20101203 Firefox/3.6.13 ( .NET CLR 3.5.30729)',
                    CURLOPT_HTTPHEADER  => array(
                        'Host: login.vk.com',
                        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                        'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                        'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                        'Connection: keep-alive',
                        'Cache-Control: max-age=0',
                       ),
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_FOLLOWLOCATION  => true,
             );

      $ch = curl_init();
      curl_setopt_array($ch, $options); 
      $res = curl_exec($ch); 
      curl_close($ch);      
      
      preg_match('/Location: https:\/\/oauth\.vk\.com\/blank\.html#access_token=([A-Fa-f0-9]+)&/is', $res, $match);
     
      if (!empty($match)) {
          $this->appToken = $match[1];
          return $match[1];
      } else {
            throw new ApiException('parse', "Parse error in setAppToken() method :\n\n".var_export($res, true)); 
      }
    }    
    
}
