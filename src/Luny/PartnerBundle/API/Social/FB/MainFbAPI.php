<?php

namespace Luny\PartnerBundle\API\Social\FB;


use Luny\PartnerBundle\API\Social\MainInterface;
use Luny\PartnerBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\Request;
use Luny\PartnerBundle\ApiFb\Facebook;

/**
 * Основной класс для работы с соц. сетью facebook.com
 * Все типы действий (photo, message, person и т.п.) в facebook, осуществляемые через данный сервис, 
 * должны быть описаны в этом классе public методом, который возвращает объект класса, содержащий конечные методы для взаимодействия с facebook.
 * Например: мы имеем класс PhotoFbAPI для работы с фотографиями, поэтому здесь в текущем классе объявлен public метод photo(), 
 * который возвращает экземпляр класса PhotoFbAPI, который в свою очередь уже содержит такие методы как add(), delete() и др.
 */
class MainFbAPI implements MainInterface {
 
    protected $cache_dir;

    /**
     * @var String login of user on vk
     */
    private $login;
    
    private $password;
    
    private $cookie_file_path;
    
    /**
     * @var String url of image 
     */
    private $upload_url;
     
    /**
     * @var Integer ID of user
     */
    private $mid;

    /**
     * @var Integer ID of group
     */
    private $gid;
    
    /**
     * @var Integer ID of photo-album
     */
    private $paid;

    private $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36';

    /**
     * @var String access_token necessary to manage user profile
     */
    private $user_token = 'CAADl84Nu3CwBAOelZA758ZCJgnYAKHZAHArWSnuyXFliVxFZBZB6VCZBWZAomLYUX5Q0irQxSbpFa3H8lVXMZB9dxZAE7ZA3eO10ZAPkVsTM3brsyWUQqy1Na5k9ZCHjWI7BPYz1CA7XdOVAhur328T2WS8KR1lWL0jNR4P4bbADhi1mMRRu3duYIK71';

    /**
     * @var String access_token necessary to manage fan page
     */
    private $page_token;
    
    /**
     * @var String ID of facebook application (type must be string...)
     */
    private $appId = '252834044894252';
    
    /**
     * @var String facebook application secret key
     */
    private $appSecret = 'eef81ab21cb28b5c809f08345f1d0220';
    
    /**
     * @var String path to the temporary file 
     */
    private $file;
    
    private $facebook;
    
    private $login_url;   
    
    private $cookie_dir;
    
    private $file_dir;


    public function __construct($params) 
    {
       $this->login = $params['login'];
       $this->password = $params['password'];
       $token = $params['token'];
       $this->gid = $params['gid'];
       
       $this->cookie_dir = $params['cookie_dir'];
       $this->file_dir = $params['file_dir'];
       
       $this->cookie_file_path = $this->cookie_dir.'/'.$this->login.'_cookie.txt';
       
       $this->facebook = new Facebook(array(
                      'appId'  => $this->appId,
                      'secret' => $this->appSecret,
                      'fileUpload' => true,
       ));
       
       $this->authorize($token);
       $this->curlAuthorize();
       $this->getPageToken();
    }
    
    public function __toString() {
        return 'fb';
    }        
    
    public function photo()
    {
       $photo = new PhotoFbAPI($this->page_token, $this->gid, $this->file_dir);
       
       return $photo;
    }

    public function message()
    {
        $message = new MessageFbAPI($this->mid, $this->user_token, $this->cookie_file_path);

        return $message;
    }
    
    public function person()
    {
       $person = new PersonFbAPI($this->user_token, $this->appId, $this->appSecret, $this->mid);
       
       return $person;
    }
        
    
    
   public function authorize($token)
    {
       /* 
        + осуществить здесь проверку на token
        если token не тот, то throw ApiException() 
        a getUrl() сделать другим методом сервиса getLoginUrl() 
       */ 
        $this->facebook->setAccessToken($token);
       // $token = $this->facebook->getAccessToken();
        $user = $this->facebook->getUser(); // ID of facebook user
            if (!$user) { 
               throw new ApiException('auth', 'Please authorize on facebook');
           } else { 
               $this->user_token = $token;
               $this->mid = $user;
           }
    }

    public function getAuthUrl() 
    {
         return $this->facebook->getLoginUrl();
    }

    public function getFbUser() 
    {
         return $this->facebook->getUser();  
    }

    public function getToken()
    {
        return $this->facebook->getAccessToken();
    }
    
    private function getPageToken()
    {
       /* 
        для добавления фото в альбомы надо использовать page_access_token 
        вместо access_token от user page_token = 'foo' по дефолту, 
        а в цикле пройдёмся по массиву (полученному в ответ от facebook)
        и найдем действительный page_token 
       */       
       $page_token = 'foo'; 
       /* 
        Здесь PHPUnit-тест выявил Facebook Exception: Unsupported get request 
        (позже выяснилось, что этот exception может означать неверный user access token)
        */
       $data = $this->facebook->api("/$this->mid/accounts?fields=access_token", 'GET', 
                                                array('access_token' => $this->user_token));
      
       foreach ($data['data'] as $val) {
              if ($val['id'] == $this->gid) {
                    $page_token = $val['access_token'];
              }
        }
       if ($page_token == 'foo') {
           throw new ApiException('parse', 'Cannot get page token', 
                   'Group: '.$this->gid.', album: '.$this->paid."\n__________\n".var_export($data, true)
                   );
       }
       
       $this->page_token = $page_token;
       return $page_token;
    }    
    
    /* TODO: как у VK, если cookie не устарели, то не вызывать весь метод авторизации */
    private function curlAuthorize()
    {

        $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,'https://login.facebook.com/login.php');
                curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
                curl_setopt($ch, CURLOPT_REFERER, 'http://www.facebook.com');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HEADER,  array(
                            'Host: www.facebook.com',
                            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                            'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
                            'Connection: keep-alive',
                           )
                        );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_file_path);	
        $page = curl_exec($ch);
            $ch_error = curl_error($ch);
                if ($ch_error) {
                    throw new ApiException('curl', 'CURL error (https://login.facebook.com/login.php): '.$ch_error);
                }        
        curl_close($ch);

        preg_match('/"LSD",\[\],\{"token":"(.*?)"\}/ius', $page, $match);
            if (!isset($match[1])) {
               throw new ApiException('parse', 'Cannot get user token', 
                            var_export($page, true)
                       );
           }
        
        preg_match('/<input type="hidden" name="lgnrnd" value="(.*?)" \/>/ius', $page, $hids);
            if (!isset($hids[1])) {
               throw new ApiException('parse', 'Cannot get hidden parameter', 
                            var_export($page, true)
                       );
           }
        
        
        $lsd = $match[1];
        $lgnrnd = $hids[1];

//        var_dump($hids);
//        var_dump($match); 

        unset($page);

        $reffer = 'http://www.facebook.com/login.php';
        $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,'https://login.facebook.com/login.php?login_attempt=1');
                curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
                curl_setopt($ch, CURLOPT_REFERER, 'http://www.facebook.com/login.php');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_POSTFIELDS, 
                                                    array(
                                                        'lsd' => $lsd,
                                                        'display' => '',
                                                        'enable_profile_selector'  => '',
                                                        'legacy_return' =>	1,
                                                        'profile_selector_ids' =>	'',
                                                        'trynum' =>	1,
                                                        'timezone' =>	-420,
                                                        'lgnrnd' =>	$lgnrnd,
                                                        'lgnjs' =>	time(),
                                                        'email' =>	$this->login,
                                                        'pass' =>	$this->password,
                                                        'persistent' =>	1,
                                                        'default_persistent' =>	0,
                                                    )
                );
                curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_file_path);
                curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_file_path);
        $page = curl_exec($ch);
            $ch_error = curl_error($ch);
                if ($ch_error) {
                    throw new ApiException('curl', 'CURL error (https://login.facebook.com/login.php?login_attempt=1): '.$ch_error);
                }
        curl_close($ch);
    }
}

