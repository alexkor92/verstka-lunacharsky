<?php

namespace Luny\PartnerBundle\API\Social\FB;


use Luny\PartnerBundle\API\Social\MessageInterface;
use Luny\PartnerBundle\Exception\ApiException;
use Luny\PartnerBundle\ApiFb\Facebook;

class MessageFbAPI implements MessageInterface {
    
        
    /**
     * @var String login of user on vk
     */
    private $login;
    
    
    /**
     * @var Integer ID of user
     */
    private $mid;

    /**
     * @var Integer ID of group
     */
    private $gid;
    


    private $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36';

    /**
     * @var String access_token necessary to manage user profile
     */
    private $user_token;
            
    /**
     * @var String access_token necessary to manage fan page
     */
    private $page_token;
    
    /**
     * @var String ID of facebook application (type must be string...)
     */
    private $appId = '252834044894252';
    
    /**
     * @var String facebook application secret key
     */
    private $appSecret = 'eef81ab21cb28b5c809f08345f1d0220';
    
    private $facebook;
    
    private $login_url;    
    
    private $cookie_file_path;
    
    public function __construct($mid, $user_token, $cookie_file_path) {
       $this->mid = $mid;
       $this->user_token = $user_token;
       $this->cookie_file_path = $cookie_file_path;
       $this->facebook = new Facebook(array(
                      'appId'  => $this->appId,
                      'secret' => $this->appSecret,
                      'fileUpload' => true,
       ));
    }
    

    public function send(array $sendParams) 
    {

        $text = isset($sendParams['text']) === false ? 0 : $sendParams['text'];
        $user_id = isset($sendParams['user_id']) === false ? 0 : $sendParams['user_id'];
        $chat_id = isset($sendParams['chat_id']) === false ? null : $sendParams['chat_id'];
      
        
        // https://www.facebook.com/messages/?
        // ajaxpipe=1&
        // ajaxpipe_token=AXhZ1AJUN51vLHby&
        // quickling[version]=1208443%3B0%3B&
        // __user=100007736371745&__a=1&
        // __dyn=7n8anEAMCBDBO29Q9UoGyk4y0yhEK49oK8GAF4imEZ94WpUpBxCE&
        // __req=jsonp_11&
        // __rev=1208443&
        // __adt=11

        $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,'https://www.facebook.com/messages/');
                curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
                curl_setopt($ch, CURLOPT_REFERER, 'http://www.facebook.com');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HEADER, 
                        array(
                            'Host: www.facebook.com',
                            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                            'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
                            'Connection: keep-alive',
                           )
                        );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_file_path);
                curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_file_path);

        $page = curl_exec($ch);
            $ch_error = curl_error($ch);
                if ($ch_error) {
                    throw new ApiException('curl', 'CURL error (https://www.facebook.com/messages/): '.$ch_error);
                }        
        curl_close($ch);
        
        //"root_message_threading_id":"\u003C1391777825338:3039043989-1152350883\u0040mail.projektitan.com>",
        /*                                                                                                                                       100007755424038
        [{"thread_id":"mid.1398410154152:60c36a5e699c8d1731","last_action_id":"1398410154293000000","participants":["fbid:100007755424038","fbid:100007736371745"],
        "former_participants":[],"name":null,"snippet":"\u0443\u0443\u0443","snippet_has_attachment":false,"is_forwarded_snippet":false,"snippet_attachments":[],"snippet_sender":"fbid:100007755424038","unread_count":0,"message_count":1,"image_src":"","timestamp_absolute":"\u0421\u0435\u0433\u043e\u0434\u043d\u044f","timestamp_datetime":"14:15","timestamp_relative":"14:15","timestamp_time_passed":0,"timestamp":1398410154149,"server_timestamp":1398410154149,"mute_settings":[],"is_canonical_user":true,"is_canonical":true,"canonical_fbid":100007755424038,"is_subscribed":true,
        "root_message_threading_id":"\u003C1398410153639:1184462149-27992309\u0040mail.projektitan.com>",
        */        
        preg_match('/\[\{"thread_id":"(.*?)",'
                    . '.*?"participants":\[.*?"fbid:'.$user_id.'".*?\]'
                    . '.*?"root_message_threading_id":"\\\u003C(.*?)\\\u0040mail\.projektitan\.com>",'
                    . '.*?name=\\\"fb_dtsg\\\"\svalue=\\\"(.*?)\\\"'
                    . '/ius',
                  $page, $match);

        if (!isset($match[3])) {
               throw new ApiException('parse', 'Cannot parse messages page', 
                            'Match: '.var_export($match, true).PHP_EOL.PHP_EOL.'______________________________'.PHP_EOL.PHP_EOL
                           .'Page: '.var_export($page, true).PHP_EOL.PHP_EOL.'______________________________'.PHP_EOL.PHP_EOL
                       );
        }        
        
        
        $message_id = '<'.$match[2].'@mail.projektitan.com>';
        $thread_id = $match[1];
        $fb_dtsg = $match[3];
        $author = 'fbid:'.$this->mid;
        $spec_to_list = 'fbid:'.$user_id;

// thread_id mid.1398410154152:60c36a5e699c8d1731
        /*
        message_batch[0][action_type]	ma-type:user-generated-message
        message_batch[0][thread_id]	mid.1391777831095:1dc5d16e1d24f57982
        message_batch[0][author]	fbid:100007736371745
        message_batch[0][author_email]	
        message_batch[0][coordinates]	
        message_batch[0][timestamp]	1397713594394
        message_batch[0][timestamp_absolute]	Сегодня
        message_batch[0][timestamp_relative]	12:46
        message_batch[0][timestamp_time_passed]	0
        message_batch[0][is_unread]	false
        message_batch[0][is_cleared]	false
        message_batch[0][is_forward]	false
        message_batch[0][is_filtered_content]	false
        message_batch[0][is_spoof_warning]	false
        message_batch[0][source]	source:titan:web

        message_batch[0][body]	test api
        message_batch[0][has_attachment]	false
        message_batch[0][html_body]	false

        message_batch[0][specific_to_list][0]	fbid:100003566023240
        message_batch[0][specific_to_list][1]	fbid:100007736371745
        message_batch[0][force_sms]	true
        message_batch[0][ui_push_phase]	V3
        message_batch[0][status]	0
        message_batch[0][message_id]	<1397713594394:2359459384-1571566848@mail.projektitan.com>

        client	mercury
        __user	100007736371745
        __a	1
        __dyn	7n8ahyj35zswyt2u6aEyx9wACwKyaF7BBGfiheCu6popG
        __req	6
        fb_dtsg	AQEw3tPRp-9K
        ttstamp	2658169119511168082112455775
        __rev	1210030

        */

        $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,'https://www.facebook.com/ajax/mercury/send_messages.php');
                curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);

                curl_setopt($ch, CURLOPT_REFERER, 'http://www.facebook.com');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HEADER,  array(
                            'Host: www.facebook.com',
                            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                            'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
                            'Connection: keep-alive',
                           )
                        );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_POSTFIELDS, 
                    array(
                    'message_batch[0][action_type]' => 'ma-type:user-generated-message',
                    'message_batch[0][thread_id]' =>   $thread_id,
                    'message_batch[0][author]'  =>	$author,
                    'message_batch[0][author_email]' => '',
                    'message_batch[0][coordinates]' => '',
                    'message_batch[0][timestamp]' => time(),
                    'message_batch[0][timestamp_absolute]' => 'Сегодня',
                    'message_batch[0][timestamp_relative]' => date('H:i'),
                    'message_batch[0][timestamp_time_passed]' => 0,
                    'message_batch[0][is_unread]' => false,
                    'message_batch[0][is_cleared]' => false,
                    'message_batch[0][is_forward]' => false,
                    'message_batch[0][is_filtered_content]' => false,
                    'message_batch[0][is_spoof_warning]' => false,
                    'message_batch[0][source]' => 'source:titan:web',

                    'message_batch[0][body]' => $text,
                    'message_batch[0][has_attachment]' => false,
                    'message_batch[0][html_body]' => false,

                    'message_batch[0][specific_to_list][0]' => $spec_to_list,
                    'message_batch[0][specific_to_list][1]' => $author,
                    'message_batch[0][force_sms]' => true,
                    'message_batch[0][ui_push_phase]' => 'V3',
                    'message_batch[0][status]' => 0,
                    'message_batch[0][message_id]' => $message_id,

                    'client' => 'mercury',
                    '__user' => $author,
                    '__a' => '',
                        // "в гугле говорят" что dyn можно передавать пустым
                    '__dyn' => '',
                    '__req' => '',
                    'fb_dtsg' => $fb_dtsg,
                    'ttstamp' => '',
                    '__rev' => '',
                    )
                );
                curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_file_path);
                curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_file_path);

        $page = curl_exec($ch);
            $ch_error = curl_error($ch);
                if ($ch_error) {
                    throw new ApiException('curl', 'CURL error (https://www.facebook.com/ajax/mercury/send_messages.php): '.$ch_error);
                }            
        curl_close($ch);        
        
    }

    public function show(array $showParams = array()) 
    {
        /*
         как использовать FQL (вдруг всё-таки буду использовать его)
            $ret = $facebook->api( array(
                         'method' => 'fql.query',
                         'query' => 'SELECT . . . ',
                     ));         
         */
        
        /*
         может пригодится для пагинации, в response массиве содержится следующая интересная конструкция:
         [paging] => Array ( 
            [previous] => https://graph.facebook.com/1433412760229650/comments?limit=25&since=1397719674&__paging_token=enc_AezWltvJkfRa8cjovwOiMSdSeFq3hofCOMk0JF_IHJ7vqMmeT01eYaZxYT1O_Izr5PPoehELwz_5coertVGFS577LTU4YI_NrgmTmpqclZ28lQ&__previous=1 
            [next] => https://graph.facebook.com/1433412760229650/comments?limit=25&until=1391840190&__paging_token=enc_AewKKScnUN2EcBzeF_7Lrgb0n_PtNlLO4vL8UfTRsHR4wYycZojNEzkPt7iYn9L0Co0nJTQ_FvnKLynMadUtdB87Xuo-ZATGWTOdXYhd6u32Rw 
            ) ) ) // почему три скобки? 0.о
           ) 
         [paging] => Array ( 
            [previous] => https://graph.facebook.com/100007736371745/inbox?limit=25&since=1397719674&__paging_token=enc_Aexnk3EWcheSlGvZKXeFPn3aAZhXgawflXBuKtdxldOI5KLrT3fOpSnXBVhIFlvCp3mfNkbgzVDPMUaZ_k6j2eWo&__previous=1 
            [next] => https://graph.facebook.com/100007736371745/inbox?limit=25&until=1397719674&__paging_token=enc_Aewron80I6H_CWuMG7oNdNGkS34Ovm7Xod4c64qFBo-eGkWopMDtLNETsCM8rq13-UhGX98gr5N1c7ppMtTn1NLN 
           ) 
         [summary] => Array ( 
             [unseen_count] => 0 
             [unread_count] => 0 
             [updated_time] => 2014-04-17T07:27:54+0000 
         )
        */
        
        $res = $this->facebook->api("/$this->mid/inbox", array('access_token' => $this->user_token));
        return $res;
    }

    
}