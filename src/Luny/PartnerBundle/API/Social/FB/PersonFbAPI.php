<?php

namespace Luny\PartnerBundle\API\Social\FB;


use Luny\PartnerBundle\API\Social\PersonInterface;
use Luny\PartnerBundle\Exception\ApiException;
use Luny\PartnerBundle\ApiFb\Facebook;

class PersonFbAPI implements PersonInterface {
    
    protected $user_token;

    protected $mid;

    private $facebook;
    
    public function __construct($token, $appId, $appSecret, $mid) 
    {
       $this->user_token = $token;
       $this->mid = $mid;
       $this->facebook = new Facebook(array(
                      'appId'  => $appId,
                      'secret' => $appSecret,
                      'fileUpload' => true,
       ));        
    }
    
    // TODO: получение информации множества юзеров
    public function getInfo($user) 
    {
        
        $result = $this->facebook->api("/v1.0/$user/?fields=first_name,last_name,picture", 'GET', array('access_token' => $this->user_token));
       
        /*
        $fields = 'first_name,last_name,picture';
        
        $queries = array(
            array('method' => 'GET', 'relative_url' => '/me/?fields='.$fields, 'access_token' => $this->user_token),
           // array('method' => 'GET', 'relative_url' => '/me/groups')
        );
        
        $result = $this->facebook->api("?batch=".json_encode($queries)."&access_token=".$this->user_token, 'POST');
     
        
  $result = $this->facebook->api( array(
                         'method' => 'fql.query',
                         'query' => 
      'SELECT uid, name, pic_small FROM user WHERE uid = 1420100541591780 OR uid = 100007736371745',
      'access_token' => $this->user_token
                     ));         
      */
/*      
array (size=4)
  'first_name' => string 'Alexandr' (length=8)
  'last_name' => string 'Gut' (length=3)
  'picture' => 
    array (size=1)
      'data' => 
        array (size=2)
          'is_silhouette' => boolean true
          'url' => string 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-frc1/t1.0-1/c25.0.81.81/s50x50/252231_1002029915278_1941483569_s.jpg' (length=117)
  'id' => string '100007736371745' (length=15)  
*/
        $info['id'] = $result['id'];
        $info['fname'] = $result['first_name'];
        $info['lname'] = $result['last_name'];
        $info['avatar'] = $result['picture']['data']['url'];        
       
        return $info;
    }
    
    public function adminInfo() 
    {
        return $this->getInfo($this->mid);
    }
    
}