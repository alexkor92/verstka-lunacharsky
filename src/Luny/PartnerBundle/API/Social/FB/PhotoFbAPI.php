<?php


namespace Luny\PartnerBundle\API\Social\FB;


use Luny\PartnerBundle\API\Social\PhotoInterface;
use Luny\PartnerBundle\Exception\ApiException;
use Luny\PartnerBundle\ApiFb\Facebook;

class PhotoFbAPI implements PhotoInterface {
    
    /**
     * @var String login of user on vk
     */
    private $login;
    
    /**
     * @var String url of image 
     */
    private $upload_url;
     
    /**
     * @var Integer ID of user
     */
    private $mid;

    /**
     * @var Integer ID of group
     */
    private $gid;
    
    /**
     * @var Integer ID of photo-album
     */
    private $album;

    private $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36';

    /**
     * @var String access_token necessary to manage user profile
     */
    private $user_token = 'CAADl84Nu3CwBAOelZA758ZCJgnYAKHZAHArWSnuyXFliVxFZBZB6VCZBWZAomLYUX5Q0irQxSbpFa3H8lVXMZB9dxZAE7ZA3eO10ZAPkVsTM3brsyWUQqy1Na5k9ZCHjWI7BPYz1CA7XdOVAhur328T2WS8KR1lWL0jNR4P4bbADhi1mMRRu3duYIK71';

    /**
     * @var String access_token necessary to manage fan page
     */
    private $page_token;
    
    /**
     * @var String ID of facebook application (type must be string...)
     */
    private $appId = '252834044894252';
    
    /**
     * @var String facebook application secret key
     */
    private $appSecret = 'eef81ab21cb28b5c809f08345f1d0220';
    
    /**
     * @var String path to the temporary file 
     */
    private $file;
    
    private $facebook;
    
    private $login_url;    
    
    private $file_dir;
    
    
    public function __construct($page_token, $gid, $file_dir) 
    {
       $this->gid = $gid;
       $this->page_token = $page_token;
       $this->file_dir = $file_dir;
       
       $this->facebook = new Facebook(array(
                      'appId'  => $this->appId,
                      'secret' => $this->appSecret,
                      'fileUpload' => true,
       ));
    }    
    
    public function add($photoParams) 
    {
        $descr = isset($photoParams['descr']) ? $photoParams['descr'] : null;
        $photo_url = $photoParams['photo_url'];
        if (isset($photoParams['fb_album'])) {
        $this->album = $photoParams['fb_album'];
        }
        
        /* TODO: раскомментить */
//        if (exif_imagetype($photo_url) === false) { 
//            throw new ApiException('url', 'Incorrect image url');
//        }
        
        $this->photoPrepare($photo_url);

        $args = array( 
                    'message' => $descr,
                    'source' => '@'.$this->file,
                    'access_token' => $this->page_token
        );
        
        $result = $this->facebook->api("/$this->album/photos", 'POST', $args);
        
        if (!isset($result['id'])) {
            throw new ApiException('upload', 'Photo cannot upload');
        }

        return array('fb_img_id' => $result['id']);        
    }
 
    /**
     * @param Integer $id
     * @param String $descr description of image on the VK-page
     */
    public function changeDescr($descrParams) 
    {
       $descr = isset($descrParams['descr']) ? $descrParams['descr'] : null; 
       $url = $descrParams['photo_url']; 
       $id = $descrParams['fb_img_id'];
       $this->album = $descrParams['fb_album'];
       
       $this->delete(array('fb_img_id' => $id));
       $img = $this->add(array(
                            'photo_url' => $url,
                            'descr' => $descr,
                        ));
       return $img;
    }
    
    
    public function delete($deleteParams) 
    {
      $id = $deleteParams['fb_img_id'];
        
      $this->facebook->api("/$id", 'DELETE', array('access_token' => $this->page_token)); 
    }
    
    
    public function setMid($mid) 
    {
        $this->mid = $mid;
    }
    
    public function setParam($gid, $album) 
    {
        $this->gid = $gid;
        $this->album = $album;
        $this->getPageToken(); // need to set page_token property 
    }
    
    private function photoPrepare($url)
    {
      $file = $this->file_dir.'/'.$this->mid.'_'.basename($url);
        $fp = fopen($file, 'w+b');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
            curl_setopt($ch, CURLOPT_TIMEOUT, 120); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_exec($ch);
                $ch_error = curl_error($ch);
                if ($ch_error) {
                    throw new ApiException('curl', 'CURL error ('.$url.'): '.$ch_error);
                }            
            curl_close($ch);
        fclose($fp);
      $this->file = $file;
    }
    
    private function getPageToken()
    {
       /* 
        для добавления фото в альбомы надо использовать page_access_token 
        вместо access_token от user page_token = 'foo' по дефолту, 
        а в цикле пройдёмся по массиву (полученному в ответ от facebook)
        и найдем действительный page_token 
       */       
       $page_token = 'foo'; 
       /* 
        Здесь PHPUnit-тест выявил Facebook Exception: Unsupported get request 
        (позже выяснилось, что этот exception может означать неверный user access token)
        */
       $data = $this->facebook->api("/$this->mid/accounts?fields=access_token", 'GET', array('access_token' => $this->user_token));
        foreach ($data['data'] as $val) {
              if ($val['id'] == $this->gid) {
                    $page_token = $val['access_token'];
              }
        }
       if ($page_token == 'foo') {
           throw new ApiException('parse', 'Cannot get page token', 
                   'Group: '.$this->gid.', album: '.$this->album."\n__________\n".var_export($data, true)
                   );
       }
       
       $this->page_token = $page_token;
       return $page_token;
    }    
}