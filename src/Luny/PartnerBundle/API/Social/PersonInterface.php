<?php

namespace Luny\PartnerBundle\API\Social;

interface PersonInterface
{
    public function getInfo($userIds);
    public function adminInfo();
    
}