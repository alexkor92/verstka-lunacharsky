<?php

namespace Luny\PartnerBundle\API\Social;


interface PhotoInterface
{
    public function add($photoParams);
    public function delete($deleteParams);
    public function changeDescr($descrParams);

}