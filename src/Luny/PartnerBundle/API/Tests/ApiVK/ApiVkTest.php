<?php

namespace Luny\PartnerBundle\Tests\ApiVK;


use Luny\PartnerBundle\ApiVK\ApiVk;

class ApiVkTest extends \PHPUnit_Framework_TestCase
{
 
    /**
     * @var Object
     */
    protected $api;


    public function __construct() 
    {
        $this->api = new ApiVk();
    }    
    
    public function testAuthorize()
    {
        //$api = new ApiVk();
                /* incorrect login or password */
          //  $this->assertEquals(false, $this->api->authorize('login', 'lalala'));
          //  $this->assertEquals(false, $this->api->status);
            
            /* successfully authorize */
            $this->assertEquals(true, $this->api->authorize('89234204076', 'lunacharsky'));
            $this->assertEquals(242587873, $this->api->getMid());
                  

            
    }
    
    /**
     * @depends testAuthorize
     */
    public function testPhotoAdd() {

        /* success */
        $res = $this->api->photoAdd(' ',
                        'xexexe...');
                
        $this->assertArrayHasKey('img_id', $res); 
        $this->assertArrayHasKey('hash', $res); 
        $this->assertRegExp('/d+/', $res['img_id']);
        $this->assertRegExp('/([A-Fa-f0-9]+){32}/', $res['hash']);
        $this->assertEquals(32, strlen($res['hash']));
        
    }

    /**
     * @depends testAuthorize
     */    
    public function testPhotoDelete() 
    {
        $this->api->photoDelete($norm_id, $norm_hash);
        $this->api->photoDelete(12131111113715, 'hrhr4845gsdr84sb');
        
    }

    /**
     * @depends testAuthorize
     */    
    public function testPhotoChangeDescr() 
    {
        $this->api->photoChangeDescr($id, $descr);
    }    
}
