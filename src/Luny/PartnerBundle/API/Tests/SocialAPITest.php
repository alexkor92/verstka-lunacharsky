<?php
 
namespace Luny\PartnerBundle\API\Social\Tests;


use Luny\PartnerBundle\API\Social\SocialAPI;


ob_start();

class SocialAPITest extends \PHPUnit_Framework_TestCase
{
 
    /**
     * @var Object
     */
    protected $api;


    public function __construct() 
    {
        $this->api = new SocialAPI();
    }    
    
    public function testSetParams()
    {

            $this->assertEquals(true, $this->api->setParams(
                        array(
                            'vk' => array(
                                'login' => '89234204076', 
                                'password' => 'lunacharsky'
                                ),
                            'fb' => array (
                                'login' => 'alexandr.gut@bk.ru',
                                'password' => 'lunacharsky',
                                'token' => 'CAADl84Nu3CwBAOelZA758ZCJgnYAKHZAHArWSnuyXFliVxFZBZB6VCZBWZAomLYUX5Q0irQxSbpFa3H8lVXMZB9dxZAE7ZA3eO10ZAPkVsTM3brsyWUQqy1Na5k9ZCHjWI7BPYz1CA7XdOVAhur328T2WS8KR1lWL0jNR4P4bbADhi1mMRRu3duYIK71',
                                'gid' => '1456166004596665',
                                'album' => '1456922711187661',
                            )
                        )
                    ));
            
    }
    
  
    /**
     * @depends testSetParams
     */
    public function testPhotoAdd() {
            $this->assertEquals(true, $this->api->setParams(
                        array(
                            'vk' => array(
                                'login' => '89234204076', 
                                'password' => 'lunacharsky',
                                ),
                            'fb' => array (
                                'login' => 'alexandr.gut@bk.ru',
                                'password' => 'lunacharsky',
                                'token' => 'CAADl84Nu3CwBAOelZA758ZCJgnYAKHZAHArWSnuyXFliVxFZBZB6VCZBWZAomLYUX5Q0irQxSbpFa3H8lVXMZB9dxZAE7ZA3eO10ZAPkVsTM3brsyWUQqy1Na5k9ZCHjWI7BPYz1CA7XdOVAhur328T2WS8KR1lWL0jNR4P4bbADhi1mMRRu3duYIK71',
                                'gid' => '1456166004596665',
                            )
                        )
                    ));
        //var_dump($this->api->socials); exit;
        
        $photoParams = array(
                                'vk_gid' => 66183434,
                                'vk_album' => 186950745,
                                'vk_mid' => 242587873,
                                'photo_url' => 'http://wonderfulengineering.com/wp-content/uploads/2014/01/Technology-Wallpaper-6.jpg',
                                'descr' => 'xexexe...',
                                'fb_group' => '1456166004596665',
                                'fb_album' => '1456922711187661',
                            );
        $res = $this->api->photo('add', $photoParams);
        
        /* success */
        //$res = $photo->add();
                
        $this->assertArrayHasKey('img_id', $res); 
        $this->assertArrayHasKey('hash', $res); 
        $this->assertRegExp('/d+/', $res['img_id']);
        $this->assertRegExp('/([A-Fa-f0-9]+){32}/', $res['hash']);
        $this->assertEquals(32, strlen($res['hash']));
        
    }
 
}
