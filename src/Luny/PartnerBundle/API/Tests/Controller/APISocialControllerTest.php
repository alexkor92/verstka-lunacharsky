<?php

namespace Luny\PartnerBundle\Controller;

use Luny\PartnerBundle\Controller;
use Symfony\Component\HttpFoundation\Request;

class APISocialControllerTest extends \PHPUnit_Framework_TestCase
{
    
 public function testPhotoAdd() {
         /* access token: website, photo_url, product_id, category_id, description */
         $params = array(
             'website' => 'fanpop.com',
             'photo_url' => '',
             'product_id' => 553,
             'category_id' => 32,
             'description' => 'api social controller unit-test'
         );
         $at = '1sugar';
         ksort($params);
            foreach ($params as $key => $value) {
                if (is_array($value)) {
                    unset($params[$key]);
                } else {
                    $at .= $value;
                }
            }
         $token = md5($at);
    /*    
     $mockEM = $this->getMock('\Doctrine\ORM\EntityManager',
            array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);         
         
     $user = $this->getMock('User');
     $user->expects($this->any())->method('getId')->will($this->returnValue('7'));
     //$user->expects($this->any())->method('find')->will($this->returnValue('7'));
     $user->expects($this->any())->method('getSalt')->will($this->returnValue('sugar'));
     
  
     $website = $this->getMock('Website');
     $website->expects($this->any())->method('getGid')->will($this->returnValue(66183434));
     $website->expects($this->any())->method('getDomain')->will($this->returnValue('fanpop.com'));
     $website->expects($this->any())->method('getVkLogin')->will($this->returnValue(89234204076));
     $website->expects($this->any())->method('getVkPass')->will($this->returnValue('lunacharsky'));
     
     
     $category = $this->getMock('Category');
     $category->expects($this->any())->method('getVkAlbum')->will($this->returnValue(186950745));
     */
     $req = new Request();
     $req->create('/api/social/addPhoto', 'GET');
     $req->request->add(
               array('website' => 'fanpop.com',
                  'client_id' => 1,
                  'access_token' => $token,
                  'product_id' => 553,
                  'category_id' => 32,
                  'photo_url' => '',
                  'description' => 'api social controller unit-test'
                )
             );
     
     $soc = new APISocialController();
     /* request: website, client_id, access_token, photo_url, product_id, category_id, description */
     $response = $soc->photoAddAction($req, 'json');
     
 }
 
}
