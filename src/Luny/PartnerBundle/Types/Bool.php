<?php
/**
 * Created by Michael A. Sivolobov
 * Date: 27.05.13
 */

namespace Luny\PartnerBundle\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class Bool extends Type
{
    static $mapper = array(
        'yes' => true,
        'no' => false,
    );

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "ENUM('yes','no') COMMENT '(DC2Type:bool)'";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return static::$mapper[$value];
    }

    /**
     * @param boolean $value
     * @param AbstractPlatform $platform
     * @return int|mixed unixtime for the date field
     * @throws \InvalidArgumentException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value) {
            return 'yes';
        } else {
            return 'no';
        }
    }

    public function getName()
    {
        return 'bool';
    }
}
