<?php

namespace Luny\PartnerBundle\ApiVK;

use Luny\PartnerBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\Request;

ini_set('max_execution_time', 120);

class ApiVK {

    /**
     * @var Bool true is succesfully, in false case see error message
     */
    public $status;
    
    /**
     * @var Array Twodimensional error messages array, key - type of error, value - detail of error
     */
    protected $errors;  

    /**
     * @var Array Messages
     */
    protected $messages;
    
    /**
     * @var String login of user on vk
     */
    protected $login;
    
    /**
     * @var String url of image 
     */
    protected $upload_url;
     
    /**
     * @var Integer ID of user
     */
    protected $mid;

    /**
     * @var Integer ID of group
     */
    protected $gid;
    
    /**
     * @var Integer ID of photo-album
     */
    protected $paid;

    protected $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36';
    
    /**
     * @var String path to the temporary file 
     */
    protected $file;
    
    /**
     * @var String ID of VK application "Luny Service" (type must be string...) string пока под вопросом
     */
    //private $appId = '4257347';
    private $appId = '4264008'; 
    
    /**
     * @var String VK application secret key
     */
    //private $appSecret = 'RFzyMLDuiltEXspwk4gE';    
    private $appSecret = 'sqEzMwTNyQnbcgAJFOuc‏';    
    
    /**
     * @var Float VK API version
     */
    private $appVers = 5.15;    
    
    private $appToken;
        
    
    public function authorize($login, $password)
    {
       $this->login = $login;
       $session_active = $this->authCheck();

          if ($session_active === false) {
               $this->authMake($login, $password);
          }
    }
    
    /**
     * 
     * @param mixed $users массив из ID юзеров (или Integer, если для одного ID), данные которых необходимо вернуть методу
     * @return json
     */
    public function userGetInfo($users) 
    {
        if (empty($this->appToken)) {$this->setAppToken();}         
        if (is_array($users)) {$users = implode(',', $users);}
        
        return file_get_contents('https://api.vk.com/method/users.get?v='.$this->appVers
                                 .'&access_token='.$this->appToken
                                 .'&user_ids='.$users
                                 .'&fields=photo_100'
                                );
    }

    public function userAdminInfo() 
    {
        return $this->userGetInfo($this->mid);
    }


    /**
     * Получение списка диалогов
     * 
     * @return String json
     */
    public function messageAllDialogs() 
    {
        if (empty($this->appToken)) {$this->setAppToken();}        
        return file_get_contents('https://api.vk.com/method/messages.getDialogs?v='.$this->appVers.'&access_token='.$this->appToken);
    }
    
    /**
     * Просмотра диалога от пользователя 
     * 
     * @param String $user_id идентификатор пользователя, историю переписки с которым необходимо вернуть (строка, обязательный параметр).
     * @return String json
     */
    public function messageShowDialog($user_id) 
    {
        if (empty($this->appToken)) {$this->setAppToken();}
        return file_get_contents('https://api.vk.com/method/messages.getHistory?v='.$this->appVers
                                    .'&access_token='.$this->appToken
                                    .'&user_id='.$user_id
                                );
    }    
    
    /**
     * Получение входящих сообщений
     * 
     * @param Integer $time_offset давность получемых сообщений (в секундах), ноль - любая давность
     * @param Integer $offset смещение, необходимое для выборки подмножества сообщений (в кол-ве сообщений)
     * @return String json
     */
    public function messageList($time_offset = 0, $offset = 0) 
    {
        if (empty($this->appToken)) {$this->setAppToken();}
        return file_get_contents('https://api.vk.com/method/messages.get?v='.$this->appVers
                                    .'&access_token='.$this->appToken
                                    .'&count=200'
                                    .'&offset='.$offset
                                    .'&time_offset='.$time_offset
                                );        
    }

    /**
     * Отправка сообщений пользователю
     * 
     * @param String $text текст сообщения
     * @param Integer $user_id ID пользователя, которому адресовано сообщение
     * @param Integer $chat_id ID диалога (не обязательный параметр)
     * 
     * @return String json ID добавленного сообщения (response: message_ID)
     */
    public function messageSend($text, $user_id, $chat_id = null) 
    {
        if (empty($this->appToken)) {$this->setAppToken();}
        return file_get_contents('https://api.vk.com/method/messages.send?v='.$this->appVers
                                    .'&access_token='.$this->appToken
                                    .'&user_id='.$user_id
                                    .'&message='.urlencode($text)
                                );
    }    
    /**
     * @return String
     */
    private function setAppToken() 
    {
       $options = array(
                    CURLOPT_URL => 'https://oauth.vk.com/authorize?client_id='.$this->appId
                                    .'&scope=messages&redirect_uri=https://oauth.vk.com/blank.html&response_type=token&v='.$this->appVers,
                    CURLOPT_TIMEOUT     => 120,
                    CURLOPT_HEADER         => true,                
                    CURLOPT_SSL_VERIFYPEER  => false,  
                    CURLOPT_SSL_VERIFYHOST  => false,
                    CURLOPT_COOKIEFILE  => dirname(__FILE__).'/files/'.$this->login.'_cookie.txt',
                    CURLOPT_USERAGENT   => 'Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.2.13) ' .
                                                                               'Gecko/20101203 Firefox/3.6.13 ( .NET CLR 3.5.30729)',
                    CURLOPT_HTTPHEADER  => array(
                        'Host: oauth.vk.com',
                        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                        'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                        'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                        'Connection: keep-alive',
                        'Cache-Control: max-age=0',
                       ),
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_FOLLOWLOCATION  => true,
             );

      $ch = curl_init();
      curl_setopt_array($ch, $options); 
      $res = curl_exec($ch);  
      curl_close($ch);
      
      preg_match('/Location: https:\/\/oauth\.vk\.com\/blank\.html#access_token=([A-Fa-f0-9]+)&/is', $res, $match);
     
      if (!empty($match)) {
          $this->appToken = $match[1];
          return $match[1];
      }
      
      preg_match('/location\.href = \"https:\/\/login\.vk\.com\/\?act=(.*?)\"/is', $res, $match);

       $options = array(
                    CURLOPT_URL => 'https://login.vk.com/?act='.$match[1],
                    CURLOPT_TIMEOUT     => 120,
                    CURLOPT_HEADER         => true,                
                    CURLOPT_SSL_VERIFYPEER  => false,  
                    CURLOPT_SSL_VERIFYHOST  => false,
                    CURLOPT_COOKIEFILE  => dirname(__FILE__).'/files/'.$this->login.'_cookie.txt',
                    CURLOPT_USERAGENT   => 'Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.2.13) ' .
                                                                               'Gecko/20101203 Firefox/3.6.13 ( .NET CLR 3.5.30729)',
                    CURLOPT_HTTPHEADER  => array(
                        'Host: login.vk.com',
                        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                        'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                        'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                        'Connection: keep-alive',
                        'Cache-Control: max-age=0',
                       ),
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_FOLLOWLOCATION  => true,
             );

      $ch = curl_init();
      curl_setopt_array($ch, $options); 
      $res = curl_exec($ch); 
      curl_close($ch);      
      
      preg_match('/Location: https:\/\/oauth\.vk\.com\/blank\.html#access_token=([A-Fa-f0-9]+)&/is', $res, $match);
     
      if (!empty($match)) {
          $this->appToken = $match[1];
          return $match[1];
      } else {
            throw new ApiException('parse', "Parse error in setAppToken() method :\n\n".var_export($res, true)); 
      }
    }

    /**
     * @param String $photo_url image address
     * @param String $descr description of image on the VK-page
     * @return Array img_id, hash => ID and hash of loaded image
     */
    public function photoAdd($photo_url, $descr = '') 
    {
     
        if (exif_imagetype($photo_url) === false) { 
            throw new ApiException('url', 'Incorrect image url');
        }
        
        $this->photoPrepare($photo_url);
        
        $post_part = array( 
                    'oid' => '-'.$this->gid,
                    'aid' => $this->paid,
                    'gid' => $this->gid,
                    'mid' => $this->mid,
                    'act' => 'do_add',
                    'ajx' => '1',
                    'photo' => '@'.$this->file
        );
        
        $hash = $this->photoAddParam();
        $post = array_merge($post_part, $hash);
        
        /*
         извлекаем host для headers из URLa для загрузки фото
         Пример: из http://cs123456.vk.com/upload.php получаем cs123456.vk.com
         Данная регулярка должна давать нужный результат и в других случаях: 
             другой домен (.me, .cc и т.д.); 
             изменился путь загрузки upload.php на какой-нибудь abraKaDabra/op/up/load123.php;
             другой протокол;
         */
        $host = preg_replace('/[\w\/:]+\/([\d\w\.]+\.vk\.[\w]{2,4})\/[\d\w\.-_\/]*/', '${1}', $this->upload_url);
        $options = array(
                CURLOPT_URL         => $this->upload_url,
                CURLOPT_TIMEOUT     => 120,
                CURLOPT_POST        => 1,
                CURLOPT_POSTFIELDS  => $post,
                CURLOPT_USERAGENT   => $this->user_agent,
                CURLOPT_HTTPHEADER  => array(
                    'Host: '.$host,
                    'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                    'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                    'Keep-Alive: 300',
                    'Connection: keep-alive',
                    'Origin: http://vk.com',
                    'Referer: http://vk.com/album-'.$this->gid.'_'.$this->paid
                   ),
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_FOLLOWLOCATION  => true
         );

        $ch = curl_init();
        curl_setopt_array($ch, $options); 
        $json = curl_exec($ch);  
            $ch_error = curl_error($ch);
            if ($ch_error) {
                throw new ApiException('curl', 'CURL error ('.$this->upload_url.'): '.$ch_error);
            }
        curl_close($ch);
       
        $inf = $this->photoSave($json);

        unset($json);
        // не if isset так как м.б., что descr = ''
        if (!empty($descr)) {
            $this->photoChangeDescr($inf['img_id'], $inf['hash'], $descr);
        }
        
        unlink($this->file); // удаляем временный файл
        
        return array('img_id' => $inf['img_id'], 'hash' => $inf['hash']);        
    }
 
    /**
     * @param Integer $id
     * @param String $hash
     * @param String $descr description of image on the VK-page
     */
    public function photoChangeDescr($id, $hash, $descr) 
    {
                $post = array(
                            'act' => 'save_desc',
                            'al' => '1',
                            'photo' => '-'.$this->gid.'_'.$id,
                            'hash' => $hash,
                            'text' => $descr
                );

                $options = array(
                        CURLOPT_URL         => 'http://vk.com/al_photos.php',
                        CURLOPT_POST        => 1,
                        CURLOPT_POSTFIELDS  => $post,
                        CURLOPT_USERAGENT   => $this->user_agent,
                        CURLOPT_COOKIEFILE  => dirname(__FILE__).'/files/'.$this->login.'_cookie.txt',
                        CURLOPT_HTTPHEADER  => array(
                            'Host: vk.com',
                            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                            'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                            'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                            'Connection: keep-alive',
                            'Origin: http://vk.com',
                        ),
                        CURLOPT_RETURNTRANSFER  => true,
                 );

                $ch = curl_init();
                curl_setopt_array($ch, $options);
                curl_exec($ch);  
                    $ch_error = curl_error($ch);
                    if ($ch_error) {
                        throw new ApiException('curl', 'CURL error (http://vk.com/al_photos.php?act=save_desc): '.$ch_error);
                    }                  
                curl_close($ch);
                
    }
    
    
    public function photoDelete($id, $hash) 
    {

       $post = array(
                    'act' => 'delete_photo',
                    'al' => '1',
                    'photo' => '-'.$this->gid.'_'.$id,
                    'hash' => $hash,
                    'set_prev' => '',
                    'sure' => 0
        );

        $options = array(
                CURLOPT_URL         => 'http://vk.com/al_photos.php',
                CURLOPT_POST        => 1,
                CURLOPT_POSTFIELDS  => $post,
                CURLOPT_USERAGENT   => $this->user_agent,
                CURLOPT_COOKIEFILE  => dirname(__FILE__).'/files/'.$this->login.'_cookie.txt',
                CURLOPT_HTTPHEADER  => array(
                    'Host: vk.com',
                    'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                    'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                    'Connection: keep-alive',
                    'Origin: http://vk.com',
                ),
                CURLOPT_RETURNTRANSFER  => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options); 
        curl_exec($ch);
            $ch_error = curl_error($ch);
            if ($ch_error) {
                throw new ApiException('curl', 'CURL error (http://vk.com/al_photos.php?act=delete_photo): '.$ch_error);
            }        
        curl_close($ch);
    }
    
    
    public function getMid() 
    {
        return $this->mid;
    }
    
    public function setMid($mid) 
    {
        $this->mid = $mid;
    }
    
    public function setParam($gid, $paid) 
    {
        $this->gid = $gid;
        $this->paid = $paid;
    }
    
    private function photoAddParam()
    {
        $options = array(
                CURLOPT_URL             => 'http://vk.com/album-'.$this->gid.'_'.$this->paid.'?act=add',
                CURLOPT_USERAGENT       => $this->user_agent,
                CURLOPT_COOKIEFILE      => dirname(__FILE__).'/files/'.$this->login.'_cookie.txt',
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_FOLLOWLOCATION  => true
         );

        $ch = curl_init();
        curl_setopt_array($ch, $options); 
        $page = curl_exec($ch);
            $ch_error = curl_error($ch);
            if ($ch_error) {
                throw new ApiException('curl', 'CURL error (http://vk.com/album-'.$this->gid.'_'.$this->paid.'?act=add): '.$ch_error);
            }         
        curl_close($ch);
        
        /*
         Пример получаемых данных:
         $param[1] - http://cs608623.vk.com/upload.php 
         (символы - и _ в REGEXP-шаблоне для подстраховки при изменении vk.com url загрузки)
         
         хэши, которые надо передать в запросе, иначе ответ от VK будет "Security Breach 2"
         $param[2] - 19ed53b945db68d2c4f3ea7dca89c50f
         $param[3] - 100ef6ebc0005a9625ac77f70986199f
         */               
        preg_match('/checkVars: \{(?:.*?) upload_url: \'([\d\w\/:\.-_]+)\'(?:.*?) hash: \'([A-Fa-f0-9]+)\'(?:.*?) rhash: \'([A-Fa-f0-9]+)\'(?:.*?)\}/s', $page, $param);

        $this->upload_url = $param[1]; 

        /* если хэши длиной не в 32 символа, то скорее всего парсинг выполнился некорректно => записываем в лог */
            if ((strlen($param[2]) != 32) || (strlen($param[3]) != 32)) {
                throw new ApiException('parse', 'Photo hash parsing problem');
            }
            
        unset($page);
         
        return array('hash' => $param[2], 'rhash' => $param[3]);       
    }
    
    private function photoPrepare($url)
    {
        $a = $this->get('kernel')->getRootDir();
        //var_dump($a); exit;
      $file = dirname(__FILE__).'/files/temp/'.$this->mid.'_'.basename($url);
        $fp = fopen($file, 'w+b');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
            curl_setopt($ch, CURLOPT_TIMEOUT, 120); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_exec($ch);
                $ch_error = curl_error($ch);
                if ($ch_error) {
                    throw new ApiException('curl', 'CURL error ('.$url.'): '.$ch_error);
                }
            curl_close($ch);
        fclose($fp);
      $this->file = $file;
    }
    
    private function photoSave($json)
    {

       /*
        парсинг данных из json: ID-фото вконтакте и его хэша
       */         
            preg_match('/\"photos\":\"(.*?)\",\"hash\":\"([A-Fa-f0-9]+)\"/s', $json, $sec);
                if (!isset($sec[1])) {
                    throw new ApiException('parse', 'Cannot get photos JSON', $json."\n\n".var_export($sec, true));
                }
       /*
         в json-ответе содержится hash и photos, 
         которые необходимо передать в запросе для сохранения фото в альбом
        */
                if (!isset($sec[2])) {
                    throw new ApiException('parse', 'Cannot get photo hash (photoSave)', 
                                           $json."\n\n************\n",var_export($sec, true)
                                          );
                }
      
        /*
         чтобы фото сохранилось в альбом необходимо сделать запрос на vk, 
         который содержал бы в себе json-пакет photos
        */
        $photos = stripslashes($sec[1]);
        /*
         также нам нужен номер сервера, на который загрузилось фото 
         (параметр server в запросе)
        */
        preg_match('/\[\"m\",\"(\d+)\",\"([A-Fa-f0-9]+)\",\"(.*?)\",(?:\d+),(?:\d+)\]/', $photos, $img);
                if (!isset($img[1])) {
                    throw new ApiException('parse', 'Cannot get photos server (photoSave)');
                }
            $post = array(
                        'act' => 'done_add',
                        'aid' => $this->paid,
                        'al' => '1',
                        'context' => '1',
                        'from' => 'html5',
                        'geo' => '1',
                        'gid' => $this->gid,
                        'hash' => $sec[2],
                        'mid' => $this->mid,
                        'photos' => $photos,
                        'server' => substr($img[1], 0, 6)
            );

            $options = array(
                    CURLOPT_URL         => 'http://vk.com/al_photos.php',
                    CURLOPT_POST        => 1,
                    CURLOPT_POSTFIELDS  => $post,
                    CURLOPT_USERAGENT   => $this->user_agent,
                    CURLOPT_COOKIEFILE  => dirname(__FILE__).'/files/'.$this->login.'_cookie.txt',
                    CURLOPT_HTTPHEADER  => array(
                        'Host: vk.com',
                        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                        'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
                        'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
                        'Connection: keep-alive',
                        'Origin: http://vk.com',
                        'Referer: http://vk.com/album-'.$this->gid.'_'.$this->paid
                    ),
                    CURLOPT_RETURNTRANSFER  => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options); 
            $res = curl_exec($ch);
                $ch_error = curl_error($ch);
                if ($ch_error) {
                        throw new ApiException('curl', 'CURL error (http://vk.com/al_photos.php?act=done_add): '.$ch_error);
                }
            curl_close($ch);
            
  
        /*
         json ответ содержит нужную нам информацию
         кроме ID фотки удобно сразу сохранить hash, который также характеризует фото
         при измении описания к фото или её удалении в запрос приходится передавать и ID и этот hash
        */
        preg_match('/photos\.saveDesc\(\'-'.$this->gid.'_([\d]+)\', \'([A-Fa-f0-9]+)\'\)/', $res, $par);
      
        unset($res);
        
        return array('img_id' => $par[1], 'hash' => $par[2]);
    }
    
    /**
     * @return boolean
     */
    private function authCheck()
    {
        $options = array(
                CURLOPT_URL             => 'http://vk.com/settings',
                CURLOPT_USERAGENT       => $this->user_agent,
                CURLOPT_COOKIEFILE      => dirname(__FILE__).'/files/'.$this->login.'_cookie.txt',
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_FOLLOWLOCATION  => true
         );

        $ch = curl_init();
        curl_setopt_array($ch, $options); 
        $page = curl_exec($ch);
            $ch_error = curl_error($ch);
             /*
              закомментировано, так как когда сессия устаревает, то curl выдает ошибку сертификата,
              логирование другой ошибки CURL реализовано чуть ниже
             */
            //            if ($ch_error) {
            //                    throw new ApiException('curl', 'CURL error (http://vk.com/feed): '.$ch_error);
            //            }
        curl_close($ch);
        
        /* 
         всегда ли при устаревшей сессии CURL выдаст ошибку (- не знаю)
         и всегда ли любая ошибка CURL означает, что сессия устарела? (- очевидно, не всегда)
         заодно парсингом смотрим id пользователя, если id не получен, то видимо
         сессия истекла, но может быть это ошибка самого парсинга...
         ВЫВОД: подумать, по возможности поменять логику на более строгую и однозначную
        */
        preg_match('/ href=\"\/audios(\d+)/', $page, $mid);
        unset($page);
            if (!isset($mid[1]) || $ch_error) {
                /* если это не ошибка сертификата, то вероятно проблема не в устаревшей сессии */
                if (strpos($ch_error, 'SSL certificate problem') === false) {
                    throw new ApiException('curl', 'Problem with authCheck (http://vk.com/settings)', $ch_error);
                }
                
                return false; // сессия устарела, надо авторизоваться -> authMake()
            } else {
                $this->mid = $mid[1];
                return true; // сессия активна, снова логиниться не надо
            }
    }

    private function authMake($login, $password)
    {
       $req = new Request(); // Symfony
       
       $post = array(
               'act'          => 'login',
               'q'            => '1',
               'al_frame'     => '1',
               'expire'       => '',
               'captcha_sid'  => '',
               'from_host'    => 'vk.com',
               'from_protocol'=> 'http',
               //'ip_h'         => md5($_SERVER['REMOTE_ADDR']),
               'ip_h'         => md5($req->getClientIp()), // Symfony
               'email'        => $login,
               'pass'         => $password           
       );

      $options = array(
                CURLOPT_URL            => 'https://login.vk.com/?act=login',
                CURLOPT_HEADER         => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERAGENT      => $this->user_agent,
                CURLOPT_SSL_VERIFYPEER => false, 
                CURLOPT_SSL_VERIFYHOST => false,  
                CURLOPT_FOLLOWLOCATION => true,  
                CURLOPT_POST           => true,  
                CURLOPT_POSTFIELDS     => $post, 
                CURLOPT_COOKIEJAR      => dirname(__FILE__).'/files/'.$this->login.'_cookie.txt'
      );

        $ch = curl_init();
        curl_setopt_array($ch, $options); 
        $page = curl_exec($ch);  
            $ch_error = curl_error($ch);
            if ($ch_error) {
                    throw new ApiException('curl', 'CURL error (https://login.vk.com/?act=login): '.$ch_error);
            }
        curl_close($ch);      
        
        /* 
         если в ответ получили страницу содержащую login.vk.com 
         значит авторизация не прошла, выдадим ошибку
        */
        if (preg_match('/ action=\"https:\/\/login\.vk\.com/', $page)) {
            throw new ApiException('auth', 'VK-authorize error');
        }
              /* Extract the user ID */
              /* l cookie is ID of user */
              preg_match("/Set-Cookie: l=(\d+);/", $page, $cookie); 
              if (!isset($cookie[1])) {
                  throw new ApiException('parse', 'Problem with id cookie parsing', $page);
              }
              $this->mid = $cookie[1];
        
        unset($page);
    }

    
}