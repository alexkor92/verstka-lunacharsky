<?php

namespace Luny\PartnerBundle\ApiFb;


use Luny\PartnerBundle\Exception\ApiException;

ini_set('max_execution_time', 120);

class ApiFb 
{

    /**
     * @var String login of user on vk
     */
    private $login;
    
    /**
     * @var String url of image 
     */
    private $upload_url;
     
    /**
     * @var Integer ID of user
     */
    private $mid;

    /**
     * @var Integer ID of group
     */
    private $gid;
    
    /**
     * @var Integer ID of photo-album
     */
    private $paid;

    private $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36';

    /**
     * @var String access_token necessary to manage user profile
     */
    private $user_token = 'CAADl84Nu3CwBAOelZA758ZCJgnYAKHZAHArWSnuyXFliVxFZBZB6VCZBWZAomLYUX5Q0irQxSbpFa3H8lVXMZB9dxZAE7ZA3eO10ZAPkVsTM3brsyWUQqy1Na5k9ZCHjWI7BPYz1CA7XdOVAhur328T2WS8KR1lWL0jNR4P4bbADhi1mMRRu3duYIK71';

    /**
     * @var String access_token necessary to manage fan page
     */
    private $page_token;
    
    /**
     * @var String ID of facebook application (type must be string...)
     */
    private $appId = '252834044894252';
    
    /**
     * @var String facebook application secret key
     */
    private $appSecret = 'eef81ab21cb28b5c809f08345f1d0220';
    
    /**
     * @var String path to the temporary file 
     */
    private $file;
    
    private $facebook;
    
    private $login_url;
    
    public function __construct() {
       $this->facebook = new Facebook(array(
                      'appId'  => $this->appId,
                      'secret' => $this->appSecret,
                      'fileUpload' => true,
       ));
    }
    
    public function authorize($token)
    {
       /* 
        + осуществить здесь проверку на token
        если token не тот, то throw ApiException() 
        a getUrl() сделать другим методом сервиса getLoginUrl() 
       */ 
        $this->facebook->setAccessToken($token);
        $user = $this->facebook->getUser(); // ID of facebook user
            if (!$user) { 
               throw new ApiException('auth', 'Please authorize on facebook');
           } else { 
               $this->user_token = $token;
               $this->mid = $user;
           }
    }

    public function getAuthUrl() 
    {
         return $this->facebook->getLoginUrl();
    }

    public function getFbUser() 
    {
         return $this->facebook->getUser();  
    }

    public function getToken()
    {
        return $this->facebook->getAccessToken();
    }


    public function photoAdd($photo_url, $descr = '') 
    {
        
        if (exif_imagetype($photo_url) === false) { 
            throw new ApiException('url', 'Incorrect image url');
        }
        
        $this->photoPrepare($photo_url);

        $args = array( 
                    'message' => $descr,
                    'source' => '@'.$this->file,
                    'access_token' => $this->page_token
        );
        
        $result = $this->facebook->api("/$this->paid/photos", 'POST', $args);
        
        if (!isset($result['id'])) {
            throw new ApiException('upload', 'Photo cannot upload');
        }

        return array('img_id' => $result['id']);        
    }
 
    /**
     * @param Integer $id
     * @param String $descr description of image on the VK-page
     */
    public function photoChangeDescr($id, $url, $descr) 
    {
       $this->photoDelete($id);
       $img = $this->photoAdd($url, $descr);
       return $img;
    }
    
    
    
    public function photoDelete($id) 
    {
      $this->facebook->api("/$id", 'DELETE', array('access_token' => $this->page_token)); 
    }
    
    // для пагинации использовать /me/threads?until=2011-05-01
    // максимум вроде как только 50 сообщений за раз
    // согласно https://developers.facebook.com/blog/post/478/ можно получать до 5000
    // useful link https://developers.facebook.com/docs/reference/fql/message/
    public function messageList() 
    {
        
        return $this->facebook->api("/$this->mid/inbox");
    }

    public function messageSend()
    {
        //https://www.facebook.com/ajax/mercury/send_messages.php
        /*
            message_batch[0][action_type]	ma-type:user-generated-message
         *                              этот хэш есть на странице facebook.com/messages
            message_batch[0][thread_id]	mid.1391777831095:1dc5d16e1d24f57982 // хеш не есть закодированный 1391777831095
            message_batch[0][author]	fbid:100007736371745
            message_batch[0][author_email]	
            message_batch[0][coordinates]	
            message_batch[0][timestamp]	1397628723958
            message_batch[0][timestamp_absolute]	Сегодня
            message_batch[0][timestamp_relative]	13:12
            message_batch[0][timestamp_time_passed]	0
            message_batch[0][is_unread]	false
            message_batch[0][is_cleared]	false
            message_batch[0][is_forward]	false
            message_batch[0][is_filtered_content]	false
            message_batch[0][is_spoof_warning]	false
            message_batch[0][source]	source:titan:web
            message_batch[0][body]	api test
            message_batch[0][has_attachment]	false
            message_batch[0][html_body]	false
            message_batch[0][specific_to_list][0]	fbid:100003566023240
            message_batch[0][specific_to_list][1]	fbid:100007736371745
            message_batch[0][force_sms]	true
            message_batch[0][ui_push_phase]	V3
            message_batch[0][status]	0
         * также на facebook.com/messages : "root_message_threading_id":"\u003C1391777825338:3039043989-1152350883\u0040mail.projektitan.com>"
            message_batch[0][message_id]	<1397628723958:2225571272-2891205858@mail.projektitan.com>
         * // и нету на странице https://www.facebook.com/ajax/mercury/thread_info.php
            client	mercury
            __user	100007736371745
            __a	1
            __dyn	7n8anEAMNoT88DgDxyG8Eio8Qqbx2mbyaFaimmEZ94WpUpBxCE // этого на странице messages нету
            __req	a
            fb_dtsg	AQFMdQ35s8dQ //  этого на странице messages нету
            ttstamp	2658170771008151531155610081
            __rev	1208443         
         */
    }


    public function getMid() 
    {
        return $this->mid;
    }
    
    public function setMid($mid) 
    {
        $this->mid = $mid;
    }
    
    public function setParam($gid, $paid) 
    {
        $this->gid = $gid;
        $this->paid = $paid;
        $this->getPageToken(); // need to set page_token property 
    }
    
    private function photoPrepare($url)
    {
      $file = dirname(__FILE__).'/files/temp/'.$this->mid.'_'.basename($url);
        $fp = fopen($file, 'w+b');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
            curl_setopt($ch, CURLOPT_TIMEOUT, 120); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_exec($ch);
                $ch_error = curl_error($ch);
                if ($ch_error) {
                    throw new ApiException('curl', 'CURL error ('.$url.'): '.$ch_error);
                }            
            curl_close($ch);
        fclose($fp);
      $this->file = $file;
    }
    
    private function getPageToken()
    {
       /* 
        для добавления фото в альбомы надо использовать page_access_token 
        вместо access_token от user page_token = 'foo' по дефолту, 
        а в цикле пройдёмся по массиву (полученному в ответ от facebook)
        и найдем действительный page_token 
       */       
       $page_token = 'foo'; 
       /* 
        Здесь PHPUnit-тест выявил Facebook Exception: Unsupported get request 
        (позже выяснилось, что этот exception может означать неверный user access token)
        */
       $data = $this->facebook->api("/$this->mid/accounts?fields=access_token", 'GET', array('access_token' => $this->user_token));
        foreach ($data['data'] as $val) {
              if ($val['id'] == $this->gid) {
                    $page_token = $val['access_token'];
              }
        }
       if ($page_token == 'foo') {
           throw new ApiException('parse', 'Cannot get page token', 
                   'Group: '.$this->gid.', album: '.$this->paid."\n__________\n".var_export($data, true)
                   );
       }
       
       $this->page_token = $page_token;
       return $page_token;
    }
    
}