<?php

namespace Luny\PartnerBundle\Exception;

class ApiException extends \Exception {
    
    protected $log_dir;

    public function __construct($type, $message, $detail = null) 
    {
       //$api = $this->getAPI();
       
       /* наши проблемы кладём в лог */
       if ($type === 'parse' || $type === 'curl' || $type === 'auth' || $type === 'usage') {
       $date = date('d.m.Y - H:i:s');
       $this->createPath($this->log_dir.'/SocialAPI');
         $fp = fopen($this->log_dir.'/SocialAPI/'.$type.'.txt', 'w');
         fwrite($fp, '*** '.$date." ***\n\n".$message."\n_____________________________".PHP_EOL.PHP_EOL
                 .$this->getFile()." ::: ".$this->getLine().PHP_EOL.PHP_EOL
                 .$detail."\n\n************************");
        fclose($fp);
       }
       
      //parent::__construct($type.': '.$message);
    }

    public function setLogDir($container) 
    {
        $this->log_dir = $container->getParameter('kernel.logs_dir');
        
    }


//    public function getAPI()
//    {
//       if (basename($this->getFile()) === 'ApiVK.php') {
//           return 'VK';
//       } else {
//          return 'FB';
//       } 
//    }
    
    private function createPath($path) 
    {
        if (is_dir($path)) return true;
        
        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
        $return = $this->createPath($prev_path);
        
        return ($return && is_writable($prev_path)) ? mkdir($path) : false;
    }
    
}