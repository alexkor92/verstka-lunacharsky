<?php
/**
 * Created by Michael A. Sivolobov
 * Date: 14.06.13
 */

namespace Luny\PartnerBundle\Controller;


use Luny\PartnerBundle\Entity\API\Collection;
use Luny\PartnerBundle\Entity\Partner\Order;
use Luny\PartnerBundle\Robokassa;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class APIPaymentController
 * @Route("/api/payments")
 * @package Luny\PartnerBundle\Controller
 */
class APIPaymentController extends APIController
{
    /**
     * @Route("/result/", name="payment_result_url")
     */
    public function resultAction(Request $request)
    {
        /**
         * @var Robokassa $robokassa
         */
        $robokassa = $this->get('robokassa');
        $params = $request->query->all() or $params = $request->request->all();
        $this->get('logger')->addDebug(var_export($params, true));
        if ($status = $robokassa->checkResult($params)) {
            $payment = $robokassa->getPayment($params);
            $em = $this->getDoctrine()->getManager();
            /** @var Order $order */
            $order = $em->getRepository('LunyPartnerBundle:Order')->find($payment->getOrderId());
            $order->setPaidStatus(Order::PAID_STATUS_PAID);
            $commission = $this->container->getParameter('commission');
            $commission = $commission['robokassa'];
            $order->setCommission($order->getPurchasesPrice() * $commission / 100);
            $em->persist($order);
            $em->flush();

            if ($result_url = $order->getWebsite()->getResultUrl()) {
                preg_match('@(.*?)://(.*?)/(.*)$@', $result_url, $matches);
                list(, $protocol, $domain, $query) = $matches;
                $protocol = $protocol == 'https' ? 'ssl' : 'tcp';
                $sock = fsockopen($protocol . '://' . $domain, 80, $errno, $errstr, 15);
                $body = array(
                    'order_id' => $order->getExternalId(),
                    'sum' => intval($payment->getSum() * 100),
                );
                $parameters = $body;

                $parameters['client_id'] = $order->getPartner()->getId();
                $body['access_token'] = $this->generateAccessToken($parameters, $order->getPartner()->getSalt());
                $body = http_build_query($body);

                if (!$sock) $this->get('logger')->addAlert("$errstr ($errno)");

                fputs($sock, "POST /$query HTTP/1.1\r\n");
                fputs($sock, "Host: $domain\r\n");
                fputs($sock, "Content-Type: application/x-www-form-urlencoded; charset=UTF-8\r\n");
                fputs($sock, "Accept: */*\r\n");
                fputs($sock, 'Content-Length: ' . strlen($body) . "\r\n");
                fputs($sock, "Connection: close\r\n\r\n");
                fputs($sock, $body);
                $body = '';
                while (!feof($sock)) {
                    $body .= fgets($sock, 4096);
                }
                fclose($sock);
            }
            $emails = $em->createQuery('SELECT u.email FROM LunyUserBundle:User u WHERE u.usertype = \'manager\'')->getScalarResult();
            $emails = array_map('current', $emails);
            $server = $_SERVER['SERVER_NAME'];
            $server = preg_replace('/^www\./', '', $server);
            $message = \Swift_Message::newInstance()
                ->setSubject('Поступила оплата по заказу №' . $order->getId() . ' (' . $order->getExternalId() . ')')
                ->setFrom('no-reply@' . $server)
                ->setTo($emails)
                ->setBody(
                    $this->renderView(
                        'LunyPartnerBundle:Helper:email_payment.html.twig',
                        array('order' => $order, 'sum' => $payment->getSum())
                    ),
                    'text/html',
                    'utf-8'
                );
            $this->get('mailer')->send($message);
            return new Response($status);
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Route("/success/", name="payment_success_url")
     */
    public function successUrlAction(Request $request)
    {
        /**
         * @var Robokassa $robokassa
         */
        $robokassa = $this->get('robokassa');
        $params = $request->query->all() or $params = $request->request->all();
        $this->get('logger')->addDebug(var_export($params, true));
        if ($robokassa->checkSuccess($params)) {
            $payment = $robokassa->getPayment($params);
            /** @var Order $order */
            $order = $this->getDoctrine()->getRepository('LunyPartnerBundle:Order')->find($payment->getOrderId());
            $url = $order->getWebsite()->getSuccessUrl() or $url = '//' . $order->getWebsiteDomain();

            $body = http_build_query(array(
                'order_id' => $order->getExternalId(),
                'sum' => intval($payment->getSum() * 100),
            ));
            if (strstr($url, '?')) {
                $url .= '&' . $body;
            } else {
                if (substr($url, -1, 1) == '/') {
                    $url .= '?' . $body;
                } else {
                    $url .= '/?' . $body;
                }
            }
            return $this->redirect($url);
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Route("/fail/", name="payment_fail_url")
     */
    public function failUrlAction(Request $request)
    {
        /**
         * @var Robokassa $robokassa
         */
        $robokassa = $this->get('robokassa');
        $params = $request->query->all() or $params = $request->request->all();
        $this->get('logger')->addDebug(var_export($params, true));
        $payment = $robokassa->getPayment($params);
        /** @var Order $order */
        $order = $this->getDoctrine()->getRepository('LunyPartnerBundle:Order')->find($payment->getOrderId());
        $url = $order->getWebsite()->getFailUrl() or $url = '//' . $order->getWebsiteDomain();
        $body = http_build_query(array(
            'order_id' => $order->getExternalId(),
            'sum' => intval($payment->getSum() * 100),
        ));
        if (strstr($url, '?')) {
            $url .= '&' . $body;
        } else {
            if (substr($url, -1, 1) == '/') {
                $url .= '?' . $body;
            } else {
                $url .= '/?' . $body;
            }
        }
        return $this->redirect($url);
    }

    /**
     * @Route(".{_format}/getParameters", name="api_payment_get_parameters")
     */
    public function getParametersAction(Request $request, $_format)
    {
        $params = $request->query->all() or $params = $request->request->all();
        $this->checkCredentials($params);


        if (!empty($params['order_id'])) {
            $order_id = $params['order_id'];
        } else {
            throw $this->createNotFoundException('Вы должны указать корректный order_id');
        }
        $em = $this->getDoctrine()->getManager();
        /** @var Order $order */
        $order = $em
            ->getRepository('LunyPartnerBundle:Order')
            ->findOneBy(
                array(
                    'externalId' => $order_id,
                    'partner' => $this->user,
                )
            );
        if (!$order || $order->getPartner()->getId() != $this->user->getId()) {
            throw $this->createNotFoundException('Заказ не найден');
        }

        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        /** @var Robokassa $robokassa */
        $robokassa = $this->get('robokassa');

        $params = $robokassa->generateParameters($order->getId(), $order->getPurchasesPrice(), $order->getContainer());

        $collection = new Collection(
            array(
                'url' => $robokassa->getUrl(),
                'parameters' => $params,
            )
        );
        $body = $serializer->serialize($collection, $_format);
        return new Response($body, 200, array('Content-Type' => 'application/' . $_format));
    }

    /**
     * @Route("/redirect/{id}", requirements={ "id" = "\d+" }, name="api_payment_redirect")
     */
    public function redirectAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Order $order */
        $order = $em->getRepository('LunyPartnerBundle:Order')->find($id);
        if (!$order) {
            throw $this->createNotFoundException('Заказ не найден');
        }
        /** @var Robokassa $robokassa */
        $robokassa = $this->get('robokassa');
        $this->user = $order->getPartner();
        $params = $robokassa->generateParameters($order->getExternalId(), $order->getPurchasesPrice(), $order->getContainer());
        return $this->redirect($robokassa->getUrl() . '?' . http_build_query($params));
    }
}