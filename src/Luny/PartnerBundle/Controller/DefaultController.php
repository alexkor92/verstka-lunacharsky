<?php

namespace Luny\PartnerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('LunyPartnerBundle:Default:index.html.twig', array('name' => $name));
    }
}
