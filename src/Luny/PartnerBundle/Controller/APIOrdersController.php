<?php
/**
 * Created by Michael A. Sivolobov
 * Date: 12.04.13
 */

namespace Luny\PartnerBundle\Controller;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use Luny\PartnerBundle\Entity\API\Status;
use Luny\PartnerBundle\Entity\Partner\Order;
use Luny\PartnerBundle\Entity\Partner\Purchase;
use Luny\PartnerBundle\Entity\Partner\Website;
use Luny\PartnerBundle\Form\OrderType;
use Luny\PartnerBundle\Form\PurchaseType;
use Luny\UserBundle\Entity\User;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api/orders.{_format}", requirements={"_format" = "json|xml|html"})
 * Class API
 * @package Luny\PartnerBundle\Controller
 */
class APIOrdersController extends APIController
{
    /**
     * @Route("/createOrder", name="api_create_order")
     */
    public function createOrderAction(Request $request, $_format)
    {
        $logger = $this->get('logger');
        $data = $request->query->all() or $data = $request->request->all();
        $logger->addInfo(var_export($data, true));
        $this->checkCredentials($data);

        $order = new Order();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->createForm(new OrderType(true), $order);

        $data['partner'] = $data['client_id'];
        $data['status'] = Order::ORDER_STATUS_PENDING;
        $data = array_intersect_key($data, $form->all());
        if (!empty($data['website'])) {
            $data['website'] = preg_replace('/^www\./', '', $data['website']);
        }
        try {
            $form->bind($data);
            if ($form->isValid()) {
                $em->persist($order);
                $em->flush();
                $emails = $em->createQuery('SELECT u.email FROM LunyUserBundle:User u WHERE u.usertype = \'manager\'')->getScalarResult();
                $emails = array_map('current', $emails);
                $server = $_SERVER['SERVER_NAME'];
                $server = preg_replace('/^www\./', '', $server);
                $message = \Swift_Message::newInstance()
                    ->setSubject('Поступил заказ №' . $order->getId() . ' (' . $order->getExternalId() . ')')
                    ->setFrom('no-reply@' . $server)
                    ->setTo($emails)
                    ->setBody(
                        $this->renderView(
                            'LunyPartnerBundle:Helper:email_message.html.twig',
                            array('order' => $form->getData())
                        ),
                        'text/html',
                        'utf-8'
                    )
                ;
                $this->get('mailer')->send($message);
            } else {
                $logger->addAlert($form->getErrorsAsString());
                $this->errors[] = $form->getErrors();
            }
        } catch (\Exception $e) {
            $logger->addAlert($e->getMessage());
            $logger->addAlert($e->getTraceAsString());
            $this->errors[] = 'Неверный запрос';
        }

        $serializer = $this->get('serializer');
        $status = empty($this->errors) ? Status::STATUS_SUCCESS : Status::STATUS_ERROR;
        $status = new Status($status, $this->errors);
        $body = $serializer->serialize($status, $_format);
        if ($_format === 'json') {
            $body = $this->repairUTF8JSON($body);
        }
        $logger->addInfo($body);
        return new Response($body, 200, array('Content-Type' => 'application/' . $_format));
    }

    /**
     * @Route("/getStatus", name="api_get_order_status")
     * @param Request $request
     */
    public function getStatusAction(Request $request, $_format)
    {
        $data = $request->query->all() or $data = $request->request->all();
        $this->checkCredentials($data);

        if (!empty($data['order_id'])) {
            $order_id = $data['order_id'];
        } else {
            throw $this->createNotFoundException('Вы должны указать корректный order_id');
        }
        /** @var Order $order */
        $order = $this
            ->getDoctrine()
            ->getRepository('LunyPartnerBundle:Order')
            ->findOneBy(
                array(
                    'externalId' => $order_id,
                    'partner' => $this->user,
                )
            );
        if (!$order || $order->getPartner()->getId() != $this->user->getId()) {
            throw $this->createNotFoundException('Заказ не найден');
        }
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');

        $object = new \Luny\PartnerBundle\Entity\API\Order();
        $object->fromOrder($order);

        $translator = $this->get('translator');
        $object->getDelivery()->setType($translator->trans($object->getDelivery()->getType()));

        $body = $serializer->serialize($object, $_format);
        if ($_format === 'json') {
            $body = $this->repairUTF8JSON($body);
        }
        return new Response($body, 200, array('Content-Type' => 'application/' . $_format));
    }

    /**
     * @Route("/cancelOrder", name="api_cancel_order")
     */
    public function cancelOrderAction(Request $request, $_format)
    {
        $data = $request->query->all() or $data = $request->request->all();
        $this->checkCredentials($data);

        if (!empty($data['order_id'])) {
            $order_id = $data['order_id'];
        } else {
            throw $this->createNotFoundException('Вы должны указать корректный order_id');
        }
        $em = $this->getDoctrine()->getManager();
        /** @var Order $order */
        $order = $this
            ->getDoctrine()
            ->getRepository('LunyPartnerBundle:Order')
            ->findOneBy(
                array(
                    'externalId' => $order_id,
                    'partner' => $this->user,
                )
            );
        if (!$order || $order->getPartner()->getId() != $this->user->getId()) {
            throw $this->createNotFoundException('Заказ не найден');
        }
        $order->setStatus(Order::ORDER_STATUS_CANCELED);
        $em->persist($order);
        $em->flush();

        $serializer = $this->get('serializer');
        $status = empty($this->errors) ? Status::STATUS_SUCCESS : Status::STATUS_ERROR;
        $status = new Status($status, $this->errors);
        $body = $serializer->serialize($status, $_format);
        if ($_format === 'json') {
            $body = $this->repairUTF8JSON($body);
        }
        return new Response($body, 200, array('Content-Type' => 'application/' . $_format));
    }
}