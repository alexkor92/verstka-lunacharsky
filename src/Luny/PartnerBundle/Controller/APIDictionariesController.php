<?php
/**
 * Created by Michael A. Sivolobov
 * Date: 13.06.13
 */

namespace Luny\PartnerBundle\Controller;

use JMS\Serializer\Serializer;
use Luny\PartnerBundle\Entity\API\Collection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class APIDictionariesController
 * @Route("/api/list.{_format}", requirements={"_format" = "json|xml"})
 * @package Luny\PartnerBundle\Controller
 */
class APIDictionariesController extends APIController
{
    /**
     * @Route("/getCategories", name="api_list_categories")
     */
    public function getCategoriesAction($_format)
    {
        $this->checkCredentials($this->getRequest()->query->all());
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');

        $categories = $this->getDoctrine()->getManager('simpla')->getRepository('LunyPartnerBundle:Category')->findAll();
        $collection = new Collection($categories);
        if ($collection->prepareList()) {
            $body = $serializer->serialize($collection, $_format);
        } else {
            throw new \ErrorException('Server error');
        }
        if ($_format === 'json') {
            $body = $this->repairUTF8JSON($body);
        }
        return new Response($body, 200, array('Content-Type' => 'application/' . $_format));
    }

    /**
     * @Route("/getBrands", name="api_list_brands")
     */
    public function getBrandsAction($_format)
    {
        $this->checkCredentials($this->getRequest()->query->all());
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');

        $brands = $this->getDoctrine()->getManager('simpla')->getRepository('LunyPartnerBundle:Brand')->findAll();
        $collection = new Collection($brands);
        if ($collection->prepareList()) {
            $body = $serializer->serialize($collection, $_format);
        } else {
            throw new \ErrorException('Server error');
        }
        if ($_format === 'json') {
            $body = $this->repairUTF8JSON($body);
        }
        return new Response($body, 200, array('Content-Type' => 'application/' . $_format));
    }

    /**
     * @Route("/getDeliveryTypes", name="api_list_delivery_types")
     */
    public function getDeliveryTypesAction($_format)
    {
        $this->checkCredentials($this->getRequest()->query->all());
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');

        $delivery_types = \Doctrine\DBAL\Types\Type::getType('enumdeliverytype')->getValues();
        $collection = new Collection($delivery_types);
        if ($collection->prepareList($this->get('translator'))) {
            $body = $serializer->serialize($collection, $_format);
        } else {
            throw new \ErrorException('Server error');
        }
        if ($_format === 'json') {
            $body = $this->repairUTF8JSON($body);
        }
        return new Response($body, 200, array('Content-Type' => 'application/' . $_format));
    }

    /**
     * @Route("/getPaymentMethods", name="api_list_payment_methods")
     */
    public function getPaymentMethodsAction($_format)
    {
        $this->checkCredentials($this->getRequest()->query->all());
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');

        $payment_methods = \Doctrine\DBAL\Types\Type::getType('enumpayments')->getValues();
        $collection = new Collection($payment_methods);
        if ($collection->prepareList($this->get('translator'))) {
            $body = $serializer->serialize($collection, $_format);
        } else {
            throw new \ErrorException('Server error');
        }
        return new Response($body, 200, array('Content-Type' => 'application/' . $_format));
    }

    /**
     * @Route("/getStatuses", name="api_list_statuses")
     */
    public function getStatusesAction($_format)
    {
        $this->checkCredentials($this->getRequest()->query->all());
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');

        $delivery_types = \Doctrine\DBAL\Types\Type::getType('enumorderstatus')->getValues();
        $collection = new Collection($delivery_types);
        if ($collection->prepareList($this->get('translator'))) {
            $body = $serializer->serialize($collection, $_format);
        } else {
            throw new \ErrorException('Server error');
        }
        if ($_format === 'json') {
            $body = $this->repairUTF8JSON($body);
        }
        return new Response($body, 200, array('Content-Type' => 'application/' . $_format));
    }
}