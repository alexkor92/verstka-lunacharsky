<?php
/**
 * Created by Michael A. Sivolobov
 * Date: 03.06.13
 */

namespace Luny\PartnerBundle\Controller;


use Luny\PartnerBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class APIController extends Controller
{
    /** @var User */
    protected $user;
    protected $errors;

    protected function checkCredentials($params)
    {
        if (!isset($params['client_id']) && !isset($params['access_token'])) {
            throw new AccessDeniedHttpException('client_id or access_token is empty');
        }
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository('LunyPartnerBundle:User')->find($params['client_id']);
        if (!$user) {
            throw new AccessDeniedHttpException('client_id or access_token is incorrect');
        }
        $access_token = $params['access_token'];

        if ($access_token !== $this->generateAccessToken($params, $user->getSalt())) {
            throw new AccessDeniedHttpException('Your access_token is incorrect');
        }
        $this->user = $user;
        return true;
    }

    protected function repairUTF8JSON($json)
    {
        return preg_replace_callback(
            '/\\\u([0-9a-fA-F]{4})/',
            create_function('$matches', 'return mb_convert_encoding("&#" . intval($matches[1], 16) . ";", "UTF-8", "HTML-ENTITIES");'),
            $json
        );
    }

    protected function generateAccessToken($params, $secret_key)
    {
        $at = $params['client_id'] . $secret_key;
        unset($params['access_token']);
        unset($params['client_id']);
        ksort($params);
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                unset($params[$key]);
            } else {
                $at .= $value;
            }
        }
        //return md5($at);
        return 'test';
    }
}