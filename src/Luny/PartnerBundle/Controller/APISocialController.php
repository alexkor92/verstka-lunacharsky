<?php

namespace Luny\PartnerBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Luny\PartnerBundle\Exception\ApiException;
use Luny\PartnerBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class APISocialController extends APIController
{
 
    public function photoAddAction(Request $request, $_format)
    {
        /* request: website, client_id, access_token, photo_url, product_id, category_id, descr */
        $data = $request->query->all() or $data = $request->request->all();
        $this->checkCredentials($data);

        $website = $this->getDoctrine()->getRepository('LunyPartnerBundle:Website')
                                       ->findOneByPartner($data['client_id']);
        if (!$website) {
            throw $this->createNotFoundException('No site found for client id '.$data['client_id']);
        }
        
        $clientDomain = $website->getDomainName();
        $photoDomain = parse_url($data['photo_url'], PHP_URL_HOST);
        if (($clientDomain != $photoDomain) && (strpos($photoDomain, '.'.$clientDomain) === false)) {
            throw new \Exception('Image URL is not from your website '.$clientDomain);
        }
        
        $category = $this->getDoctrine()
                         ->getRepository('LunyPartnerBundle:Category')
                         ->findOneBy(array('ext_id' => $data['category_id'], 'domain' => $website->getId()));
        if (!$category) {
                 throw $this->createNotFoundException('No category found for id '.$data['category_id']);
        }
        
        $product = $this->getDoctrine()->getRepository('LunyPartnerBundle:Product')
                        ->findOneBy(array('ext_id' => $data['product_id'], 'website' => $website->getId()));
        if ($product) {
              throw new \Exception('Product ('.$data['product_id'].') already exists, use update action');    
        }
        
           $product = new Product();
           $product->setExtId($data['product_id']);
           $product->setCategory($category);
           $product->setWebsite($website);
           
          /* TODO: хранить $params в array в БД*/
           $params = array();
           if ($website->getFbUse() === true) {
               $params['fb'] = array(
                                    'login' => $website->getFbLogin(),
                                    'password' => $website->getFbPass(),
                                    'gid' => $website->getFbGroup(),
                                    'token' => $website->getFbToken(),
                                );
           }
           if ($website->getVkUse() === true) {
               $params['vk'] = array(
                                    'login' => $website->getVkLogin(),
                                    'password' => $website->getVkPass(),
                                    'gid' => $website->getVkGroup(),
                                );
           }
           
           $social = $this->get('social');
           $social->setParams($params);
           $resp = $social->photo('add', array(
                                    'photo_url' => $data['photo_url'],
                                    'descr' => $data['descr'],
                                    'fb_album' => $category->getFbAlbum(),
                                    'vk_album' => $category->getVkAlbum(),
                         ));
           
                         
           $product->setFbImgId($resp['fb']['fb_img_id']);                    

           $product->setVkImgId($resp['vk']['vk_img_id']);
           $product->setVkImgHash($resp['vk']['vk_img_hash']);

         /* record to db */
           if (!empty($data['descr'])) {$product->setDescr($data['descr']);}
           $em = $this->getDoctrine()->getManager();
           $em->persist($product);
           $em->flush(); 
        /* succesfully */
         $body = json_encode(array('status' => 'success'));
         return new Response($body, 200, array('Content-Type' => 'application/'.$_format));
    }    
    
    
    public function photoChangeDescrAction(Request $request, $_format)
    {
        /*  request: website, client_id, access_token, product_id, descr, photo_url  */
        $data = $request->query->all() or $data = $request->request->all();
            $user =  $this->checkCredentials($data);

             $website = $this->getDoctrine()->getRepository('LunyPartnerBundle:Website')
                                            ->findOneByPartner($this->user->getId());
             if (!$website) {
                  throw $this->createNotFoundException('No site found for client id '.$data['client_id']);
             }
             
            $clientDomain = $website->getDomainName();
            $photoDomain = parse_url($data['photo_url'], PHP_URL_HOST);
            if (($clientDomain != $photoDomain) && (strpos($photoDomain, '.'.$clientDomain) === false)) {
                throw new \Exception('Image URL is not from your website '.$clientDomain);
            }             
             
           $em = $this->getDoctrine()->getEntityManager();
           $product = $em->getRepository('LunyPartnerBundle:Product')
                           ->findOneBy(array('ext_id' => $data['product_id'], 'website' => $website->getId()));
             
                if (!$product || $product->getCategory()->getDomain()->getDomain() != $data['website']) {
                    throw $this->createNotFoundException('No product found for id '.$data['product_id']);
                }                
                   
//                /* используем facebook */
               $fb_img_id = $product->getFbImgId();
//               if (($website->getFbUse() === true) && !empty($fb_img_id)) {
//                    /* получить id photo удалить его, загрузить заново */
//                        if ($website->getDomain() !=  parse_url($data['photo_url'], PHP_URL_HOST)) {
//                            /* error: image is not from your site */
//                        }                    
//                    $fb = $this->get('fb_api');
//                    $fb->authorize($website->getFbToken());
//                    $fb->setParam($website->getFbGroup(), $product->getCategory()->getFbAlbum());
//                    $fb_img = $fb->photoChangeDescr($fb_img_id, $data['photo_url'], $data['descr']);
//                    
//                }
//
                /* используем вконтакте */
               $vk_img_id = $product->getVkImgId();
//               if (($website->getVkUse() === true) && ($vk_img_id > 0)) {
//                     $vk = $this->get('vk_api');    
//                     $vk->authorize($website->getVkLogin(), $website->getVkPass());
//                     $vk->setParam($website->getVkGroup(), $product->getCategory()->getFbAlbum());
//                     $vk->photoChangeDescr($vk_img_id, $product->getVkImgHash(), $data['descr']);
//                }
               
                
          /* TODO: хранить $params в array в БД*/
           $params = array();
           if ($website->getFbUse() === true) {
               $params['fb'] = array(
                                    'login' => $website->getFbLogin(),
                                    'password' => $website->getFbPass(),
                                    'gid' => $website->getFbGroup(),
                                    'token' => $website->getFbToken(),
                                );
           }
           if ($website->getVkUse() === true) {
               $params['vk'] = array(
                                    'login' => $website->getVkLogin(),
                                    'password' => $website->getVkPass(),
                                    'gid' => $website->getVkGroup(),
                                );
           }
           
           $social = $this->get('social');
           $social->setParams($params);
           $resp = $social->photo('changeDescr', array(
                                    'photo_url' => $data['photo_url'],
                                    'descr' => $data['descr'],
                                    'fb_album' => $product->getCategory()->getFbAlbum(),
                                    'fb_img_id' => $fb_img_id,
                                    'vk_img_id' => $vk_img_id,
                                    'vk_img_hash' => $product->getVkImgHash(),
                         ));
                
           $product->setFbImgId($resp['fb']['fb_img_id']);
           $product->setDescr($data['descr']);
           $em->flush();
          
         $body = json_encode(array('status' => 'success'));
         return new Response($body, 200, array('Content-Type' => 'application/'.$_format));            
    }   

   
    public function photoDeleteAction(Request $request, $_format)
    {
        /*  request: website, client_id, access_token, product_id */
        $data = $request->query->all() or $data = $request->request->all();
        $this->checkCredentials($data);
        
        $website = $this->getDoctrine()->getRepository('LunyPartnerBundle:Website')
                                        ->findOneByPartner($this->user->getId());
            if (!$website) {
                    throw $this->createNotFoundException('No website found for id '.$data['product_id']);
            }         
        
         $em = $this->getDoctrine()->getEntityManager();
         $product = $em->getRepository('LunyPartnerBundle:Product')
                       ->findOneBy(array('ext_id' => $data['product_id'], 'website' => $website->getId()));

            if (!$product) {
                throw $this->createNotFoundException('No product found for id '.$data['product_id']);
            }
       
              /* используем facebook */
               $fb_img_id = $product->getFbImgId();
//               if (($website->getFbUse() === true) && ($fb_img_id > 0)) {
//                 $fb = $this->get('fb_api');
//                 $fb->authorize($website->getFbToken());
//                 $fb->photoDelete($fb_img_id);
//               }
//
//               /* используем вконтакте */
               $vk_img_id = $product->getVkImgId();
//               if (($website->getVkUse() === true) && ($vk_img_id > 0)) {        
//                 $vk = $this->get('vk_api');    
//                 $vk->authorize($website->getVkLogin(), $website->getVkPass());
//                 $vk->setParam($website->getVkGroup(), $product->getCategory()->getVkAlbum());
//                 $vk->photoDelete($vk_img_id, $product->getVkImgHash());
//               }
           
          /* TODO: хранить $params в array в БД*/
           $params = array();
           if ($website->getFbUse() === true) {
               $params['fb'] = array(
                                    'login' => $website->getFbLogin(),
                                    'password' => $website->getFbPass(),
                                    'gid' => $website->getFbGroup(),
                                    'token' => $website->getFbToken(),
                                );
           }
           if ($website->getVkUse() === true) {
               $params['vk'] = array(
                                    'login' => $website->getVkLogin(),
                                    'password' => $website->getVkPass(),
                                    'gid' => $website->getVkGroup(),
                                );
           }
           
           $social = $this->get('social');
           $social->setParams($params);
           $resp = $social->photo('delete', array(
                                    'fb_album' => $product->getCategory()->getFbAlbum(),
                                    'fb_img_id' => $fb_img_id,
                                    'vk_img_id' => $vk_img_id,
                                    'vk_img_hash' => $product->getVkImgHash(),
                         ));               
               
          $em->remove($product);
          $em->flush();
         $body = json_encode(array('status' => 'success'));
         return new Response($body, 200, array('Content-Type' => 'application/'.$_format));        
    }
    
    /**
     * change image and description (optional) with the defined ext_id (product_id in request)
     */
    public function photoUpdateAction(Request $request, $_format)
    {
        /* request: website, client_id, access_token, photo_url, product_id, category_id, descr */
        
        $data = $request->query->all() or $data = $request->request->all();
        $user = $this->checkCredentials($data); 
        
        $website = $this->getDoctrine()->getRepository('LunyPartnerBundle:Website')
                                       ->findOneByPartner($data['client_id']);
        if (!$website) {
            throw $this->createNotFoundException('No site found for id '.$data['client_id']);
        }

        $clientDomain = $website->getDomainName();
        $photoDomain = parse_url($data['photo_url'], PHP_URL_HOST);
        if (($clientDomain != $photoDomain) && (strpos($photoDomain, '.'.$clientDomain) === false)) {
            throw new \Exception('Image URL is not from your website '.$clientDomain);
        }

        $category = $this->getDoctrine()
                         ->getRepository('LunyPartnerBundle:Category')
                         ->findOneBy(array('ext_id' => $data['category_id'], 'domain' => $website->getId()));
        if (!$category) {
                 throw $this->createNotFoundException('No category found for id '.$data['category_id']);
        }
        
          $em = $this->getDoctrine()->getEntityManager();
          $product = $em->getRepository('LunyPartnerBundle:Product')
                        ->findOneBy(array('ext_id' => $data['product_id'], 'website' => $website->getId()));

            if (!$product) {
                throw $this->createNotFoundException('No product found for id '.$data['product_id']);
            }
             
//               /* используем facebook */
//               if ($website->getFbUse() === true) {
//                $fb = $this->get('fb_api');
//                $fb->authorize($website->getFbToken());
//                $fb->setParam($website->getFbGroup(), $category->getFbAlbum());
//                $fb_img_id = $product->getFbImgId();
//                 if (!empty($fb_img_id)) {
//                     $fb->photoDelete($fb_img_id);               
//                 }
//                $fb_img = $fb->photoAdd($data['photo_url'], $data['descr']);
//                $product->setFbImgId($fb_img['img_id']);                
//               }
//     
//               /* используем вконтакте */
//               if ($website->getVkUse() === true) {            
//                 $vk = $this->get('vk_api');    
//                 $vk->authorize($website->getVkLogin(), $website->getVkPass());
//                 $vk_img_id = $product->getVkImgId();
//                  if ($vk_img_id > 0) {
//                      $vk->setParam($website->getVkGroup(), $product->getCategory()->getVkAlbum());
//                      $vk->photoDelete($vk_img_id, $product->getVkImgHash());
//                  }
//                 $vk->setParam($website->getVkGroup(), $category->getVkAlbum());
//                 $vk_img = $vk->photoAdd($data['photo_url'], $data['descr']);
//                 
//                 $product->setVkImgId($vk_img['img_id']);
//                 $product->setVkImgHash($vk_img['hash']);                 
//
//
//           }
           $fb_img_id = $product->getFbImgId();
           $vk_img_id = $product->getVkImgId();
          /* TODO: хранить $params в array в БД*/
           $params = array();
           if ($website->getFbUse() === true) {
               $params['fb'] = array(
                                    'login' => $website->getFbLogin(),
                                    'password' => $website->getFbPass(),
                                    'gid' => $website->getFbGroup(),
                                    'token' => $website->getFbToken(),
                                );
           }
           if ($website->getVkUse() === true) {
               $params['vk'] = array(
                                    'login' => $website->getVkLogin(),
                                    'password' => $website->getVkPass(),
                                    'gid' => $website->getVkGroup(),
                                );
           }
           
           $social = $this->get('social');
           $social->setParams($params);
           $social->photo('delete', array(
                                    'fb_album' => $product->getCategory()->getFbAlbum(),
                                    'fb_img_id' => $fb_img_id,
                                    'vk_img_id' => $vk_img_id,
                                    'vk_img_hash' => $product->getVkImgHash(),
                         ));   
           
           $resp = $social->photo('add', array(
                                    'photo_url' => $data['photo_url'],
                                    'descr' => $data['descr'],
                                    'fb_album' => $category->getFbAlbum(),
                                    'vk_album' => $category->getVkAlbum(),
                         ));
                 
          $product->setFbImgId($resp['fb']['fb_img_id']);                    

          $product->setVkImgId($resp['vk']['vk_img_id']);
          $product->setVkImgHash($resp['vk']['vk_img_hash']);
          
          $product->setDescr($data['descr']);  
          $product->setCategory($category);  
          $em->flush();
         $body = json_encode(array('status' => 'success'));
         return new Response($body, 200, array('Content-Type' => 'application/'.$_format));         
    }   

    public function facebookAuthAction(Request $request)
    {
        /* request: website, client_id, access_token */
       
        $data = $request->query->all() or $data = $request->request->all();
        $user = $this->checkCredentials($data); 
        
        $em = $this->getDoctrine()->getEntityManager();
        $website = $em->getRepository('LunyPartnerBundle:Website')->findOneByPartner($this->user->getId());
       
        $fb = $this->get('fb_api');
        $user = $fb->getFbUser(); // реализовать в API метод getUser
        
        if (!$user) {
             $fb = $this->get('fb_api');
             $login_url = $fb->getAuthUrl();
             return new RedirectResponse($login_url);
        } else {
             $website->setFbToken($fb->getToken());
             $em->flush();
             return new Response('success');
        }
    }
    
    
    public function messageShowAction(Request $request, $_format) 
    {
       /* request: website, client_id, access_token, dialog, page (optional) */
       // dialog = VK_user_ID переписку с которым надо получить
        $data = $request->query->all() or $data = $request->request->all();
        $this->checkCredentials($data);
        
        $website = $this->getDoctrine()->getRepository('LunyPartnerBundle:Website')
                                       ->findOneByPartner($data['client_id']);
        if (!$website) {
            throw $this->createNotFoundException('No site found for client id '.$data['client_id']);
        }
        $page = intval($request->query->get('page')) or $page = 1;
        $dialog = intval($request->query->get('dialog'));
        
                  /* TODO: хранить $params в array в БД*/
           $params = array();
           if ($website->getFbUse() === true) {
               $params['fb'] = array(
                                    'login' => $website->getFbLogin(),
                                    'password' => $website->getFbPass(),
                                    'gid' => $website->getFbGroup(),
                                    'token' => $website->getFbToken(),
                                );
           }
//           if ($website->getVkUse() === true) {
//               $params['vk'] = array(
//                                    'login' => $website->getVkLogin(),
//                                    'password' => $website->getVkPass(),
//                                    'gid' => $website->getVkGroup(),
//                                );
//           }
           
         
           ini_set('xdebug.var_display_max_depth', 7);
           $social = $this->get('social');
           $social->setParams($params);
           $resp = $social->message('show');   
           var_dump($resp['fb']['data']); exit;

        
        
        
        
        //$vk = $this->get('vk_api');    
        //$vk->authorize($website->getVkLogin(), $website->getVkPass());
        
        $em = $this->getDoctrine()->getManager();        
        
        $this->messageOneUpdate($vk, $website, $em, $count, $persons);
        $this->personProfilesUpdate($persons, $vk, $em);
        
        $em->flush();
        
        $messages = $em->createQuery(
                    'SELECT m.id as mid, m.read_state, m.author, m.time, m.text, u.fname, u.lname, u.avatar FROM LunyPartnerBundle:Message m '
                   .'JOIN LunyPartnerBundle:SocPerson u '
                   .'WHERE u.user_id = m.author AND m.website = :site AND m.author = :dialog '
                   .'ORDER BY m.time DESC'
                )
                ->setParameter('dialog', $dialog)
                ->setParameter('site', $website->getId())
                ->setFirstResult(($page - 1) * 10)
                ->setMaxResults(10)
                ->getScalarResult();
        
        if (empty($messages)) {
            return new Response('no messages found', 200, array('Content-Type' => 'application/'.$_format));
        }
        
        $mids = '';
        foreach ($messages as $val) {
            if ($val['read_state'] == 'no') {
                 if ($mids !== '') {$mids .= ', '; }
            $mids .= $val['mid'];
            }
        }

        if ($mids !== '') {
             $em->createQuery('UPDATE LunyPartnerBundle:Message m SET m.read_state = \'yes\' '
                              .'WHERE m.id IN ('.$mids.')')
                              ->execute();
        }
        
        $jsonMess = json_encode($messages);
        return new Response($jsonMess, 200, array('Content-Type' => 'application/'.$_format));
    }
    

    public function messageDialogsAction(Request $request, $_format) 
    {
       /* request: website, client_id, access_token, page (optional)  */
       // dialog = VK_user_ID переписку с которым надо получить
        $data = $request->query->all() or $data = $request->request->all();
        $this->checkCredentials($data);
        
        $website = $this->getDoctrine()->getRepository('LunyPartnerBundle:Website')
                                       ->findOneByPartner($data['client_id']);
        if (!$website) {
            throw $this->createNotFoundException('No site found for client id '.$data['client_id']);
        }
        $page = intval($request->query->get('page')) or $page = 1;
        
        $vk = $this->get('vk_api');    
        $vk->authorize($website->getVkLogin(), $website->getVkPass());
        
        $em = $this->getDoctrine()->getManager();
        
        $this->messageOneUpdate($vk, $website, $em, $count, $persons);
        $this->personProfilesUpdate($persons, $vk, $em);
        
        $preJsonDialogs = $this->getDoctrine()->getManager()->createQuery(
                    'SELECT m.author, u.fname, u.lname, u.avatar FROM LunyPartnerBundle:Message m '
                   .'JOIN LunyPartnerBundle:SocPerson u '
                   .'WHERE u.user_id = m.author AND m.website = :site '
                   .'GROUP BY m.author '
                   .'ORDER BY m.time DESC'
                )
                ->setParameter('site', $website->getId())
                ->setFirstResult(($page - 1) * 3)
                ->setMaxResults(3)
                ->getScalarResult();

        $jsonDialogs = json_encode($preJsonDialogs);
        return new Response($jsonDialogs, 200, array('Content-Type' => 'application/'.$_format));
         
    }    
    
    public function messageSendAction(Request $request, $_format) 
    {
       // /api/social/message/send.json?access_token=test&website=wonderfulengineering.com&client_id=1&text=vkontakte&user_id=242587873&social=vk
       /* request: website, client_id, access_token, to_id, text, social */
       // dialog = VK_user_ID переписку с которым надо получить
        $data = $request->query->all() or $data = $request->request->all();
        $this->checkCredentials($data);
        
        $website = $this->getDoctrine()->getRepository('LunyPartnerBundle:Website')
                                       ->findOneByPartner($data['client_id']);
        if (!$website) {
            throw $this->createNotFoundException('No site found for client id '.$data['client_id']);
        }
      
//         $vk = $this->get('vk_api');    
//         $vk->authorize($website->getVkLogin(), $website->getVkPass());

         $dialog = $this->getDoctrine()->getRepository('LunyPartnerBundle:Message')
                                       ->findOneBy(
                                            array(
                                                'author' => $data['user_id'],
                                                'website' => $data['client_id'],
                                            )
                                        );
         
         if (!$dialog) {
             throw $this->createNotFoundException('No dialog found for site '.$website->getDomain());
         }
         
      /* TODO: хранить $params в array в БД*/
       $params = array();
       if ($data['social'] == 'fb') {
           $params['fb'] = array(
                                'login' => $website->getFbLogin(),
                                'password' => $website->getFbPass(),
                                'gid' => $website->getFbGroup(),
                                'token' => $website->getFbToken(),
                            );
       }
       elseif ($data['social'] == 'vk') {
           $params['vk'] = array(
                                'login' => $website->getVkLogin(),
                                'password' => $website->getVkPass(),
                                'gid' => $website->getVkGroup(),
                            );
       }
         
        $social = $this->get('social');
        $social->setParams($params);
        
        $resp = $social->message('send', array(
                                        'user_id' => $data['user_id'],
                                        'text' => $data['text'],
                        )
                ); 
        
         //$response = $vk->messageSend($data['text'], $data['dialog'], $data['dialog']);
         /* проверка успешно ли отослано сообщение */    
        /*
         if (strstr($resp['vk'], 'error')) {
             return new Response($resp['vk'], 200, array('Content-Type' => 'application/'.$_format));
         }
         */
         $personResp = $social->person('adminInfo');
         $authorInfo = $personResp[$data['social']];
         
         $em = $this->getDoctrine()->getEntityManager();
             $newMess = new \Luny\PartnerBundle\Entity\Message();
             $newMess->setAuthor($authorInfo['id']);
             $newMess->setChatId($data['user_id']);
          // TODO: теперь ведь это отдельная сущность, сделать здесь её обновление 
          // $newMess->setAvatar($authorInfo['avatar']);
             $newMess->setText($data['text']);
             $newMess->setTime(time());
             $newMess->setReadState('yes');
             $newMess->setWebsite($website);
             $newMess->setSocial($data['social']);
         $em->persist($newMess);
         $em->flush();
         
        return new Response('success', 200, array('Content-Type' => 'application/'.$_format));         
    }
  
        
    public function messageUpdateAction() 
    {
      //$count = 0; // посчитаем сколько новых сообщений в итоге запишется в базу
      $em = $this->getDoctrine()->getManager();
      $vk = $this->get('vk_api');
      
      $websites = $em->createQuery(
            'SELECT w FROM LunyPartnerBundle:Website w WHERE w.vk_use = :use'
        )->setParameter('use', '1')->getResult();

      $newMessIds = array();
      
      foreach ($websites as $website) {
          // игнорируем тех пользователей сервиса, который неправильно указали данные для входа в VK
          try {
          $vk->authorize($website->getVkLogin(), $website->getVkPass());
          } catch (ApiException $e) {
              continue;
          }
          // обновление сообщений всех клиентов сервиса сводится к поочередному одиночному обновлению
          $ids = $this->messageOneUpdate($vk, $website, $em, $count, $persons);
          
      } // end  foreach ($websites as $website)
      
       // выполним запрос к базе на сохранение сообщений, составленный вызовами метода messageOneUpdate()
        $em->flush(); 
        
        // добавим новые или обновим существующие данные о пользователях вконтакте
        $this->personProfilesUpdate($persons, $vk, $em);

      return new Response('Added '.$count.' new messages.');
    }

    
    /**
     * Загрузка новых сообщений определенного по website-объекту клиента сервиса
     * 
     * @param Object $vk VK-service
     * @param Object $website client data
     * @param Object $em Doctrine entity-manager
     * @param Integer $count Счётчик кол-ва добавленных сообщений
     * @param Array $persons массив, который будет содержать ID пользователей VK, информацию о которых необходимо обновить
     */
    protected function messageOneUpdate($vk, $website, $em, &$count = 0, &$persons = array()) 
    {
        // узнаем VK ID профиля клиента сервиса
        $vkMid = $vk->getMid();
        
         // получим время последнего сохраненного сообщения
          // по этому времени будем сравнивать время сообщений полученных от VK
          // если время сообщений от VK превышает наше maxTime время, то будем сохранять такие сообщения
          $maxTime = $em->getRepository('LunyPartnerBundle:Message')
                        ->createQueryBuilder('m')
                        ->select('MAX(m.time)')
                        ->where('m.website = :site')
                        ->andWhere('m.author != :vkid')
                        ->setParameters(array(
                                              'site' => $website->getId(),
                                              'vkid' => $vkMid
                                             )
                                       )
                        ->getQuery()
                        ->getSingleScalarResult();

          // время, прошедшее с момента отправки сообщения до текущего момента в секундах 
          $time_offset = time() - $maxTime;
          // смещение, необходимое для выборки определенного подмножества сообщений
          $offset = 0;

          // запускаем цикл до тех пор, пока не пройдёмся по всем сообщениям время которых больше, 
          // чем максимальное время сообщения в базе (то есть новых сообщений)
          // сам цикл нужен, чтобы сохранять новые сообщения, если их кол-во больше 200
              while (true) {
                $vkResponse = json_decode($vk->messageList($time_offset, $offset), true);
                // выделяем отдельно из ответа массив полученных сообщений
                $messItems = $vkResponse['response']['items'];
                // узнаем кол-во полученных сообщений
                $amount = count($messItems);
                if ($amount === 0) {break;} 

                        foreach ($messItems as $mess) {
                          // если это сообщение от пользователя сервиса, то пропускаем его
                          if ($mess['user_id'] == $vkMid) {continue;}
                          $message = new \Luny\PartnerBundle\Entity\Message();
                          $message->setAuthor($mess['user_id']);
                          $message->setWebsite($website);
                          $message->setTime($mess['date']);
                          $message->setText($mess['body']);
                          $em->persist($message);
                          
                          // строим массив из ID для обновления SocPerson
                          $persons[] = $mess['user_id'];
                          
                          $count++;
                        }
                // если нам было отдано на текущем шаге 200 сообщений, 
                // то вероятно могут быть ещё новые сообщения в следующих 200
                // если нет, то значит получили все новые сообщения и выходим из цикла
                if ($amount < 200) {break;}
                else {$offset += 200;}
              } // end while (true)
    }
    
    /**
     * Обновляет базу информации об авторах сообщений (имя, фамилия, адрес аватарки)
     * 
     * @param Array $ids массив ID пользователей вконтакте, информацию о которых надо сохранить/обновить
     * @param Object $vk 
     * @param Object $em Doctrine entity-manager
     */
    protected function personProfilesUpdate($ids, $vk, $em) 
    {
       if (!empty($ids)) {
            $user = json_decode($vk->userGetInfo(array_unique($ids)), true);
            // подготавливаем данные для sql-запроса
            $values = '';
            foreach ($user['response'] as $info) {
                if ($values !== '') {$values .= ', ';}
                $values .= '(\''.$info['id'].'\', \''.$info['first_name'].'\', \''.$info['last_name'].'\', \''.$info['photo_100'].'\', \'VK\')';
            }
            // user_id у нас уникальный, поэтому используем replace into, однако построить такой запрос можно только напрямую
            $query = 'REPLACE INTO `social_person` (`user_id`,`fname`,`lname`,`avatar`,`type`) VALUES '.$values.';';
            $connection = $em->getConnection();
            $statement = $connection->executeQuery($query);
            $statement->execute();
        }  
    }
    
    
}