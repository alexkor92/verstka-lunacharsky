<?php

namespace Luny\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\UserBundle\Controller\RegistrationController as BaseController;


/**
 * Class RegistrationController
 */
class RegistrationController extends BaseController
{

    public function registerAction(Request $request=null)
    {
      $response = parent::registerAction($request);

      return $response;
    }
}
