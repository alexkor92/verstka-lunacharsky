<?php 
namespace Luny\SiteBundle\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Luny\SiteBundle\Entity\RouteUrlBijection;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
/**
 * My custom datatype.
 */
class TextWithUrlsType extends Type
{
    const TEXT_WITH_URLS = 'textwithurls'; // modify to match your type name
    
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'TEXT';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $text = base64_decode($value);
        $container = $this->getContainer($platform);
        $text = preg_replace_callback('/\(§\)(.*?)\(§\)/ius',
                                        function($match) use ($container) 
                                        {
                                            $data = unserialize($match[1]);
                                            $link = $container->get('router')->generate($data['route'], $data['params']);
                                            return $link;
                                        },
                                     $text);
        return $text;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        $container = $this->getContainer($platform);
        preg_match_all('/\shref="?([^"\']+)"?[^>]*>/ius', $value, $match);

        $text = $value;
        if (empty($match)) {
            return $text;
        }

            foreach ($match[1] as $url) {
                
                if (stristr($url, 'http://')) {
                    $alink = parse_url($url);
                    $site = str_replace('www.', '', $alink['host']);
                    $domains = $container->getParameter('domains');
                        if (in_array($site, $domains)) {$url = $alink['path'];} 
                        else {continue;}
                }

                try {
                    $route = $container->get('router')->match($url);
                } catch (ResourceNotFoundException $e) {continue;}
                
              $params = $route; unset($params['_controller']); unset($params['_route']);
              $link = array('route' => $route['_route'], 'params' => $params);
              $text = str_replace(' href="'.$url.'"', ' href="(§)'.serialize($link).'(§)"', $value);
            }

      return base64_encode($text);
     
            
          // здесь return странно ведет себя с html-сущностями...   
//        $text = preg_replace_callback('/<a[^>]+href=[\'"]?([^"\']+)[\'"]*[^>]+>/ius',
//                                        function($match) use ($container) {
//                                            $route = $container->get('router')->match($match[1]);
//                                              $params = $route;
//                                              unset($params['_controller']);
//                                              $sData = serialize($params);
//                                              return '(§)'.$sData.'(§)';
//                                        },
//               $value);
    }

    public function getName()
    {
        return self::TEXT_WITH_URLS;
    }    
    
    protected function getContainer($platform) 
    {
       $listeners = $platform->getEventManager()->getListeners();
       $ret = $listeners['prePersist'];
        foreach ($ret as $val) {
            if ($val instanceof \Luny\SiteBundle\Listener\TypeListener) {
                       return $val->getContainer();
            }       
        }

    }
}