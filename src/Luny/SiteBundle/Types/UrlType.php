<?php 
namespace Luny\SiteBundle\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Luny\SiteBundle\Entity\RouteUrlBijection;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
/**
 * My custom datatype.
 */
class UrlType extends TextWithUrlsType
{
    const URL = 'url'; // modify to match your type name
    
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $var = unserialize($value);
        $container = $this->getContainer($platform);
        if (is_array($var)) {
               $url = $container->get('router')->generate($var['route'], $var['params']);
        } else {
            $url = $var;
        }
        return $url;
    }

    public function convertToDatabaseValue($url, AbstractPlatform $platform)
    {
        $container = $this->getContainer($platform);
                
                if (stristr($url, 'http://')) {
                    $alink = parse_url($url);
                    $site = str_replace('www.', '', $alink['host']);
                    $domains = $container->getParameter('domains');
                        if (in_array($site, $domains)) {$url = $alink['path'];} 
                        else {
                            return serialize($url);
                            
                        }
                }

                try {
                    $route = $container->get('router')->match($url);
                } catch (ResourceNotFoundException $e) {
                    return serialize($url);
                }
                
              $params = $route; unset($params['_controller']); unset($params['_route']);
              $link = array('route' => $route['_route'], 'params' => $params);
              $text = serialize($link);
      return $text;
    }

    public function getName()
    {
        return self::URL;
    }    
    

}