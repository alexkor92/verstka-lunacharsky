<?php

namespace Luny\SiteBundle\Admin;
 
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin ;
 
class TechnologyAdmin extends Admin
{
    
    protected $formOptions = array(
    'validation_groups' => 'admin'
    );
   
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array('label' => 'Name'))
            ->add('icon', 'iphp_file', array('label' => 'Icon'))
            ->add('preview', 'textarea', array('label' => 'Preview', 'attr'=>array('rows'=>'4')))
            ->add('info', 'textarea', array('label' => 'Information', 'attr'=>array('class'=>'ckeditor')))
            ->add('keywords', 'text', array('label' => 'Keywords', 'required' => false))
            ->add('description', 'textarea', array('label' => 'Description', 'required' => false))
        ;
    }
  
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
        ;
    }
 
}