<?php

namespace Luny\SiteBundle\Admin;
 
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin ;
 
class CustomerAdmin extends Admin
{
   
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array('label' => 'Name'))
            ->add('organization', 'text', array('label' => 'Organization'))
            ->add('preview', 'textarea', array('label' => 'Brief info'))
            ->add('info', 'textarea', array('label' => 'Information', 'attr'=>array('class' => 'ckeditor')))
            ->add('site', 'text', array('label' => 'Site'))
            ->add('image', 'iphp_file', 
                            array('label' => 'Organization image', 'required' => true))                   
            //->add('response', 'textarea', array('label' => 'Response', 'attr'=>array('class' => 'ckeditor')))
            ->add('projects', 'sonata_type_model', 
                    array('expanded' => true, 'by_reference' => false, 'multiple' => true, 'required' => false))
            ->add('keywords', 'text', array('label' => 'Keywords', 'required' => false))
            ->add('description', 'textarea', array('label' => 'Description', 'required' => false))
        ;
    }
  
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('site')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('site')
            ->add('project_id')
        ;
    }
 
    
    
    public function prePersist($customer)
    {
        $organization = $customer->getOrganization();
        $letter = $this->generateLetter($organization);
        $customer->setLetter($letter);
    }

    public function preUpdate($customer)
    {
        $organization = $customer->getOrganization();
        $letter = $this->generateLetter($organization);
        $customer->setLetter($letter);   
    }    
    
    /**
     * Generation the first letter of the customer for the glossary page
     *  
     * @param string name
     * @return char the first letter of the organization
     */
    protected function generateLetter($name) 
    {
        preg_match('/[^A-zА-я]*([A-zА-я]){1}.*?/iu', $name, $match);
        
        if (isset($match[1])) {
            
            $letter = mb_strtolower($match[1]);
            
        } else {
          // exception  
        }
        return $letter;
    }
    
}