<?php

namespace Luny\SiteBundle\Admin;
 
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin ;
 
class FlashbackAdmin extends Admin
{
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                   ->add('image', 'iphp_file', array('label' => 'Image'))
                   ->add('text', 'textarea', array('label' => 'Text'))
                   ->add('date', 'date', array('years' => range(2007, 2050), 'format' => 'dd MMMM yyyy'))
                   ;
    }
  
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
//        $datagridMapper
//            ->add('name')
//        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('text');
    }
    
}