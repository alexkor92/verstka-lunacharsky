<?php

namespace Luny\SiteBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;

class ProjectAdmin extends Admin
{

    public function prePersist($project)
    {
        $this->preUpdate($project);
        $project->setTechnologies($project->getTechnologies());
        $project->setWorkers($project->getWorkers());
    }

    public function preUpdate($project)
    {
        $project->setTechnologies($project->getTechnologies());
        $project->setWorkers($project->getWorkers());
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $categories = $this->getConfigurationPool()
            ->getContainer()
            ->getParameter('project_types');

        $formMapper->add('title', 'text',
            array('label' => 'Title'))
            ->add('name', 'text',
                array('label' => 'Name'))
            ->add('category', 'choice',
                array('label' => 'Project type',
                    'choices' => $categories,
                    'required' => false,
                ))
            ->add('screen', 'iphp_file',
                array('label' => 'Screen', 'required' => false))
            ->add('problem', 'textarea',
                array('label' => 'Problem', 'attr' => array('rows' => '3'), 'required' => false))
            ->add('link', 'text',
                array('label' => 'Site', 'required' => false))
            ->add('design', 'textarea',
                array('label' => 'Design', 'attr' => array('rows' => '4', 'class' => 'ckeditor'), 'required' => false))
            ->add('programing', 'textarea',
                array('label' => 'Programing', 'attr' => array('rows' => '4', 'class' => 'ckeditor'), 'required' => false))
            ->add('promotion', 'textarea',
                array('label' => 'Promotion', 'attr' => array('rows' => '4', 'class' => 'ckeditor'), 'required' => false))
            ->add('resp_title', 'text',
                array('label' => 'Response title', 'attr' => array('placeholder' => 'Выражаю благодарность'), 'required' => false))
            ->add('resp_text', 'textarea',
                array('label' => 'Response content', 'attr' => array('rows' => '4', 'class' => 'ckeditor'), 'required' => false))
            ->add('resp_auth', 'text',
                array('label' => 'Response author', 'required' => false))
            ->add('resp_pos', 'text',
                array('label' => 'Response author position', 'required' => false))
            ->add('goverment', 'choice',
                array('label' => 'Federal order?',
                    'choices' => array('0' => 'Нет', '1' => 'Да'),
                    'data' => 0,
                    'required' => true, 'expanded' => true))
            ->add('gover_icon', 'iphp_file',
                array('label' => 'Goverment icon', 'required' => false))
            ->add('customer', 'entity',
                array('class' => 'Luny\SiteBundle\Entity\Customer', 'required' => false))
            ->add('workers', 'sonata_type_model',
                array('by_reference' => false, 'multiple' => true, 'label' => 'Project Team', 'required' => false))
            ->add('technologies', 'sonata_type_model',
                array('by_reference' => false, 'multiple' => true, 'required' => false))
            ->add('rel_year', 'text',
                array('label' => 'Year', 'required' => false))
            ->add('keywords', 'text',
                array('label' => 'Keywords', 'required' => false))
            ->add('description', 'textarea',
                array('label' => 'Description', 'required' => false));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
            ->add('title')
            ->add('link');
    }


}