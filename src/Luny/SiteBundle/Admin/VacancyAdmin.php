<?php

namespace Luny\SiteBundle\Admin;
 
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin ;
 
class VacancyAdmin extends Admin
{
    
    protected $formOptions = array(
           'validation_groups' => 'admin'
    );
   
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', 'text', array('label' => 'Title'))
            ->add('image', 'iphp_file', array('label' => 'Image'))
            ->add('preview', 'textarea', array('label' => 'Preview'))
            ->add('descr', 'textarea', array('label' => 'Information', 'attr'=>array('class'=>'ckeditor')))
        ;
    }
  
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
        ;
    }
 
}