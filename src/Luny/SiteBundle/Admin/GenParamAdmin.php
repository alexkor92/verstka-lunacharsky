<?php

namespace Luny\SiteBundle\Admin;
 
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin ;
use Sonata\AdminBundle\Route\RouteCollection;

class GenParamAdmin extends Admin
{
   
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('email', 'text', array('label' => 'Email'))
            ->add('phone', 'textarea', array('label' => 'Contact Phone(s)', 'required' => false))
            ->add('counter', 'textarea', array('label' => 'Counter Code', 'required' => false))
            ->add('handling', 'checkbox', 
                            array('label' => 'Technical works',
                                  'required' => false,
                        ))
        ;
    }
  
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
//        $datagridMapper
//            ->add('name')
//        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
        ;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
         $collection
              ->remove('create')
              ->remove('delete')
              ->remove('list')
         ;
    }       
 
}