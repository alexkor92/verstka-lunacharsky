<?php

namespace Luny\SiteBundle\Admin;
 
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin ;
 
class MenuAdmin extends Admin
{
    
    protected $formOptions = array(
    'validation_groups' => 'admin'
    );
    
    protected $datagridValues = array(
        '_sort_by' => 'place, type'  // name of the ordered field
    );
    
   
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('title', 'text', array('label' => 'Name'))
                ->add('url', 'text', array('label' => 'Path URL', 
                                           'attr' => array('placeholder' => '/page/example')
                                           )
                      )
                ->add('place', 'integer', array('label' => 'Place'))                
                ->add('type', 'choice', 
                            array('label' => 'Type', 
                                      'choices' => array(
                                          'horizontal' => 'Главное верхнее',
                                          'on_project_page' => 'Меню под проектами',
                                         ),
                                  'required' => true,
                        ))
        ;
    }
  
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                  ->addIdentifier('title')                 
                  ->add('place')
                  ->add('type')
        ;
    }
    
    public function postPersist($menu)
    {
         $this->checkUrlExisting($menu->getUrl());
        
    }
    
    public function postUpdate($menu)
    {
         $this->checkUrlExisting($menu->getUrl());
    }    
    
    protected function checkUrlExisting($path) 
    {
        $domains = $this->getConfigurationPool()->getContainer()->getParameter('domains');
        $url = 'http://'.$domains[0].$path;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        if ($code == 404) 
        { 
            $this
                    ->getRequest()
                    ->getSession()
                    ->getFlashBag()
                    ->add('page_warning', 'ВНИМАНИЕ: страницы с адресом <a href="'.
                            $url.'" target="_blank">'.
                            $url.'</a> не существует. Рекомендуем создать эту страницу для стабильной работы сайта')
                ;
        }
        elseif ($code != 200) 
        {
             $this
                    ->getRequest()
                    ->getSession()
                    ->getFlashBag()
                    ->add('page_warning', 'ВНИМАНИЕ: страница с адресом <a href="'.
                            $url.'" target="_blank">'.
                            $url.'</a> вернула код ответа <b>'.$code.'</b>')
                ;
        }
        
        curl_close($curl);
    }
            
    
}