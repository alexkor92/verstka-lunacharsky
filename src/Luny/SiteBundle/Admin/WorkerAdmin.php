<?php

namespace Luny\SiteBundle\Admin;
 
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin ;
 
class WorkerAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('fname', 'text', array('label' => 'First Name'))
            ->add('lname', 'text', array('label' => 'Last Name'))
            ->add('position', 'text', array('label' => 'Position'))
            ->add('photo', 'iphp_file', array('label' => 'Photo'))
            ->add('info', 'textarea', array('label' => 'Information', 'attr'=>array('class'=>'ckeditor')))
            ->add('quote', 'textarea', array('label' => 'Quote', 'attr'=>array('class'=>'ckeditor')))
            ->add('preview', 'textarea', array('label' => 'Popup preview', 'required' => false))
            ->add('keywords', 'text', array('label' => 'Keywords', 'required' => false))
            ->add('description', 'textarea', array('label' => 'Description', 'required' => false))
            ->add('dismiss', 'choice', array(
                'choices'   => array('0' => 'Работает', '1' => 'Уволен'),
                'required'  => false,
                'label'=>'Dismiss'));
    }
  
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('lname')
            ->add('position');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('fname', 'text', array('label'=>'Имя'))
            ->addIdentifier('lname', 'text', array('label'=>'Фамилия'))
            ->addIdentifier('position', 'text', array('label'=>'Должность'))
            ->addIdentifier('dismiss', 'choice', array('choices'   => array('0' => 'Работает', '1' => 'Уволен'), 'label'=>'Статус'));
    }
 
}