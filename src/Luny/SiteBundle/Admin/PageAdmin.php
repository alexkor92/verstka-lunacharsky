<?php

namespace Luny\SiteBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\AdminBundle\Validator\ErrorElement;
 
class PageAdmin extends Admin
{
   
    protected $maxPerPage = 250;
    protected $maxPageLinks = 250;

    protected $formOptions = array(
                                    'validation_groups' => array()
                                  );
    
    public function createQuery($context = 'list')
    {
        $em = $this->modelManager->getEntityManager('Luny\SiteBundle\Entity\Page');

        $queryBuilder = $em->createQueryBuilder('p')
                           ->select('p')
                           ->from('LunySiteBundle:Page', 'p')
                           ->where('p.parent IS NOT NULL')
                           ->orderBy('p.root, p.lft', 'ASC');

        $query = new ProxyQuery($queryBuilder);
        return $query;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
     $listMapper->add('up', 'text', array('template' => 'LunySiteBundle:admin:field_tree_up.html.twig', 'label'=>' '))
                ->add('down', 'text', array('template' => 'LunySiteBundle:admin:field_tree_down.html.twig', 'label'=>' '))
                //->add('id', null, array('sortable'=>false))
                ->addIdentifier('laveled_title', 'text', array('sortable'=>false, 'label'=>'Название страницы'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array()
                    ), 'label'=> 'Действия'
                ));

    }

    protected function configureFormFields(FormMapper $form)
    {
        $subject = $this->getSubject();
        $id = $subject->getId();

        $form->with('Общие')
            ->add('parent', null,
                array(
                    'label' => 'Раздел',
                    'required' => true,
                    'property' => 'laveled_title',
                    'query_builder' =>  function($er) use ($id) {
                        $qb = $er->createQueryBuilder('p');
                        if ($id){
                            $qb->where('p.id <> :id')->setParameter('id', $id);
                        }
                        $qb->orderBy('p.root, p.lft', 'ASC');
                        return $qb;
                    })
                )
            ->add('title', 'text', array('label' => 'Заголовок'))
            ->add('name', 'text', array('label' => 'Название', 'attr'=>array('data-origin'=>'page_name')))
            ->add('url', 'text', array('label' => 'Название в URL', 'attr'=>array('data-tran'=>'page_path')))
            ->add('keywords', 'text', array('label' => 'Keywords'))
            ->add('description', 'text', array('label' => 'Description'))
            ->add('content', 'textarea', array('label' => 'Текст страницы', 'attr'=>array('rows'=>'4', 'class' => 'ckeditor')))
            ->end();
    }

    public function preRemove($page)
    {
        $em = $this->modelManager->getEntityManager($page);
        $repo = $em->getRepository("LunySiteBundle:Page");
        $subtree = $repo->childrenHierarchy($page);
        foreach ($subtree AS $el) {
            $menus = $em->getRepository('LunySiteBundle:AdditionalMenu')
                        ->findBy(array('page'=> $el['id']));
            foreach ($menus AS $m){
                $em->remove($m);
            }
            $services = $em->getRepository('LunySiteBundle:Service')
                           ->findBy(array('page'=> $el['id']));
            foreach ($services AS $s){
                $em->remove($s);
            }
            $em->flush();
        }

        $repo->verify();
        $repo->recover();
        $em->flush();
    }

    public function postPersist($page)
    {
        $user = $this->getConfigurationPool()->getContainer()
                     ->get('security.context')->getToken()->getUser();
        $page->setUserCreate($user);
   
        $em = $this->modelManager->getEntityManager($page);
        $repo = $em->getRepository("LunySiteBundle:Page");

        $way = implode($repo->getPath($page));
        $page->setWay($way);
             
        $repo->verify();
        $repo->recover();
        $em->flush();
    }

    public function postUpdate($page)
    {
        $user = $this->getConfigurationPool()->getContainer()
                     ->get('security.context')->getToken()->getUser();
        $page->setUserUpdate($user);
        
        $em = $this->modelManager->getEntityManager($page);
        $repo = $em->getRepository("LunySiteBundle:Page");

        $way = implode($repo->getPath($page));
        $page->setWay($way);
        
        $repo->verify();
        $repo->recover();
        $em->flush();
    }

    
    public function setSecurityContext($securityContext) {
        $this->securityContext = $securityContext;
    }

    public function getSecurityContext() {
        return $this->securityContext;
    }

    
    public function validate(ErrorElement $errorElement, $page)
    {
            //throw new \Exception(__METHOD__); /* check: is enable the validator for the form */

//        $errorElement->with('title')->assertMinLength(array('limit' => 3))->end();
//        $errorElement->with('name')->assertMinLength(array('limit' => 3))->end();
        
        if (preg_match('/[^A-Z^a-z^0-9^-^_]/', $page->getUrl())) {
            $errorElement->with('url')
                         ->addViolation('Url may contain only a-z, -, _')->end();
        }
    }
    
    public function toString($page) {
        return $page->getTitle();
    }
 
}