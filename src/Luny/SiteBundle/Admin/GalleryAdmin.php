<?php

namespace Luny\SiteBundle\Admin;
 
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin ;
 
class GalleryAdmin extends Admin
{
    
    protected $formOptions = array(
    'validation_groups' => 'admin'
    );
   
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('image', 'iphp_file', array('label' => 'Изображение'))
                   ->add('name', 'text', array('label' => 'Name'))
                   ->add('descr', 'textarea', array('label' => 'About', 'required' => false));
    }
  
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }
    
    
    public function postPersist($gallery)
    {
        $image = $gallery->getImage();
        $this->genMini($image['path'], $image['fileName']);
    }

    public function postUpdate($gallery)
    {
        $image = $gallery->getImage();
        $this->genMini($image['path'], $image['fileName']);        
    }    
 
    /**
     * To generate the preview image
     * @param type $path path to the image file
     * @param type $name name of the image file
     */
    private function genMini($loc, $name) {
        $dir = $this->getConfigurationPool()->getContainer()->getParameter('kernel.root_dir');
        $path = $dir.'/../web'.$loc;
        $f = 'jpg';
	$width = 80;
	$height = 80;
	$rgb = 0xFFFFFF; // background color
            $size = getimagesize($path);
            $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
            $icfunc = "imagecreatefrom" . $format;
            $x_ratio = $width / $size[0];
            $y_ratio = $height / $size[1];
            $ratio = min($x_ratio, $y_ratio);
            $use_x_ratio = ($x_ratio == $ratio);
            $new_width = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
            $new_height = !$use_x_ratio ? $height : floor($size[1] * $ratio);
            $new_left = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
            $new_top = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);
            $isrc = $icfunc($path);
            $idest = imagecreatetruecolor($width, $height);
            imagefill($idest, 0, 0, $rgb);
            imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0,
            $new_width, $new_height, $size[0], $size[1]);
            $mini_file = str_replace($name, '', $path).'/../mini'.$name;
	imagejpeg($idest, $mini_file);
	imagedestroy($isrc);
	imagedestroy($idest);
    }
    
}