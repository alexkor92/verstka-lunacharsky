<?php

namespace Luny\SiteBundle\Admin;
 
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin ;
 
class BlogAdmin extends Admin
{
   
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', 'text', array('label' => 'Title'))
            ->add('name', 'text', array('label' => 'Name'))
            ->add('keywords', 'text', array('label' => 'Keywords'))
            ->add('description', 'text', array('label' => 'Description'))
            ->add('preview', 'textarea', array('label' => 'Preview', 'attr'=>array('rows'=>'4')))
            ->add('illustration', 'iphp_file', array('label' => 'Illustration'))
            ->add('content', 'textarea', array('label' => 'Content', 'attr'=>array('class'=>'ckeditor')))
            ->add('worker', 'entity', array('class' => 'Luny\SiteBundle\Entity\Worker', 'label'=>'Author'))
            ->add('branch', 'entity', array('class' => 'Luny\SiteBundle\Entity\BranchBlog', 'required' => false, 'label'=>'Branch'))
        ;
    }
  
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('branch')
            ->add('worker')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', 'text', array('label' => 'Заголовок'));
    }
 
}