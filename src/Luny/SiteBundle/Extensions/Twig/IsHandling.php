<?php

namespace Luny\SiteBundle\Extensions\Twig;

class IsHandling extends \Twig_Extension
{
    private $em;

//    public function getFunctions() {
//    return array(
//        'is_handling' => new Twig_Function_Method($this, 'handling')
//    );
//}

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('is_handling', array($this, 'handling')),
        );
    }


    public function handling()
    {
        //var_dump($this->container); exit;
        return $this->em
                    ->getRepository('LunySiteBundle:GenParam')
                    ->findOneByName('luny')
                    ->getHandling()
               ;
    }

    public function getName()
    {
        return 'is_handling';
    }
    
    public function __construct($em)
    {
        $this->em = $em;
    }    

}