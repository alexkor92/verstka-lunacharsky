<?php

namespace Luny\SiteBundle\Extensions\Twig;

use petrovich\petrovich;


/**
 * Twig - Custom fucntion для применения склонения по падежам (в данном случае родительного)
 * применяется на странице талантов в popup-окне для ссылки "Посетить страницу {ИМЯ_СОТРУДНИКА_В_Род._падеже}" 
 * класс можно дописать и под другие падежи при необходимости
 */
class PetrovichGen extends \Twig_Extension
{

    public function getFilters() 
    {
        return array(
            new \Twig_SimpleFilter('nametogenitive', array($this, 'getGenitive')),
        );
    }


    public function getGenitive($name)
    {
        $petrovich = new Petrovich();
        return $petrovich->firstname($name, Petrovich::CASE_DATIVE);
    }

    public function getName()
    {
        return 'cases_helper';
    }

}