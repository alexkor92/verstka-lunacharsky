<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class TechnologyController extends Controller
{
    
    
    public function indexAction()
    {
        $data = $this->getDoctrine()
                   ->getRepository('LunySiteBundle:Technology')
                   ->findAll();
        
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('проекты', $this->get('router')->generate('project_list'));
        $breadcrumbs->addItem('технологии');
        
        return $this->render('LunySiteBundle:Technology:index.html.twig', 
                              array('technologies' => $data)
                            );
        
    }
    
    public function viewAction($name)
    {
        $rep = $this->getDoctrine()->getRepository('LunySiteBundle:Technology');
        $data = $rep->findOneByName($name);
        $tList = $rep->findAll();

        if (!$data) {
            throw $this->createNotFoundException('No technology found!');
        }

        
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('технологии', $this->get('router')->generate('technology_list'));
        $breadcrumbs->addItem($data->getName());
        
        return $this->render('LunySiteBundle:Technology:view.html.twig', 
                              array(
                                'technology' => $data,
                                'technologies' => $tList,
                               )
                            );
    }
    
}