<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class CustomerController extends Controller
{
    
    private $langs_labels = array('ru' => 'Русский', 'en' => 'Английский');
    
    private $letters = array(
                            'ru' => array("а","б","в","г","д","е","ё","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ч","ш","щ","ы","э","ю","я"),
                            'en' => array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z")
                            );

    
    public function indexAction()
    {
        $data = $this->getDoctrine()
                     ->getRepository('LunySiteBundle:Customer')
                     ->findAll();
                
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('клиенты');

        return $this->render('LunySiteBundle:Customer:index.html.twig', 
            array('customers' => $data)
        );
    }
    
    public function glossaryAction(Request $request) 
    {
        $lang = $request->query->get('lang');
        $language = (!empty($lang) && isset($this->letters[$lang])) ? $lang : 'ru';

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));

        $breadcrumbs->addItem('клиенты', $this->get('router')->generate('customer_list'));
        $breadcrumbs->addItem('глоссарий');

        $availableLets = $this->getDoctrine()->getRepository('LunySiteBundle:Customer')->createQueryBuilder('c')
            ->select('DISTINCT c.letter')
            ->where('c.letter <> \'\'')
            ->getQuery()
            ->getArrayResult();
                            
        $available = array();
        foreach ($availableLets as $val)
        {
            $available[] = $val['letter'];
        }
        //var_dump($available); exit;
                           
        return $this->render('LunySiteBundle:Customer:glossary.html.twig',
            array(
                'lets' => $this->letters,
                'langs_labels' => $this->langs_labels,
                'lang' => $language,
                'available' => $available
            )
        );
    }
    
    public function glossaryAjaxAction(Request $request) 
    {
        $lang = $request->query->get('lang');
        $language = (!empty($lang) && isset($this->letters[$lang])) ? $lang : 'ru';
        $lid = intval($request->query->get('id'));
        $letter = $this->letters[$language][$lid];
        //if (empty($lid)) {$lid = 0;}
       
        $data = $this->getDoctrine()->getRepository('LunySiteBundle:Customer')->createQueryBuilder('c')
            ->andWhere('c.letter LIKE :letter')
            ->setParameter('letter', $letter)
            ->getQuery()
            ->getResult();

        return $this->render('LunySiteBundle:Customer:ajax_glossary.html.twig',
            array(
                'letter' => $letter,
                'customers' => $data,
            )
        );
    }
    
    public function viewAction($id)
    {
        $data = $this->getDoctrine()
                     ->getRepository('LunySiteBundle:Customer')
                     ->find($id);


        if (!$data) {
            throw $this->createNotFoundException('No customer found!');
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('клиенты', $this->get('router')->generate('customer_list'));
        $breadcrumbs->addItem($data->getName());

        return $this->render('LunySiteBundle:Customer:view.html.twig', 
            array(
                'customer' => $data,
                'projects' => $data->getProjects()
            )
        );
    }
}