<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{
    public function mainAction($type, $class = null, $active = null)
    {
        $data = $this->getDoctrine()
            ->getRepository('LunySiteBundle:Menu')
            ->findBy(
                array('type' => $type),
                array('place' => 'ASC')
            );

        $products = $this->getDoctrine()->getRepository('LunySiteBundle:Page')
            ->findOneByUrl('products');

        $workers = $this->getDoctrine()->getRepository('LunySiteBundle:Worker')
            ->findAll();

        $flashback = $this->getDoctrine()->getRepository('LunySiteBundle:Flashback')
            ->findAll();

        $wanted = $this->getDoctrine()->getRepository('LunySiteBundle:Vacancy')
            ->findAll();
        
        return $this->render('LunySiteBundle:Menu:menu_'.$type.'.html.twig',
            array(
                'data' => $data,
                'class' => $class,
                'active' => $active,
                'products' => $products->getContent(),
                'workers' => $workers,
                'flashback' => $flashback,
                'wanted' => $wanted
            )
        );
    }
}
