<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Luny\SiteBundle\Helpers\Paginator;
use Symfony\Component\HttpFoundation\Request;

class WorkerController extends Controller
{
    /*
     * @var integer количество выводимых сотрудников на странице в actions index и ajaxSlider
     */
    private $amount = 4;
    
    public function indexAction()
    {
        $rep = $this->getDoctrine()
            ->getRepository('LunySiteBundle:Worker');
        
        $total = $rep->createQueryBuilder('w')
            ->select('COUNT(w)')
            ->where('w.dismiss = 0')
            ->getQuery()
            ->getSingleScalarResult();

        $pages = ceil($total / $this->amount);

        $data = $rep->findBy(array('dismiss' => '0'), array(), $this->amount, 0);   // where, order by, limit, offset

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('о компании', $this->get('router')->generate('page_view', array('way' => '/about')));
        $breadcrumbs->addItem('сотрудники');
        
        return $this->render('LunySiteBundle:Worker:index.html.twig', 
            array(
                'workers' => $data,
                'pages' => $pages,
            )
        );
    }
    
//    public function ajaxPersonAction($id)
//    {
//        $data = $this->getDoctrine()
//            ->getRepository('LunySiteBundle:Worker')
//            ->findOneById($id);
//
//        return $this->render('LunySiteBundle:Worker:ajax_person.html.twig',
//            array('worker' => $data)
//        );
//    }
    
    // здесь ajax пагинация
    public function ajaxSliderAction(Request $request)
    {
        $page = intval($request->query->get('pageid'));
       
        $em = $this->getDoctrine()->getManager();
        $count = $em->createQuery('SELECT COUNT(w) FROM Luny\SiteBundle\Entity\Worker w WHERE w.dismiss = 0')
                     ->getSingleScalarResult();
         
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT w FROM Luny\SiteBundle\Entity\Worker w WHERE w.dismiss = 0");
        $query->setMaxResults($this->amount)->setFirstResult($page*$this->amount); //уже есть в хэлпере Paginator

        $data = $query->getResult();
        
        return $this->render('LunySiteBundle:Worker:ajax_slider.html.twig', 
                              array('workers' => $data)
                            );
        
    }
    
    public function ajaxArticlesAction($id, Request $request)
    {
        $page = intval($request->query->get('page'));
         
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT b.id, b.title, b.preview FROM Luny\SiteBundle\Entity\Blog b WHERE b.worker = :author");
        $query->setParameter('author', $id);
        $query->setMaxResults(3)->setFirstResult(($page-1)*3);

        $data = $query->getResult();
        
        return $this->render('LunySiteBundle:Worker:ajax_articles.html.twig', 
                              array('blogs' => $data)
                            );
        
    }    
    
    public function viewAction($id, $page)
    {

        $worker = $this->getDoctrine()
                       ->getRepository('LunySiteBundle:Worker');

        $data = $worker->createQueryBuilder('w')
            ->select('w')
            ->where('w.dismiss = 0 and w.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();

        if (!$data)
            throw $this->createNotFoundException('No worker found!');

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT b.id, b.title, b.preview FROM Luny\SiteBundle\Entity\Blog b WHERE b.worker = :author");
        $query->setParameter('author', $id);
        $query->setMaxResults(3)->setFirstResult(($page-1)*3);

        // С помощью подсказок указываем Doctrine, что нужно подключить нашего обходчика
        // и устанавливаем флаг, обозначающий, что нашему запросу необходимо будет подсчитать общее количество строк
//        $query->setHint('\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER',
//                        'Luny\\SiteBundle\\Entity\\Walkers\\MysqlPaginationWalker'
//                       );
//        $query->setHint("mysqlWalker.sqlCalcFoundRows", true);
        // Получаем посчитанное количество записей, возвращённых предыдущим запросом
        //$totalCount = $em->getConnection()->query('SELECT FOUND_ROWS()')->fetchColumn(0);

        $blogs = $query->getResult();
       
         $count = $em->createQuery('SELECT COUNT(b.id) FROM Luny\SiteBundle\Entity\Blog b WHERE b.worker = :author')
                     ->setParameter('author', $id)
                     ->getSingleScalarResult();
         
         
         
         if ($count != 0) {
             $pages = $this
                           ->get('helper.paginator')
                           ->paginate(
                                    'worker_ajax_articles', 
                                    array('id' => $id),
                                    'page', // имя параметра отвечающего за номер текущей страницы
                                    $count, //количество записей
                                    $page-1, //id странички, наш параметр {page}
                                    3, // кол-во отображаемых элементов на одной страничке
                                    10
                                )
                            ;
         } else {
             $pages = null;
         }
        
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('о компании', $this->get('router')->generate('page_view', array('way' => '/about')));
        $breadcrumbs->addItem('сотрудники', $this->get('router')->generate('worker_list'));
        $breadcrumbs->addItem($data->getFname().' '.$data->getLname());

        return $this->render('LunySiteBundle:Worker:view.html.twig', 
                              array(
                                   'worker' => $data,
                                   'blogs' => $blogs,
                                   'pages' => $pages,
                            )
             );
    }
    
}