<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class ProjectController extends Controller
{
    public function indexAction($category = null)
    {
        $rep = $this->getDoctrine()->getRepository('LunySiteBundle:Project');

        $categories = null;
        
        if ($category) {
            $categories = $this->container->getParameter('project_types');
            if (!array_key_exists($category, $categories)) {
                 throw $this->createNotFoundException('No page found!');
            }
            $data = $rep->findList($category);
            $type = 'projects_'.$category;
            $amount_pages = $rep->findAmountPage($category);
        } else {
            $data = $rep->findAll();
            $type = null;
            $amount_pages = 0;
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        if($category)
            $breadcrumbs->addItem($categories[$category], $this->get('router')->generate('project_type', array('category' => $category)));
        else
            $breadcrumbs->addItem('проекты');

        return $this->render('LunySiteBundle:Project:index.html.twig', 
            array(
                'projects' => $data,
                'page' => $type,
                'category' => $category,
                'pages' => $amount_pages
            )
        );
        
    }

    public function viewAction($id)
    {
        $data = $this->getDoctrine()
            ->getRepository('LunySiteBundle:Project')
            ->find($id);

        if (!$data) {
            throw $this->createNotFoundException('No project found!');
        }

        $categories = $this->container->getParameter('project_types');

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem($categories[$data->getCategory()], $this->get('router')->generate('project_type', array('category' => $data->getCategory())));
        $breadcrumbs->addItem($data->getTitle());
        
        return $this->render('LunySiteBundle:Project:view.html.twig', 
            array(
                'pr' => $data, // pr is project
                'team' => $data->getWorkers()
            )
        );
    }
    
    public function govAction() {

       $data = $this->getDoctrine()
            ->getRepository('LunySiteBundle:Project')
            ->findByGoverment(1);
        
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('Государственным учреждениям');        
        
        return $this->render('LunySiteBundle:Project:gov.html.twig',
            array(
                'orders' => $data,
                //'s' => $,
            )
        );
    }

    public function ajaxAction($category = null, $start = 0)
    {
        $doct = $this->getDoctrine();

        $data = $doct->getRepository('LunySiteBundle:Project')
            ->findList($category, $start);

        return $this->render('LunySiteBundle:Project:ajax.html.twig',
            array(
                'projects' => $data
            )
        );
    }
}