<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Luny\SiteBundle\Entity\Mail;

class MailController extends Controller
{

    public function formAction(Request $request) 
    {
        $mail = new Mail();
        
        $form = $this->createFormBuilder($mail)
                     ->add('text', 'textarea', array('label' => 'Текст сообщения: ', 'attr' => array(
                                                                                                  'class' => 'row',
                                                                                                  'required' => 'required',
                                                                                                  'id' => 'cb-text',
                                                                                                )))
                     ->add('email', 'email', array('label' => 'Ваша электронная почта: ', 'attr' => array(
                                                                                                    'class' => 'row', 
                                                                                                    'type' => 'email',
                                                                                                    'required' => 'required',
                                                                                                    'id' => 'cb-mail',
                                                                                                )))
                     ->getForm();
                
                
       if ($request->getMethod() == 'POST') {
           $form->bind($request);
           
           if ($form->isValid()) {

                $data = $request->query->all() or $data = $request->request->all();
                $this->sendMail($data['form']['email'], $data['form']['text']);

                return $this->render('LunySiteBundle:Mail:feedback_success.html.twig' );                

            } 
        
       }
       
      return $this->render('LunySiteBundle:Mail:feedback.html.twig', 
                                          array(
                                              'form' => $form->createView(),
                                           )
                                );
    }
    
       
    private function sendMail($email, $text)
    {

        
           $mail = new Mail();
           $mail->setEmail($email);
           $mail->setText($text);

           $em = $this->getDoctrine()->getManager();
           $em->persist($mail);
           $em->flush(); 
           
	   $subject='=?UTF-8?B?'.base64_encode('LUNY.RU: Вопрос из формы обратной связи').'?=';
           $headers="From: <{$email}>\r\n".
					"Reply-To: {$email}\r\n".
					"MIME-Version: 1s.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

           $to = $this->getDoctrine()
                      ->getRepository('LunySiteBundle:GenParam')
                      ->findOneByName('luny')
                      ->getEmail();

           mail($to, $subject, $text, $headers);
        
    }
    
}