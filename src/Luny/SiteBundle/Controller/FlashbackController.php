<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FlashbackController extends Controller
{
    public function indexAction()
    {
        $flashback = $this->getDoctrine()
            ->getRepository('LunySiteBundle:Flashback')
            ->findBy(
                array(),
                array('date' => 'ASC')
            );

        
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('о компании', $this->get('router')->generate('page_view', array('way' => '/about')));
        $breadcrumbs->addItem('флешбэк');
        
        return $this->render('LunySiteBundle:Flashback:index.html.twig', 
            array(
                'flashback' => $flashback,
            )
        );
    }
}
