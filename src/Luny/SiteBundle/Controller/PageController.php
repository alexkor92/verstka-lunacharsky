<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT w.fname, w.lname, w.photo, w.position, w.quote, RAND() as HIDDEN rand
                                  FROM Luny\SiteBundle\Entity\Worker w WHERE w.dismiss = '0'
                                  ORDER BY rand");
        if ($query)
            $worker = $query->setMaxResults(1)->getSingleResult();
        else
            $worker = null;

        return $this->render('LunySiteBundle:Page:index.html.twig', array('worker' => $worker));
    }
    
    public function viewAction($way)
    {
        $repo = $this->getDoctrine()->getRepository('LunySiteBundle:Page');
        $data = $repo->findOneByWay($way);

        if (!$data)
            throw $this->createNotFoundException('No page found!');

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));

        $pages = array(
            'promotion',
            'hosting',
            'domains'
        );

        if(in_array($data->getUrl(), $pages))
            $breadcrumbs->addItem('электрокоммерция', $this->get('router')->generate('project_type', array('category' => 'elcom')));

        if($data->getUrl() != 'products')
            $breadcrumbs->addItem($data->getTitle());
        else
            $breadcrumbs->addItem('система управления сайтом');

        return $this->render('LunySiteBundle:Page:view.html.twig', array('page' => $data));
    }
}
