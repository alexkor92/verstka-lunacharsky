<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SeoBlockController extends Controller
{
    
    public function mainAction($page)
    {

       $data = $this->getDoctrine()
                    ->getRepository('LunySiteBundle:SeoBlock')
                    ->findOneByPage($page);
        
        return $this->render('LunySiteBundle::blocks.html.twig', 
                              array(
                                  'title' => $data->getTitle(),
                                  'description' => $data->getDescription(),
                                  'keywords' => $data->getKeywords(),
                               )
                            );
        
        
    }
    
}
