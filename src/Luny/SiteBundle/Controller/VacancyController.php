<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class VacancyController extends Controller
{
    public function indexAction($page = 1)
    {
        $doct = $this->getDoctrine();

        $data = $doct->getRepository('LunySiteBundle:Vacancy')
            ->findList();

        $amount_pages = $doct->getRepository('LunySiteBundle:Vacancy')
            ->findAmountPage();
       
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('о компании', $this->get('router')->generate('page_view', array('way' => '/about')));
        $breadcrumbs->addItem('wanted');

        return $this->render('LunySiteBundle:Wanted:index.html.twig', 
            array(
                'vacancies' => $data,
                'pages' => $amount_pages
            )
        );

    }
    
    public function viewAction($id)
    {
        $data = $this->getDoctrine()
            ->getRepository('LunySiteBundle:Vacancy')
            ->findOneById($id);

        if (!$data) {
            throw $this->createNotFoundException('No vacancy found!');
        }
        
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('о компании', $this->get('router')->generate('page_view', array('way' => '/about')));
        $breadcrumbs->addItem('wanted', $this->get('router')->generate('wanted_list'));
        $breadcrumbs->addItem($data->getTitle());
        
        return $this->render('LunySiteBundle:Wanted:view.html.twig',
            array(
                'vacancy' => $data,
            )
        );
    }

    public function ajaxAction($start = 0)
    {
        $doct = $this->getDoctrine();

        $data = $doct->getRepository('LunySiteBundle:Vacancy')
            ->findList($start);

        return $this->render('LunySiteBundle:Wanted:ajax.html.twig',
            array(
                'vacancies' => $data
            )
        );
    }
}