<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BlogController extends Controller
{
    public function indexAction($branch = null)
    {
        $doct = $this->getDoctrine();
        
        $branches = $doct->getRepository('LunySiteBundle:BranchBlog')
            ->findAll();
        
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('главная', $this->get('router')->generate('homepage'));

        if ($branch != 0)
        {
            $branTitle = $doct->getRepository('LunySiteBundle:BranchBlog')
                ->findOneById($branch);

            $data = $doct->getRepository('LunySiteBundle:Blog')
                ->findBundle(0, $branch);

            $breadcrumbs->addItem('журналистам', $this->get('router')->generate('blog_list'));
            $breadcrumbs->addItem($branTitle->getTitle());
        }
        else
        {
            $data = $doct->getRepository('LunySiteBundle:Blog')
                ->findBundle();
            $breadcrumbs->addItem('журналистам');
        }

        $amount_pages = $doct->getRepository('LunySiteBundle:Blog')
            ->findAmountPage($branch);

        return $this->render('LunySiteBundle:Blog:index.html.twig', 
            array(
                'blogs' => $data,
                'branches' => $branches,
                'cur_branch' => $branch,
                'amount_pages' => $amount_pages,
            )
        );
    }

    public function ajaxAction(Request $request)
    {
        $branch = intval($request->query->get('branch'));
        $start = intval($request->query->get('start'));

        $doct = $this->getDoctrine();

        $data = $doct->getRepository('LunySiteBundle:Blog')
            ->findBundle($start, $branch);

        return $this->render('LunySiteBundle:Blog:ajax.html.twig',
            array(
                'blogs' => $data
            )
        );
    }
    
    public function viewAction($id)
    {
        $data = $this->getDoctrine()
            ->getRepository('LunySiteBundle:Blog')
            ->find($id);

        if (!$data) {
            throw $this->createNotFoundException('No article found!');
        }

        return $this->render('LunySiteBundle:Blog:view.html.twig', 
            array(
                'blog' => $data,
                'author' => $data->getWorker()
            )
        );
    }
}