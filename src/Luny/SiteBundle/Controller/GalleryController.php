<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class GalleryController extends Controller
{
    
    public function listAction()
    {
       $em = $this->getDoctrine()->getManager(); 
       $query = $em->createQuery("SELECT img FROM LunySiteBundle:Gallery img");
       $data = $query->getArrayResult();

           $images = array();
           foreach ($data as $val) {
               $image = $val['image']['path'];
               $thumb = str_replace('main'.$val['image']['fileName'], 'mini'.$val['image']['fileName'], $image);
               $images[] = array('image' => $image, 'thumb' => $thumb, 'folder' => 'Gallery');
           }

       $body = json_encode($images);
       
       return new Response($body, 200, array('Content-Type' => 'application/json'));
    }
    
}
