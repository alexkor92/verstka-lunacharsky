<?php

namespace Luny\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class GenParamController extends Controller
{
    
    public function counterAction()
    {

       $counter = $this->getDoctrine()
                    ->getRepository('LunySiteBundle:GenParam')
                    ->findOneByName('luny')
                    ->getCounter()
               ;
        
        return new Response($counter);
        
        
    }
    
}
