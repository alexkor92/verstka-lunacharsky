<?php

namespace Luny\SiteBundle\Helpers;

use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * На основании полученных данных о кол-ве элементов и кол-ва отображаемых элементов 
 * на одной странице данный класс генерирует шаблон ссылок для постраничной навигации 
 * (независимо от способа получения данных)
 */
class Paginator
{
    
 /**
  * @var object Router
  */
 public $router;
    
 private $paginator;


 public function __construct(Router $router) 
 {
  $this->router = $router;
 } 

 /*
  * Генерирует html содержащий ссылки
  * 
  * @param string $routeName Имя маршрута
  * @param array $routeParams параметры маршрута без параметра номера страницы
  * @param string $pageId имя параметра, отвечающего номеру текущей страницы
  * @param integer $intemCount общее количество элементов
  * @param integer $currPage номер текущей страницы
  * @param integer $maxItems количество элементов на странице
  * @param integer $maxPages максимальное количество отображаемых ссылок
  * 
  * @return string html сгенерированных ссылок
  */
 public function paginate($routeName, $routeParams, $pageId, $itemCount, $currPage = 1, $maxItems = 3, $maxPages = 7)
 {
   
    $page_count = ceil($itemCount / $maxItems);
    
    $lft = (($currPage - (($maxPages - 1) / 2)) > 0) ?
    ($currPage - (($maxPages - 1) / 2)) : 1;
    
    $rgt = (($currPage + (($maxPages - 1) / 2)) < $page_count) ?
    ($currPage + (($maxPages - 1) / 2)) : $page_count; 
    
        if ($lft > 1) {
         $this->paginator[1] = 1;
        }

        if ($lft > 2) {
         $this->paginator['leftPoints'] = '...'; 
        }

        for ($i = $lft; $i <= $rgt; $i++) {
         $this->paginator[$i] = $i; 
        }

        if ($rgt < $page_count - 1) {
         $this->paginator['rightPoints'] = '...';
        }

        if ($rgt < $page_count) {
         $this->paginator[(int) $page_count] = (int) $page_count; 
        }

    $result = '';
    foreach($this->paginator as $page => $value )
    {
        if(is_integer($value) and $value != $currPage) {
           // if($value != 1) {
            $params =  $routeParams;
            // TODO: сделать опциональным передавать номер страницы как get querystring либо как path (сейчас как querystring)
           // $params[$pageId] = $page;
             $result .= ' <a href="'.$this->router->generate($routeName, $params).'?'.$pageId.'='.$page.'"> '.$page.'</a>'; 
//            } else {
//             $result .= "1"; 
//            } 
        } elseif ($value != $currPage) {
             $result .= ' '.$value;
        } else {
             $result .= ' '.$value;
        }
     }
  
  return $result;
        
 }
 
}

