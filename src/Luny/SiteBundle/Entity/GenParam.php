<?php

namespace Luny\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Entity\GenParam
 *
 * @ORM\Entity(repositoryClass="GenParamRepository")
 * @ORM\Table(name="general_parameters", indexes={@ORM\Index(name="name", columns={"name"})})
 */
class GenParam
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true, length=200)
     */
    protected $name;

    /**
     * @var string HTML/JavaScript код счётчика посещений
     * @ORM\Column(type="text", nullable=true)
     */
    protected $counter;

    /**
     * @var string E-mail адрес обратной связи
     * @ORM\Column(type="string", length=100)
     */
    protected $email;
    
    /**
     * @var string Телефон(ы) для связи
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone;
    
    /**
     * @var bool true, если на сайте ведутся технические работы
     * @ORM\Column(type="bool", nullable=true)
     */
    protected $handling = true;


    /**
     * Set name
     *
     * @param string $name
     * @return GenParam
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * For SonataAdmin ACL
     *
     * @return string 
     */
    public function getId()
    {
        return $this->name;
    }

    /**
     * Set counter
     *
     * @param string $counter
     * @return GenParam
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * Get counter
     *
     * @return string 
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return GenParam
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return GenParam
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    public function __toString()
    {
        return 'Настройки';
    }
    

    /**
     * Set handling
     *
     * @param bool $handling
     * @return GenParam
     */
    public function setHandling($handling)
    {
        $this->handling = $handling;

        return $this;
    }

    /**
     * Get handling
     *
     * @return bool 
     */
    public function getHandling()
    {
        return $this->handling;
    }
}
