<?php

namespace Luny\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity\Customer
 *
 * @ORM\Entity(repositoryClass="CustomerRepository")
 * @ORM\Table(name="customer")
 * @FileStore\Uploadable
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */    
    protected $keywords;

    /**
     * @ORM\Column(type="text")
     */
    protected $preview;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;
    
    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="string", columnDefinition="CHAR(1) NOT NULL")
     */
    protected $letter;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $organization;    
    
    /**
     * @Assert\File( maxSize="1M")
     * @FileStore\UploadableField(mapping="customer")
     * @ORM\Column(type="array")
     */  
    protected $image;    
    
    
    /**
     * @ORM\Column(type="textwithurls")
     */
    protected $info;

    /**
     * @ORM\Column(type="textwithurls", nullable=true)
     */
    protected $response;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    protected $site;

    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=true)
     */
    protected $projects;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Entity\Customer
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of name.
     *
     * @param string $name
     * @return \Entity\Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of info.
     *
     * @param string $info
     * @return \Entity\Customer
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get the value of info.
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set the value of response.
     *
     * @param string $response
     * @return \Entity\Customer
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get the value of response.
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set the value of site.
     *
     * @param string $site
     * @return \Entity\Customer
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get the value of site.
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Add Project entity to collection (one to many).
     *
     * @param \Entity\Project $project
     * @return \Entity\Customer
     */
    public function addProject(Project $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Get Project entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    public function __sleep()
    {
        return array('id', 'name', 'info', 'response', 'site', 'image');
    }

    public function __toString()
    {
        return $this->name ? : '-';
    }
    
    /**
     * Remove projects
     *
     * @param \Luny\SiteBundle\Entity\Project $projects
     */
    public function removeProject(\Luny\SiteBundle\Entity\Project $projects)
    {
        $this->projects->removeElement($projects);
    }

    /**
     * Set organization
     *
     * @param string $organization
     * @return Customer
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return string 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set image
     *
     * @param array $image
     * @return Customer
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return array 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Customer
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Customer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }



    /**
     * Set letter
     *
     * @param string $letter
     * @return Customer
     */
    public function setLetter($letter)
    {
        $this->letter = $letter;

        return $this;
    }

    /**
     * Get letter
     *
     * @return string 
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Set preview
     *
     * @param string $preview
     * @return Customer
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;

        return $this;
    }

    /**
     * Get preview
     *
     * @return string 
     */
    public function getPreview()
    {
        return $this->preview;
    }
}
