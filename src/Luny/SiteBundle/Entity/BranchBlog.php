<?php

namespace Luny\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity\Blog
 *
 * @ORM\Entity(repositoryClass="BranchBlogRepository")
 * @ORM\Table(name="blog_branch")
 * @FileStore\Uploadable
 */
class BranchBlog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Blog", mappedBy="branch")
     */
    protected $blogs;
    
    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $title;
 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->blogs = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return BlogBranch
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add blogs
     *
     * @param \Luny\SiteBundle\Entity\Blog $blogs
     * @return BlogBranch
     */
    public function addBlog(Blog $blogs)
    {
        $this->blogs[] = $blogs;

        return $this;
    }

    /**
     * Remove blogs
     *
     * @param \Luny\SiteBundle\Entity\Blog $blogs
     */
    public function removeBlog(Blog $blogs)
    {
        $this->blogs->removeElement($blogs);
    }

    /**
     * Get blogs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBlogs()
    {
        return $this->blogs;
    }
    
    public function __toString() 
    {
        return $this->title;
    }
}
