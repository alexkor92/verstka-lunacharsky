<?php

namespace Luny\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity\Technology
 *
 * @ORM\Entity(repositoryClass="TechnologyRepository")
 * @ORM\Table(name="technology")
 * @UniqueEntity(fields={"name"}, message="Duplicate technology", groups={"admin"})
 * @FileStore\Uploadable
 */
class Technology
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true, length=120)
     */
    protected $name;
   
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */    
    protected $keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;
    
    /**
     * @Assert\File( maxSize="1M")
     * @FileStore\UploadableField(mapping="technology")
     * @ORM\Column(type="array")
     */  
    protected $icon;
  
    /**
     * @ORM\Column(type="text")
     */
    protected $preview;

    /**
     * @ORM\Column(type="textwithurls")
     */
    protected $info;
    
    /**
     * @ORM\ManyToMany(targetEntity="Project", mappedBy="technologies")
     */
    protected $projects;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Entity\Technology
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of name.
     *
     * @param string $name
     * @return \Entity\Technology
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of icon.
     *
     * @param string $icon
     * @return \Entity\Technology
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get the value of icon.
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set the value of preview.
     *
     * @param string $preview
     * @return \Entity\Technology
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;

        return $this;
    }

    /**
     * Get the value of preview.
     *
     * @return string
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * Set the value of info.
     *
     * @param string $info
     * @return \Entity\Technology
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get the value of info.
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Add Project entity to collection.
     *
     * @param \Entity\Project $project
     * @return \Entity\Technology
     */
    public function addProject(Project $project)
    {
        $project->addTechnology($this);
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Get Project entity collection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    public function __sleep()
    {
        return array('id', 'name', 'icon', 'preview', 'info');
    }

    /**
     * Remove projects
     *
     * @param \Luny\SiteBundle\Entity\Project $projects
     */
    public function removeProject(\Luny\SiteBundle\Entity\Project $projects)
    {
        $this->projects->removeElement($projects);
    }
    
    public function __toString()
    {
        return $this->name ? : '-';
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Technology
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Technology
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
