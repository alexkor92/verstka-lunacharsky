<?php

namespace Luny\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity\Project
 *
 * @ORM\Entity(repositoryClass="ProjectRepository")
 * @ORM\Table(name="project", indexes={@ORM\Index(name="customer_id", columns={"customer_id"})})
 * @FileStore\Uploadable
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var string type of projects, see app/config/settings.yml project_types parameter
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    protected $category;


    /**
     * @Assert\File( maxSize="2M")
     * @FileStore\UploadableField(mapping="project")
     * @ORM\Column(type="array", nullable=true)
     */
    protected $screen;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $problem;


    /**
     * @ORM\Column(type="textwithurls", nullable=true)
     */
    protected $design;


    /**
     * @ORM\Column(type="textwithurls", nullable=true)
     */
    protected $programing;


    /**
     * @ORM\Column(type="textwithurls", nullable=true)
     */
    protected $promotion;

    /**
     * @ORM\Column(type="integer")
     */
    protected $goverment;

    /**
     * @Assert\File(maxSize="1M")
     * @FileStore\UploadableField(mapping="goverment")
     * @ORM\Column(type="array", nullable=true)
     */
    protected $gover_icon;


    /**
     * @var $resp_title Title of response. e.g. "Выражаю благодарность!"
     * @ORM\Column(type="string", nullable=true)
     */
    protected $resp_title;

    /**
     * @ORM\Column(type="textwithurls", nullable=true)
     */
    protected $resp_text;

    /**
     * @var $resp_auth The response author: name and surname
     * @ORM\Column(type="string", nullable=true)
     */
    protected $resp_auth;

    /**
     * @var $resp_pos The position of the response author
     * @ORM\Column(type="string", nullable=true)
     */
    protected $resp_pos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $link;

    /**
     * @ORM\Column(type="smallint", length=4, nullable=true)
     */
    protected $rel_year;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="projects")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    protected $customer;

    /**
     * @ORM\ManyToMany(targetEntity="Technology", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinTable(name="project_technology",
     *   joinColumns={
     *     @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="technology_id", referencedColumnName="id")
     *   }
     * )
     */
    protected $technologies;

    /**
     * @ORM\ManyToMany(targetEntity="Worker", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinTable(name="project_worker",
     *   joinColumns={
     *     @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="worker_id", referencedColumnName="id")
     *   }
     * )
     */
    protected $workers;

    public function __construct()
    {
        $this->technologies = new ArrayCollection();
        $this->workers = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Entity\Project
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of title.
     *
     * @param string $title
     * @return \Entity\Project
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of name.
     *
     * @param string $name
     * @return \Entity\Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of screen.
     *
     * @param string $screen
     * @return \Entity\Project
     */
    public function setScreen($screen)
    {
        $this->screen = $screen;

        return $this;
    }

    /**
     * Get the value of screen.
     *
     * @return array
     */
    public function getScreen()
    {
        return $this->screen;
    }

    /**
     * Set the value of problem.
     *
     * @param string $problem
     * @return \Entity\Project
     */
    public function setProblem($problem)
    {
        $this->problem = $problem;

        return $this;
    }

    /**
     * Get the value of problem.
     *
     * @return string
     */
    public function getProblem()
    {
        return $this->problem;
    }

    public function setTechnologies($technologies)
    {
        $this->technologies = $technologies;

        return $this;
    }

    public function setWorkers($workers)
    {
        $this->workers = $workers;

        return $this;
    }

    /**
     * Set the value of response.
     *
     * @param string $response
     * @return \Entity\Project
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get the value of response.
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set the value of link.
     *
     * @param string $link
     * @return \Entity\Project
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get the value of link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set the value of rel_year.
     *
     * @param integer $rel_year
     * @return \Entity\Project
     */
    public function setRelYear($rel_year)
    {
        $this->rel_year = $rel_year;

        return $this;
    }

    /**
     * Get the value of rel_year.
     *
     * @return integer
     */
    public function getRelYear()
    {
        return $this->rel_year;
    }

    /**
     * Set Customer entity (many to one).
     *
     * @param \Entity\Customer $customer
     * @return \Entity\Project
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get Customer entity (many to one).
     *
     * @return \Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Add Technology entity to collection.
     *
     * @param \Entity\Technology $technology
     * @return \Entity\Project
     */
    public function addTechnology(Technology $technology)
    {
        $this->technologies[] = $technology;

        return $this;
    }

    /**
     * Get Technology entity collection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTechnologies()
    {
        return $this->technologies;
    }

    /**
     * Add Worker entity to collection.
     *
     * @param \Entity\Worker $worker
     * @return \Entity\Project
     */
    public function addWorker(Worker $worker)
    {
        $this->workers[] = $worker;

        return $this;
    }

    /**
     * Get Worker entity collection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkers()
    {
        return $this->workers;
    }

    public function __sleep()
    {
        return array('id', 'title', 'name', 'category', 'customer_id', 'problem', 'screen', 'response', 'link', 'rel_year', 'technologies', 'workers');
    }

    public function __toString()
    {
        return $this->title ? : '-';
    }

    /**
     * Remove technologies
     *
     * @param \Luny\SiteBundle\Entity\Technology $technologies
     */
    public function removeTechnology(\Luny\SiteBundle\Entity\Technology $technologies)
    {
        $this->technologies->removeElement($technologies);
    }

    /**
     * Remove workers
     *
     * @param \Luny\SiteBundle\Entity\Worker $workers
     */
    public function removeWorker(\Luny\SiteBundle\Entity\Worker $workers)
    {
        $this->workers->removeElement($workers);
    }

    /**
     * Set design
     *
     * @param string $design
     * @return Project
     */
    public function setDesign($design)
    {
        $this->design = $design;

        return $this;
    }

    /**
     * Get design
     *
     * @return string
     */
    public function getDesign()
    {
        return $this->design;
    }

    /**
     * Set programing
     *
     * @param string $programing
     * @return Project
     */
    public function setPrograming($programing)
    {
        $this->programing = $programing;

        return $this;
    }

    /**
     * Get programing
     *
     * @return string
     */
    public function getPrograming()
    {
        return $this->programing;
    }

    /**
     * Set promotion
     *
     * @param string $promotion
     * @return Project
     */
    public function setPromotion($promotion)
    {
        $this->promotion = $promotion;

        return $this;
    }

    /**
     * Get promotion
     *
     * @return string
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Set resp_title
     *
     * @param string $respTitle
     * @return Project
     */
    public function setRespTitle($respTitle)
    {
        $this->resp_title = $respTitle;

        return $this;
    }

    /**
     * Get resp_title
     *
     * @return string
     */
    public function getRespTitle()
    {
        return $this->resp_title;
    }

    /**
     * Set resp_text
     *
     * @param string $respText
     * @return Project
     */
    public function setRespText($respText)
    {
        $this->resp_text = $respText;

        return $this;
    }

    /**
     * Get resp_text
     *
     * @return string
     */
    public function getRespText()
    {
        return $this->resp_text;
    }

    /**
     * Set resp_auth
     *
     * @param string $respAuth
     * @return Project
     */
    public function setRespAuth($respAuth)
    {
        $this->resp_auth = $respAuth;

        return $this;
    }

    /**
     * Get resp_auth
     *
     * @return string
     */
    public function getRespAuth()
    {
        return $this->resp_auth;
    }

    /**
     * Set resp_pos
     *
     * @param string $respPos
     * @return Project
     */
    public function setRespPos($respPos)
    {
        $this->resp_pos = $respPos;

        return $this;
    }

    /**
     * Get resp_pos
     *
     * @return string
     */
    public function getRespPos()
    {
        return $this->resp_pos;
    }

    /**
     * Set goverment
     *
     * @param \tinyint $goverment
     * @return Project
     */
    public function setGoverment($goverment)
    {
        $this->goverment = $goverment;

        return $this;
    }

    /**
     * Get goverment
     *
     * @return \tinyint
     */
    public function getGoverment()
    {
        return $this->goverment;
    }

    /**
     * Set gover_icon
     *
     * @param array $goverIcon
     * @return Project
     */
    public function setGoverIcon($goverIcon)
    {
        $this->gover_icon = $goverIcon;

        return $this;
    }

    /**
     * Get gover_icon
     *
     * @return array
     */
    public function getGoverIcon()
    {
        return $this->gover_icon;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Project
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Project
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
