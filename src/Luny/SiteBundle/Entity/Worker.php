<?php

namespace Luny\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity\Worker
 *
 * @ORM\Entity(repositoryClass="WorkerRepository")
 * @ORM\Table(name="worker")
 * @FileStore\Uploadable
 */
class Worker
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
   
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */    
    protected $keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;
    
    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $fname;

    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $lname;

    
    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $position;

    /**
     * @Assert\File( maxSize="1M")
     * @FileStore\UploadableField(mapping="worker")
     * @ORM\Column(type="array")
     */    
    protected $photo;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $preview;      
    
    /**
     * @ORM\Column(type="textwithurls")
     */
    protected $info;    
    
    /**
     * @ORM\Column(type="textwithurls")
     */
    protected $quote;

    /**
     * @ORM\Column(type="smallint", length=1, options={"default": 0})
     */
    protected $dismiss;

    /**
     * @ORM\OneToMany(targetEntity="Blog", mappedBy="worker")
     */
    protected $blogs;

    /**
     * @ORM\ManyToMany(targetEntity="Project", mappedBy="workers")
     */
    protected $projects;

    public function __construct()
    {
        $this->blogs = new ArrayCollection();
        $this->projects = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Entity\Worker
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of fname.
     *
     * @param string $fname
     * @return \Entity\Worker
     */
    public function setFname($fname)
    {
        $this->fname = $fname;

        return $this;
    }

    /**
     * Get the value of fname.
     *
     * @return string
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set the value of lname.
     *
     * @param string $lname
     * @return \Entity\Worker
     */
    public function setLname($lname)
    {
        $this->lname = $lname;

        return $this;
    }

    /**
     * Get the value of lname.
     *
     * @return string
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set the value of position.
     *
     * @param string $position
     * @return \Entity\Worker
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get the value of position.
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    
    /**
     * Set the value of photo.
     *
     * @param string $photo
     * @return \Entity\Worker
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get the value of photo.
     *
     * @return array
     */
    public function getPhoto()
    {
        return $this->photo;
    }
    
    /**
     * Set the value of info.
     *
     * @param string $info
     * @return \Entity\Worker
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get the value of info.
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Add Blog entity to collection (one to many).
     *
     * @param \Entity\Blog $blog
     * @return \Entity\Worker
     */
    public function addBlog(Blog $blog)
    {
        $this->blogs[] = $blog;

        return $this;
    }

    /**
     * Get Blog entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlogs()
    {
        return $this->blogs;
    }

    /**
     * Add Project entity to collection.
     *
     * @param \Entity\Project $project
     * @return \Entity\Worker
     */
    public function addProject(Project $project)
    {
        $project->addWorker($this);
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Get Project entity collection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    public function __sleep()
    {
        return array('id', 'fname', 'lname', 'position', 'info');
    }

    /**
     * Remove blogs
     *
     * @param \Luny\SiteBundle\Entity\Blog $blogs
     */
    public function removeBlog(\Luny\SiteBundle\Entity\Blog $blogs)
    {
        $this->blogs->removeElement($blogs);
    }

    /**
     * Remove projects
     *
     * @param \Luny\SiteBundle\Entity\Project $projects
     */
    public function removeProject(\Luny\SiteBundle\Entity\Project $projects)
    {
        $this->projects->removeElement($projects);
    }
    
    public function __toString()
    {
        return $this->fname.' '.$this->lname ? : '-';
    }

    /**
     * Set quote
     *
     * @param string $quote
     * @return Worker
     */
    public function setQuote($quote)
    {
        $this->quote = $quote;

        return $this;
    }

    /**
     * Get quote
     *
     * @return string 
     */
    public function getQuote()
    {
        return $this->quote;
    }

    /**
     * Set preview
     *
     * @param string $preview
     * @return Worker
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;

        return $this;
    }

    /**
     * Get preview
     *
     * @return string 
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Worker
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Worker
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dismiss
     *
     * @param smallint $dismiss
     * @return Worker
     */
    public function setDismiss($dismiss)
    {
        $this->dismiss = $dismiss;

        return $this;
    }

    /**
     * Get dismiss
     *
     * @return smallint
     */
    public function getDismiss()
    {
        return $this->dismiss;
    }
}
