<?php

namespace Luny\SiteBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\UserBundle\Entity\User as SonataUser;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\ExecutionContext;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\NestedTreeRepository")
 * @UniqueEntity(fields={"url"}, message="Duplicate URL", groups={"admin"})
 * ORM\Entity(repositoryClass="PageRepository")
 */
class Page
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    
    /**
     * @ORM\Column(name="way", type="string", length=3000, nullable=true)
     */
     protected $way;

     
    /**
     * @ORM\Column(type="string", unique=true, length=60, nullable=false)
     */
    protected $url;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */    
    
    protected $keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $preview;

    /**
     * @ORM\Column(type="textwithurls", nullable=true)
     */
    protected $content;

    /**
     * @ORM\Column()
     */
    protected $created_at;

    /**
     * @ORM\Column()
     */
    protected $updated_at;

   /**
    * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="pages")
    * @ORM\JoinColumn(name="user", referencedColumnName="id")
    */
    protected $user;

   /**
    * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="pages")
    * @ORM\JoinColumn(name="user_update", referencedColumnName="id")
    */
    protected $user_update;    
    
    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer", nullable=true)
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer", nullable=true)
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer", nullable=true)
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=false)
     */
    private $root = 1;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;
    
    
    
    public function __construct()
    {
        // default datetime
        $now = date('Y-m-d H:i:s');
        $this->created_at = $now;
        $this->updated_at = $now;
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Entity\Page
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of title.
     *
     * @param string $title
     * @return \Entity\Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of name.
     *
     * @param string $name
     * @return \Entity\Page
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function setWay($way)
    {
        $this->way = $way;
    }

    public function getWay()
    {
        return $this->way;
    }
    
    
    /**
     * Set the value of path.
     *
     * @param string $url
     * @return \Entity\Page
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Get the value of url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of keywords.
     *
     * @param string $keywords
     * @return \Entity\Page
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get the value of keywords.
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set the value of description.
     *
     * @param string $description
     * @return \Entity\Page
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of preview.
     *
     * @param string $preview
     * @return \Entity\Page
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;

        return $this;
    }

    /**
     * Get the value of preview.
     *
     * @return string
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * Set the value of content.
     *
     * @param string $content
     * @return \Entity\Page
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    
    /**
     * Set the value of created_at.
     *
     * @param  $created_at
     * @return \Entity\Page
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of created_at.
     *
     * @return 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set the value of updated_at.
     *
     * @param  $updated_at
     * @return \Entity\Page
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get the value of updated_at.
     *
     * @return 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }


    
    public function setParent(Page $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }
    
    public function setChildren(Page $children = null)
    {
        $this->children = $children;
    }
   
    public function getLft() {
        return $this->lft;
    }

    public function getLvl() {
        return $this->lvl;
    }

    public function getRgt() {
        return $this->rgt;
    }

    public function getRoot() {
        return $this->root;
    }

    public function setLft($lft) {
        $this->lft = $lft;
    }

    public function setLvl($lvl) {
        $this->lvl = $lvl;
    }

    public function setRgt($rgt) {
        $this->rgt = $rgt;
    }

    public function setRoot($root) {
        $this->root = $root;
    }

    public function getChildren()
    {
        return $this->children;
    }
    
    public function __toString()
    {
        if ($this->lvl == 0) {
            return '';
        } else {
            return '/'.$this->url;
        }
    }
    
    
    public function getLaveledTitle()
    {
        $prefix = '';
        for ($i = 1; $i <= $this->lvl; $i++){
            $prefix .= '> ';
        }
        return $prefix . $this->name;
    }    
    
    public function __sleep()
    {
        return array('id', 'title', 'name', 'keywords', 'description', 'preview', 'content', 'created_at', 'updated_at', 'user_id');
    }

    /**
     * Add children
     *
     * @param \Luny\SiteBundle\Entity\Page $children
     * @return Page
     */
    public function addChild(\Luny\SiteBundle\Entity\Page $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Luny\SiteBundle\Entity\Page $children
     */
    public function removeChild(\Luny\SiteBundle\Entity\Page $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Set user_create
     *
     * @param \Application\Sonata\UserBundle\Entity\User $userCreate
     * @return Page
     */
    public function setUserCreate(\Application\Sonata\UserBundle\Entity\User $userCreate = null)
    {
        $this->user_create = $userCreate;

        return $this;
    }

     public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    } 
    
    
    /**
     * Get user_create
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUserCreate()
    {
        return $this->user_create;
    }

    /**
     * Set user_update
     *
     * @param \Application\Sonata\UserBundle\Entity\User $userUpdate
     * @return Page
     */
    public function setUserUpdate(\Application\Sonata\UserBundle\Entity\User $userUpdate = null)
    {
        $this->user_update = $userUpdate;

        return $this;
    }

    /**
     * Get user_update
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUserUpdate()
    {
        return $this->user_update;
    }
    

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
