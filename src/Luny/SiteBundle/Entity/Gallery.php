<?php

namespace Luny\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity\Gallery
 *
 * @ORM\Entity(repositoryClass="GalleryRepository")
 * @ORM\Table(name="gallery")
 * @UniqueEntity(fields={"name"}, message="Duplicate name", groups={"admin"})
 * @FileStore\Uploadable
 */
class Gallery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true, length=120)
     */
    protected $name;

    /**
     * @Assert\File(maxSize="5M")
     * @FileStore\UploadableField(mapping="gallery")
     * @ORM\Column(type="array")
     */  
    protected $image;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $descr;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Gallery
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param array $image
     * @return Gallery
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return array 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return Gallery
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }
    
    public function __toString()
    {
        return $this->name ? : '-';
    }    


    
}
