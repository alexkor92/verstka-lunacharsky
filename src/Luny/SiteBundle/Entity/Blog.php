<?php

namespace Luny\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity\Blog
 *
 * @ORM\Entity(repositoryClass="BlogRepository")
 * @ORM\Table(name="blog", indexes={@ORM\Index(name="worker_id", columns={"worker_id"})})
 * @FileStore\Uploadable
 */
class Blog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $keywords;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $description;

    /**
     * @ORM\Column(type="text")
     */
    protected $preview;

    /**
     * @Assert\File( maxSize="1M")
     * @FileStore\UploadableField(mapping="blog")
     * @ORM\Column(type="array")
     */  
    protected $illustration;
  
    
    /**
     * @ORM\Column(type="textwithurls")
     */
    protected $content;

    /**
     * @ORM\Column()
     */
    protected $created_at;

    /**
     * @ORM\Column()
     */
    protected $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="Worker", inversedBy="blogs")
     * @ORM\JoinColumn(name="worker_id", referencedColumnName="id")
     */
    protected $worker;

    /**
     * @ORM\ManyToOne(targetEntity="BranchBlog", inversedBy="blogs")
     * @ORM\JoinColumn(name="branch_id", referencedColumnName="id")
     */
    protected $branch;    
    

    public function __construct()
    {
        // default datetime
        $now = date('Y-m-d H:i:s');
        $this->created_at = $now;
        $this->updated_at = $now;
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Entity\Blog
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of title.
     *
     * @param string $title
     * @return \Entity\Blog
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of name.
     *
     * @param string $name
     * @return \Entity\Blog
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of keywords.
     *
     * @param string $keywords
     * @return \Entity\Blog
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get the value of keywords.
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set the value of description.
     *
     * @param string $description
     * @return \Entity\Blog
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of preview.
     *
     * @param string $preview
     * @return \Entity\Blog
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;

        return $this;
    }

    /**
     * Get the value of preview.
     *
     * @return string
     */
    public function getPreview()
    {
        return $this->preview;
    }
    
    /**
     * Set the value of illustration.
     *
     * @param array $illustration
     * @return \Entity\Blog
     */
    public function setIllustration($illustration)
    {
        $this->illustration = $illustration;

        return $this;
    }

    /**
     * Get the value of illustration.
     *
     * @return array
     */
    public function getIllustration()
    {
        return $this->illustration;
    }

    /**
     * Set the value of content.
     *
     * @param string $content
     * @return \Entity\Blog
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of created_at.
     *
     * @param  $created_at
     * @return \Entity\Blog
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of created_at.
     *
     * @return 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set the value of updated_at.
     *
     * @param  $updated_at
     * @return \Entity\Blog
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get the value of updated_at.
     *
     * @return 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set Worker entity (many to one).
     *
     * @param \Entity\Worker $worker
     * @return \Entity\Blog
     */
    public function setWorker(Worker $worker = null)
    {
        $this->worker = $worker;

        return $this;
    }

    /**
     * Get Worker entity (many to one).
     *
     * @return \Entity\Worker
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * Set BranchBlog entity (many to one).
     *
     * @param \Entity\BranchBlog $branch
     * @return \Entity\Blog
     */
    public function setBranch(BranchBlog $branch = null)
    {
        $this->branch = $branch;

        return $this;
    }

    /**
     * Get BranchBlog entity (many to one).
     *
     * @return \Entity\BranchBlog
     */
    public function getBranch()
    {
        return $this->branch;
    }

    public function __sleep()
    {
        return array('id', 'title', 'name', 'illustration', 'keywords', 'description', 'preview', 'content', 'worker_id', 'created_at', 'updated_at');
    }

    public function __toString()
    {
        return $this->title ? : '-';
    }
}
