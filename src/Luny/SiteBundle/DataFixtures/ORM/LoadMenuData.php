<?php

namespace Luny\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Luny\SiteBundle\Entity\Menu;

class LoadMenuData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $default = array(
                         array('title' => 'Электрокоммерция', 'type'=>'horizontal', 'url' => '/projects/type/elcom', 'place' => 1),
                         array('title' => 'Медийные проекты', 'type'=>'horizontal', 'url' => '/projects/type/media', 'place' => 2),
                         array('title' => 'Мобильные приложения', 'type'=>'horizontal', 'url' => '/projects/type/mobile', 'place' => 3),
                         array('title' => 'Автоматизация', 'type'=>'horizontal', 'url' => '/projects/type/autom', 'place' => 4),
                         array('title' => 'О компании', 'type'=>'horizontal', 'url' => '/page/about', 'place' => 5),
               
                         array('title' => 'Доменые имена', 'type'=>'on_project_page', 'url' => '/page/domains', 'place' => 1),
                         array('title' => 'Хостинг', 'type'=>'on_project_page', 'url' => '/page/hosting', 'place' => 2),
                         array('title' => 'Продвижение', 'type'=>'on_project_page', 'url' => '/page/promotion', 'place' => 3),
                 
                         array('title' => 'Таланты', 'type'=>'on_about_page', 'url' => '/workers', 'place' => 1),
                         array('title' => 'Флешбэк', 'type'=>'on_about_page', 'url' => '/flashback', 'place' => 2),
                         array('title' => 'Wanted', 'type'=>'on_about_page', 'url' => '/wanted', 'place' => 3),
                         array('title' => 'Продукты', 'type'=>'on_about_page', 'url' => '/page/products', 'place' => 4),
        );
        
        foreach ($default as $label) 
        {
                $menuLabel = new Menu();
                $menuLabel->setTitle($label['title']);
                $menuLabel->setUrl($label['url']);
                $menuLabel->setPlace($label['place']);
                $menuLabel->setType($label['type']);
                $manager->persist($menuLabel);
        }
        
        $manager->flush();
    }
}