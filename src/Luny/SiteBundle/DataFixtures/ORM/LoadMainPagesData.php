<?php

namespace Luny\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Luny\SiteBundle\Entity\Page;

class LoadMainPageData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
                $rootPage = new Page();
                $rootPage->setTitle('Главная (корень)');
                $rootPage->setName('Главная (корень)');
                $rootPage->setUrl('/');
                $rootPage->setLvl(0);
                $rootPage->setLft(1);
                $rootPage->setRgt(1);
                $rootPage->setRoot(1);
                $manager->persist($rootPage);
                
                $companyPage = new Page();
                $companyPage->setParent($rootPage);                
                $companyPage->setTitle('О компании');
                $companyPage->setName('О компании');
                $companyPage->setContent('
                    <div class="wrap">
                        <div class="chief"></div>
                        <div class="title">А.В. Луначарский
                            <div class="date">1875-1933</div>
                        </div>
                        <div class="text">Что касается нашей страны, то разрешенный возраст вступления в брачный союз составляет восемнадцать лет. Если же молодые люди по определенным причинам хотят создать семью в более раннем возрасте, например, в связи с незапланированной беременностью, то при наличии медицинской справки.</div>
                    </div>');
                $companyPage->setWay('/about');
                $companyPage->setUrl('about');
                $companyPage->setLvl(1);
                $companyPage->setLft(12);
                $companyPage->setRgt(21);
                $companyPage->setRoot(1);
                $manager->persist($companyPage);
                
                $domainsPage = new Page();
                $domainsPage->setParent($rootPage);                
                $domainsPage->setTitle('Домены');
                $domainsPage->setName('Домены');
                $domainsPage->setContent('
                    <div class="page-title">
                        <div class="content-left"><a class="b-link" href="#">Вернуться к списку сайтов</a></div>
                        <div class="content-right">
                            <h1>RU, РФ, NET, COM, ORG, BIZ, INFO, FM</h1>
                            <div class="subtitle">Полный спектр услуг по регистрации доменных имен</div>
                        </div>
                        <div class="content-left">
                            <div class="banner"><img alt="" src="/img/temp/left-thumb1.jpg" /></div>
                        </div>
                        <div class="content-right">
                            <p>Все дата-центры представляют собой высокотехнологические площадки, оборудованные современными системами, обеспечивающими бесперебойную, защищенную работу установленного на них оборудования:</p>
                            <ul>
                                <li>системы пожаротушения;</li>
                                <li>системы климат-контроля;</li>
                                <li>системы бесперебойного питания;</li>
                                <li>системы мониторинга за состоянием оборудования и каналов;</li>
                                <li>системы авторизованного доступа.</li>
                            </ul>
                        </div>
                    </div>');
                $domainsPage->setWay('/domains');
                $domainsPage->setUrl('domains');
                $domainsPage->setLvl(1);
                $domainsPage->setLft(12);
                $domainsPage->setRgt(21);
                $domainsPage->setRoot(1);
                $manager->persist($domainsPage);
                
                $hostingPage = new Page();
                $hostingPage->setParent($rootPage);                
                $hostingPage->setTitle('Хостинг');
                $hostingPage->setName('Хостинг');
                $hostingPage->setContent('
                    <div class="page-title">
                        <div class="content-left"><a class="b-link" href="#">Вернуться к списку сайтов</a></div>
                        <div class="content-right">
                            <h1>Техническая площадка</h1>
                            <div class="subtitle">Услуги крупнейших хостинг-провайдеров России</div>
                        </div>
                        <div class="content-left">
                            <div class="banner"><img alt="" src="/img/temp/left-thumb2.jpg" /></div>
                        </div>
                        <div class="content-right">
                            <p>Все дата-центры представляют собой высокотехнологические площадки, оборудованные современными системами, обеспечивающими бесперебойную, защищенную работу установленного на них оборудования:</p>
                            <ul>
                                <li>системы пожаротушения;</li>
                                <li>системы климат-контроля;</li>
                                <li>системы бесперебойного питания;</li>
                                <li>системы мониторинга за состоянием оборудования и каналов;</li>
                                <li>системы авторизованного доступа.</li>
                            </ul>
                        </div>
                    </div>');
                $hostingPage->setWay('/hosting');
                $hostingPage->setUrl('hosting');
                $hostingPage->setLvl(1);
                $hostingPage->setLft(12);
                $hostingPage->setRgt(21);
                $hostingPage->setRoot(1);
                $manager->persist($hostingPage);

                $promotionPage = new Page();
                $promotionPage->setParent($rootPage);                
                $promotionPage->setTitle('Продвижение');
                $promotionPage->setName('Продвижение');
                $promotionPage->setContent('
                    <div class="page-title">
                        <div class="content-left"><a class="b-link" href="#">Вернуться к списку сайтов</a></div>
                        <div class="content-right">
                            <h1>Продвижение</h1>
                        </div>
                    </div>
                    <div id="promo-menu">
                        <div class="item">
                            <div class="man">
                                <img src="/img/temp/pman-1.png" />
                            </div>
                            <div class="text">
                                <a href="#">Увеличение</a> вашей аудитории
                                <div class="picture">
                                    <img alt="" src="/img/temp/pman-pic-1.png" />
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="man">
                                <img src="/img/temp/pman-2.png" />
                            </div>
                            <div class="text">
                                <a href="promotion.hml">Продвижение</a> в поисковых системах
                                <div class="picture">
                                    <img alt="" src="/img/temp/pman-pic-2.png" />
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="man">
                                <img src="/img/temp/pman-3.png" />
                            </div>
                            <div class="text">
                                <a href="#">Узнаваемость</a> в социальных сетях
                                <div class="picture">
                                    <img alt="" src="/img/temp/pman-pic-3.png" />
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="man">
                                <img src="/img/temp/pman-4.png" />
                            </div>
                            <div class="text">
                                <a href="#">Стимуляция</a> продажи через интернет
                                <div class="picture">
                                    <img alt="" src="/img/temp/pman-pic-4.png" />
                                </div>
                            </div>
                        </div>
                    </div>');
                $promotionPage->setWay('/promotion');
                $promotionPage->setUrl('promotion');
                $promotionPage->setLvl(1);
                $promotionPage->setLft(12);
                $promotionPage->setRgt(21);
                $promotionPage->setRoot(1);
                $manager->persist($promotionPage);
                
                $productsPage = new Page();
                $productsPage->setParent($rootPage);                
                $productsPage->setTitle('Продукты');
                $productsPage->setName('Продукты');
                $productsPage->setContent('
                    <div class="page-title">
                        <div class="content-left"><a class="b-link" href="#">Технологии</a></div>
                        <div class="content-right">
                            <h1>Система управления сайтом</h1>
                            <div class="subtitle">Многозадачная. Устойчивая. Простая в управлении</div>
                        </div>
                    </div>
                    <div class="products">
                        <div class="head"></div>
                        <div class="hand">&nbsp;</div>
                        <div class="link"><a href="#">Смотреть</a></div>
                        <div class="items">
                            <div class="item">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><a href="#">Сервис учета ежедневных целей</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#"><img alt="" src="/img/temp/land-prod-1.png" /> </a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="item">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><a href="#">Сервис общения с посетителями сайта</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#"><img alt="" src="/img/temp/land-prod-2.png" /> </a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="item">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><a href="#">Сервис клиентских взаимодействий</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#"><img alt="" src="/img/temp/land-prod-3.png" /> </a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="item">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><a href="#">Сервис учета ежедневных целей</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#"><img alt="" src="/img/temp/land-prod-1.png" /> </a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="item">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><a href="#">Сервис общения с посетителями сайта</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#"><img alt="" src="/img/temp/land-prod-2.png" /> </a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>');
                $productsPage->setWay('/products');
                $productsPage->setUrl('products');
                $productsPage->setLvl(1);
                $productsPage->setLft(12);
                $productsPage->setRgt(21);
                $productsPage->setRoot(1);
                $manager->persist($productsPage);                
                
        $manager->flush();
    }
}