<?php

namespace Luny\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Luny\SiteBundle\Entity\SeoBlock;

class LoadSeoBlockData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $blocks = 
                array(
                     array('name' => 'Главная страница', 'page' => 'index', 'title' => 'Студия LUNACHARSKY'),
                     array('name' => 'Проекты (общий список)', 'page' => 'projects', 'title' => 'Наши работы'),
                     array('name' => 'Проекты (электрокоммерция)', 'page' => 'projects_elcom', 'title' => 'Электрокоммерция'),
                     array('name' => 'Проекты (автоматизация)', 'page' => 'projects_autom', 'title' => 'Автоматизация'),
                     array('name' => 'Проекты (мобильные приложения)', 'page' => 'projects_mobile', 'title' => 'Мобильные приложения'),
                     array('name' => 'Проекты (медийные проекты)', 'page' => 'projects_media', 'title' => 'Медийные проекты'),
                     array('name' => 'Проекты (федеральные заказы)', 'page' => 'projects_gov', 'title' => 'Государственным учреждениям'),
                     array('name' => 'Таланты (список сотрудников)', 'page' => 'workers', 'title' => 'Наши таланты'),
                     array('name' => 'Технологии (список)', 'page' => 'technologies', 'title' => 'Технологии'),
                     array('name' => 'Wanted (список вакансий)', 'page' => 'wanted', 'title' => 'Вакансии'),
                     array('name' => 'Флешбэк', 'page' => 'flashback', 'title' => 'Флешбэк'),
                     array('name' => 'Наши клиенты (общий список)', 'page' => 'customers', 'title' => 'Наши клиенты'),
                     array('name' => 'Наши клиенты (глоссарий)', 'page' => 'customers_abc', 'title' => 'Клиенты студии'),
                     array('name' => 'Статьи (общий список)', 'page' => 'blogs', 'title' => 'Авторские статьи'),
                );
        
        foreach ($blocks as $val) {
            $block = new SeoBlock();
            $block->setName($val['name']);    
            $block->setPage($val['page']);
            $block->setTitle($val['title']);
            $manager->persist($block);
        }
        
        $manager->flush();
    }
}