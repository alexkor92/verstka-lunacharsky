<?php

namespace Luny\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Luny\SiteBundle\Entity\Worker;

class LoadWorkerData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $worker = new Worker();
        $worker->setFname('Иван');
        $worker->setLname('Иванов');
        $worker->setQuote('Я создался через doctrine fixtures...');
        $worker->setPreview('Здесь должен быть popup анонс для страницы таланты...');
        $worker->setPosition('Тестировщик');
        $worker->setPhoto('a:7:{s:8:"fileName";s:6:"/person-cutted.png";s:12:"originalName";s:19:"person-cutted.png";s:8:"mimeType";s:9:"image/png";s:4:"size";i:16037;s:4:"path";s:42:"/img/temp/person-cutted.png";s:5:"width";i:323;s:6:"height";i:432;}');
        
        $manager->persist($worker);
        $manager->flush();
    }
}