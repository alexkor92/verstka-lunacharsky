<?php

namespace Luny\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Luny\SiteBundle\Entity\GenParam;

class LoadGenParamData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $param = new GenParam();
        $param->setName('luny');    
        $param->setEmail('ms@luny.ru');
        $param->setPhone('660-06-11');
        $manager->persist($param);
        $manager->flush();
    }
}