(function($, undefined){
    var bl = $('#blog-list');
    if (!bl.length){
        return;
    }
    var cont = bl.find('.items'),
        url,                                // url ajax загрузки
        pages = cont.find('.page'),         // блоки страниц
        pages = cont.find('.page'),         // блоки страниц
        slider = bl.find('.blog-slider'),
        pointer = slider.find('.pointer'),
        step,                               // длина шага в слайдере
        numbers,                            // цифры в слайдере
        pad,                                // отступ
        current = 0;                        // текущий

    init();

    function init(){
        var data = bl.data(),
            numCont = slider.find('.numbers'),
            numWidth = numCont.width(),
            numPre = numCont.css('padding-left');
            createMark = function(point, id){
                var el = document.createElement('div');
                el.className = 'num';
                el.innerHTML = '<a>'+id+'</a>';
                numCont.append(el);
                el.style.left = point - $(el).width() / 2 + 3 + 'px';
            };
        var num = data['pagenum']; // колчиество страниц
        url = data['url'];
        if (!num || num < 2){
            slider.hide();
            return;
        }
        var i;
        if (pages.length < num){
            var el;
            pages.each(function(){
                // 0 - не загружен, 1 - загружается, 2 - загружен
                $(this).data('status', 2);
                initTurn(this);
            });
            for (i = pages.length; i < num; i++){
                el = document.createElement('div');
                el.className = 'page';
                cont.append(el);
                $(el).data('status', 0);
            }
            pages = cont.find('.page');
            cont.height(pages.eq(current).height());
        }
        step = Math.floor(numWidth / (num - 1));
        createMark(0, 1);
        for (i = 1; i < num-1; i++){
            createMark(i * step, i+1);
        }
        createMark(numWidth, num);
        numbers = numCont.find('.num');
        numbers.eq(0).css('left', 0);
        numbers.eq(current).addClass('active');
        numbers.eq(num-1).css('left', 'auto').addClass('last');
        slider.find('.pointer').draggable({
            axis: 'x',
            containment: "parent",
            grid: [step],
            stop: onStop
        });
        slider.on('click', '.num', numClick);
        $(window).on('resize', onResize);
        onResize();
    }

    function onResize(){
        var blWidth = bl.width(),
            winWidth = $(window).width();
        if (blWidth >= winWidth){
            pad = blWidth;
            return;
        }
        pad = Math.ceil((winWidth - blWidth) / 2) + blWidth;
        cont.height(pages.eq(current).height());
    }

    function onStop(ev, ui){
        var next = Math.round(ui.position.left / step);
        if (current === next){
            return;
        }
        switchPage(next);
    }

    function initTurn(el){
        var $this = $(el);
        $this.find('.item').each(function(){
            $(this).css({
                'transform': 'rotate('+(Math.random() * 12 - 6)+'deg)'
            });
        });
    }

    function numClick(e){
        e.preventDefault();
        var $this = $(this);
        if ($this.hasClass('active')){
            return;
        }
        switchPage(numbers.index(this));
    }

    function switchPage(next){
        var curPage = pages.eq(current),
            nextPage = pages.eq(next),
            status = nextPage.data('status');
        if (status == 0){
            nextPage.addClass('loading').data('status', 1);
            setTimeout(function(){
                $.ajax({
                    url: url,
                    data: {
                        'branch' : bl.data()['branch'],
                        'start' : next},
                    dataType: 'html',
                    success: function(data){
                        nextPage.html(data).removeClass('loading').data('status', 2);
                        initTurn(nextPage);
                        cont.height(nextPage.height());
                    }
                });
            }, 3000);
        }
        curPage.fadeOut(300, function(){
            $(this).removeClass('current');
            nextPage.fadeIn(300).addClass('current');
            cont.height(nextPage.height());
        });
        numbers.eq(current).removeClass('active');
        numbers.eq(next).addClass('active');
        cont.height(nextPage.height());
        pointer.css({
           left: step * next
        });
        current = next;
    }

})(jQuery);