/* Слайдер сецификаций */
(function($){
    var main = $('#p-detailed'),
        items,
        pageNums,
        current;
    if (!main.length){
        return;
    }

    init();

    function init(){
        var pager = main.find('.pager');
        items = main.children('.item');
        if (!items.length){
            return;
        }
        items.slice(1).hide();
        var item,
            doc = document.createDocumentFragment();
        for (var i = 0; i<items.length; i++){
            item = document.createElement('div');
            item.className = 'page-num';
            item.innerHTML = i+1;
            doc.appendChild(item);
        }
        pager.append(doc);
        pageNums = pager.children();
        current = 0;
        pageNums.first().addClass('active');
        pager.on('click', '.page-num', pageNumClick);
    }

    function pageNumClick(e){
        e.preventDefault();
        var $this = $(this);
        if ($this.hasClass('active')){
            return;
        }
        var n = pageNums.index(this);
        pageNums.eq(current).removeClass('active');
        items.eq(current).addClass('moving').fadeOut(200);
        $this.addClass('active');
        current = n;
        items.eq(n).fadeIn(200, animationStop);
    }

    function animationStop(){
        items.filter('.moving').removeClass('moving');
    }
})(jQuery);
