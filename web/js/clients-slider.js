(function ($){
    var rs = $('#roller-slider');
    if (!rs.length){
        return;
    }
    var itemsCount,
        items,
        cont,
        current = 0;

    init();

    function init(){
        cont = rs.find('.container');
        items = cont.find('.item');
        itemsCount = items.length;
        // Если элементов меньше трех
        switch (itemsCount){
            case 1:
                items.eq(0).addClass('center');
                cont.children('.arrow').hide();
                return;
                break;
            case 2:
                for (var i = 0; i < 2; i++){
                    cont.append(items.eq(i).clone(false));
                    items = cont.find('.item');
                    itemsCount = 4;
                }
                break;
        }
        rs.children('.arrow').on('click', arrowClick);
        items.eq(0).addClass('left');
        items.eq(1).addClass('center');
        items.eq(2).addClass('right');
    }

    function arrowClick(){
        var direction = ($(this).hasClass('left')) ? -1 : 1;
        move(direction);
    }

    function move(dir){
        var currents = [],
            temp,
            i,
            imax;
        if (dir < 0){
            i = -1;
            imax = 3;
        }else{
            i = 0;
            imax = 4;
        }
        for (i; i < imax; i++){
            currents.push(items.get((current + i) % itemsCount));
        }
        currents = $(currents);
        current += dir;
        if (current < 0){
            current = itemsCount - 1;
        }
        if (current >= itemsCount){
            current %= itemsCount;
        }
        switch (dir){
            case -1:
                currents.eq(0).addClass('left');
                currents.eq(1).removeClass('left').addClass('center');
                currents.eq(2).removeClass('center').addClass('right');
                currents.eq(3).removeClass('right');
                break;
            case 1:
                currents.eq(0).removeClass('left');
                currents.eq(1).removeClass('center').addClass('left');
                currents.eq(2).removeClass('right').addClass('center');
                currents.eq(3).addClass('right');
                break;
        }
        if (!Modernizr.csstransitions){
            return;
        }
        currents
            .queue(function(){
                $(this).addClass('moving').dequeue();
            })
            .delay(400)
            .queue(function(){
                $(this).removeClass('moving').dequeue();
            });
    }
})(jQuery);