// Слайдер CMS
(function($){
    var cms = $('#cms-slider');
    if (!cms.length){
        return;
    }
    var current = 0,
        nItem,
        stepWidth = 112,
        finalStep,
        cont = cms.find('.container'),
        text = cms.find('.text'),
        items;

    init();

    function init(){
        var itemsCont = cms.find('.items');
        items = cont.children('.item');
        if (!items.length){
            cms.find('.arrow').hide();
            itemsCont.hide();
            text.hide();
            return;
        }
        if (items.length === 1){
            cms.find('.arrow').hide();
            cont.css({left: stepWidth});
            items.addClass('active');
            setText(0);
            return;
        }
        var output = $(document.createDocumentFragment());
        if (items.length === 2){
            output.append(items.clone());
            output.append(items.eq(0).clone());
            finalStep = 2;
        }
        if (items.length > 2){
            output.append(items.slice(0,3).clone());
            finalStep = items.length;
        }
        nItem = 1;
        cont.append(output);
        items = cont.children();
        items.eq(1).addClass('active');
        setText(1);
        cms.on('click', '.arrow', arrowClick);
        cms.on('click', '.item', itemClick);
    }

    function arrowClick(){
        var next = ($(this).hasClass('left')) ? current - 1 : current + 1;
        moveCont(next);
    }

    function moveCont(n){
        if (n < 0){
            cont.css({left: stepWidth * finalStep * -1});
            n = finalStep - 1;
        }
        if (n > finalStep){
            cont.css({left: 0});
            n = 1;
        }
        items.eq(current + 1).removeClass('active');
        items.eq(n + 1).addClass('active');
        current = n;
        nItem = n + 1;
        cont.stop(true, false).animate({left: n * stepWidth * -1}, 200, animationDone);
    }

    function animationDone(){
        setText(current + 1);
    }

    function setText(n){
        text.html(items.eq(n).children('.hiddentext').html());
    }

    function itemClick(e){
        e.preventDefault();
        var n = items.index(this);
        moveCont(current + n - nItem);
    }
})(jQuery);