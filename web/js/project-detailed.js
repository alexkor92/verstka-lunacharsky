/* ���� �������� ������� */

(function($){
	var tabs = $('.label');
	var contents = $('.tab-content');

	tabs.each(function(){
        for (var j = 0; j < tabs.length; j++){
            if (j > 0){
                contents.eq(j).css("display", "none");
                tabs.eq(j).removeClass('active');
            } else {
                contents.eq(j).css("display", "block");
                $(this).addClass('active');
            }
            tabs.eq(j).children().children().html((j+1))
        }

		$(this).click(function(){
			for (var j = 0; j < tabs.length; j++){
				if (tabs.eq(j)[0] != $(this).context){
					contents.eq(j).css("display", "none");
					tabs.eq(j).removeClass('active');
				} else {
					contents.eq(j).css("display", "block");
					$(this).addClass('active');
				}
			}
		});
	});
	
})(jQuery);