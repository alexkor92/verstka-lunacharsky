// Показ popup
var popup = {};
(function($){
    var wrap = $('#wrap'),
        fwrap = $('#fwrap'),
        pop = $('#pop'),
        popbg = $('#popbg'),
        popups = $('.popup'),
        body = $('body');

    if (!popups.length){
        return;
    }

    popup.show = function(cl){
        var current = popups.filter('.visible');
        if (body.hasClass('popshown')){
            current.removeClass('visible');
        } else {
            wrap.add(fwrap).css({width: wrap.width()});
            body.addClass('popshown');
            popbg.fadeIn(200);
            pop.fadeIn(200);
        }
        popups.filter('.'+cl).addClass('visible');
    }

    popup.hide = function(e){
        if (e && e.target){
            e.preventDefault();
        }
        popbg.fadeOut(200);
        wrap.add(fwrap).css({width: 'auto'});
        popups.filter('.visible').removeClass('visible');
        pop.fadeOut(200);
        body.removeClass('popshown');
    }

    popup.update = function(cl, data){
        popups.filter('.'+cl).html(data);
    }

    function onResize(){
        if (!body.hasClass('popshown')){
            return;
        }
        body.removeClass('popshown');
        wrap.add(fwrap)
            .css({width: 'auto'})
            .css({width: wrap.width()});
        body.addClass('popshown');
    }

    popups.on('click', '.close', popup.hide);

})(jQuery);

// ресайз главной страницы на ie < 8
(function($){
    if (!$('html').hasClass('lt-ie8')){
        return;
    }
    var mainPage = $('#main-page');

    var resizeMainPage = function(){
        mainPage.children('.wrap').css('height', mainPage.height());
    }

    resizeMainPage();

    $(window).on('resize', resizeMainPage);
})(jQuery);

// Скрипты формы обратной связи
(function(){
    var loaded = false,
        pop = $('#pop').find('.callback'),
        url = pop.data('action');


    $('#cb-link').on('click', onLinkCkick);
    pop.on('submit', 'form', formSubmit);

    function onLinkCkick(e){
        if (e && e.target){
            e.preventDefault();
        }
        popup.show('loading');
        $.ajax({
            url: url,
            dataType: 'html',
            success: function(data){
                popup.update('callback', data);
                popup.show('callback');
            },
            error: function(){
                popup.hide();
            }
        });
    }

    function formSubmit(e){
        e.preventDefault();
        var data = $(this).serializeArray();
        popup.show('loading');
        setTimeout(function(){
            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function(data){
                    popup.update('callback', data);
                    popup.show('callback');
                },
                error: function(){
                    popup.show('callback');
                }
            });
        }, 1000);
    }
})(jQuery);

// Скрипты "чуда"
(function(){
    $('.footer-hands').on('click', 'a', function(e){
        e.preventDefault();
        popup.show('miracle');
    });
    $('.popup.miracle').children('.cancel').on('click', 'a', popup.hide);
})(jQuery);
