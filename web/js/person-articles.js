(function($){
    var perArticles,
        container,
        pages,
        pager,
        cWidth,
        pagerItems,
        current,
        previous,
        loading = false,
        heights = [];

    $(init);

    function init(){
        perArticles  = $('#person-articles');
        if (!perArticles.length){
            return;
        }
        container = perArticles.find('.items');
        cWidth = container.width();
        pages = container.children('.page').get();
        pager = perArticles.find('.person-pager');
        pagerItems = pager.find('a');
        current = pagerItems.index(pagerItems.filter('.active'));
        if (current < 0){
            current = 0;
            pagerItems.eq(current).addClass('active');
        }
        if (pagerItems.length === 1){
            pager.on('click', 'a', voidFunc);
            return;
        }
        if (current > 0){
            pages[current] = pages[0];
            pages[0] = false;
        }
        heights[current] = $(pages[current]).height();
        container.height(heights[current]);
        pager.on('click', 'a', pagerClick);
    }

    function pagerClick(e){
        e.preventDefault();
        if (loading){
            return false;
        }
        var $this = $(this);
        if ($this.hasClass('active')){
            return;
        }
        $this.addClass('active');
        pagerItems.eq(current).removeClass('active');
        var index = pagerItems.index(this);
        if (pages[index]){
            previous = current;
            current = index;
            swapPages();
        }else{
            loadPage($this.attr('href'), index);
        }
    }

    function loadPage(url, index){
        loading = true;
        $(pages[current]).addClass('moving').animate({
            left: (index > current) ? -1 * cWidth : cWidth
        }, 300, finishedMove);
        previous = current;
        current = index;
        container.addClass('loading');
        $.ajax({
            url: url,
            success: pageLoaded,
            error: pageError
        });
    }

    function pageLoaded(data){
        var el = document.createElement('div');
        el.className = 'page';
        container.append(el).removeClass('loading');
        var $el = $(el).html(data);
        pages[current] = el;
        heights[current] = $el.height();
        loading = false;
        container.animate({
            height: heights[current]
        }, 300);
        $el
            .addClass('moving')
            .css({
                left: (current > previous) ? cWidth : -1 * cWidth
            })
            .animate({
                left: 0
            }, 300, finishedMove);
    }

    function pageError(){
        pager.on('click', 'a', pagerClick);
        pager.off('click', 'a', voidFunc);
        $(pages[previous]).animate({
            left: 0
        }, 300, finishedMove);
    }

    function swapPages(){
        var curPage = $(pages[previous]).addClass('moving'),
            nextPage = $(pages[current]).addClass('moving');
        container
            .animate({
                height: heights[current]
            }, 300);
        if (previous > current){
            curPage
                .animate({
                    left: cWidth
                }, 300, finishedMove);
            nextPage
                .show()
                .css({
                    left: -1 * cWidth
                })
                .animate({
                    left: 0
                }, 300, finishedMove);
        } else {
            curPage
                .animate({
                    left: -1 * cWidth
                }, 300, finishedMove);
            nextPage
                .show()
                .css({
                    left: cWidth
                })
                .animate({
                    left: 0
                }, 300, finishedMove);
        }
    }

    function finishedMove(){
        var index = $.inArray(this, pages),
            $this = $(this).removeClass('moving');
        if (index !== current){
            $this.hide();
        }
    }
})(jQuery);