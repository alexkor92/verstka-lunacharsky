// скролл для flashback
(function($){
    var flash = $('#flashback');
    if (!flash.length){
        return;
    }
    var cont = flash.find('.container'),
        scale = flash.find('.scale'),
        years = {},// позиция по X на scale, шаг (container к scale)
        ykeys = [],
        itemW = 504,
        spanItems,
        curSpan = 0,
        pointer,
        limitScale, // максимальная точка учета позиции бегунка
        limitCont; // максимальная позиция для контейнера

    init();

    function init(){
        // cчитываем года и кол-во элементов на каждый
        var items = flash.find('.item'),
            last = items.first().data('year'),
            c = 0,
            datesBlock = flash.find('.dates'),
            spans = document.createDocumentFragment(),
            points = document.createDocumentFragment(),
            scaleW = scale.width(),
            step,
            createPoint = function(scaleX, cl){
                var el = document.createElement('div'),
                    classes = ['point'];
                if (cl){
                    classes.push(cl);
                } else {
                    el.style.left = scaleX + 'px';
                }
                el.className = classes.join(' ');
                points.appendChild(el);
            },
            createSpan = function(year, scaleX, cl){
                var el = document.createElement('span'),
                    classes = [];
                el.appendChild(document.createTextNode(year));
                el.setAttribute('data-year', year);
                if (cl){
                    classes.push(cl);
                } else {
                    el.style.left = scaleX + 'px';
                }
                el.className = classes.join(' ');
                spans.appendChild(el);
            };
        $.each(items, function(i, el){
            var y = $(el).data('year');
            if (last === y){
                c++;
                return;
            }
            ykeys.push(last);
            years[last] = {
                count: c
            };
            c = 1;
            last = y;
        });
        if (typeof years[last] === 'undefined'){
            ykeys.push(last);
            years[last] = {
                count: c
            }
        }
        last = parseInt(last) + 1;
        ykeys.push(last);
        years[last] = {
            count: 0
        }

        // Создаем точки на шкале и лэйблы для годов
        c = ykeys.length; // кол-во меток годов
        step = (c > 2) ? Math.round( scaleW / (c-1) ) : scaleW;
        createPoint(0, 'first');
        createSpan(ykeys[0], 0, 'first');
        years[ykeys[0]].scaleX = years[ykeys[0]].contX = 0;

        if (c > 2){
            var goneSum = 0;
            for (var i = 1; i < c-1; i++){
                years[ykeys[i]].scaleX = step * i - 3;
                goneSum += years[ykeys[i-1]].count*itemW
                years[ykeys[i]].contX = goneSum;
                createPoint(step * i - 3);
                createSpan(ykeys[i], step * i - 19);
            }
        }
        createPoint(c - 1, 'last');
        createSpan(ykeys[c-1], 0, 'last');
        years[ykeys[c - 1]].scaleX = scaleW;
        datesBlock.append(spans);
        spanItems = datesBlock.children('span');
        spanItems.eq(curSpan).addClass('active');
        scale.append(points);
        //
        if (items.length < 3){
             return;
        }
        // вычисляем относительный шаг (cont / scale)
        for (i = 0; i < c; i++){
            years[ykeys[i]].step = years[ykeys[i]].count * itemW / step;
        }
        
        c = (c > 2) ? c-2 : c = 0;
        limitScale = years[ykeys[c]].scaleX + step / years[ykeys[c]].count * ( years[ykeys[c]].count - 2 );
        limitCont = years[ykeys[c]].contX + itemW * ( years[ykeys[c]].count - 2 );

        datesBlock.on('click', 'span', spanClick);
        pointer = flash.find('.pointer');
        pointer.draggable({
                axis: 'x',
                containment: "parent",
                drag: onDrag,
                stop: onStop
            });
    }

    function onDrag(e, ui){
        var i = 0;
        while (ui.position.left >= years[ykeys[i]].scaleX){
            i++;
        }
        i--;
        if (i !== curSpan){
            spanItems.eq(curSpan).removeClass('active');
            curSpan = i;
            spanItems.eq(curSpan).addClass('active');
        }
        moveCont(getStep(ui.position.left));
    }

    function onStop(e, ui){
        moveCont(getStep(ui.position.left));
    }

    function moveCont(x){
        cont.css({marginLeft: x * -1});
    }

    function getStep(x){
        var l = ykeys.length;
        if ( x > limitScale ) return limitCont;
        for (var i = 0; i < ykeys.length; i++){
            if (x < years[ykeys[i]].scaleX){
                i--;
                return years[ykeys[i]].step * (x - years[ykeys[i]].scaleX) + years[ykeys[i]].contX;
            }
        }
    }
    function spanClick(){
        var $this = $(this),
            index,
            pos;
        if ($this.hasClass('first')){
            pos = 0;
            index = 0;
        }
        else if ($this.hasClass('last')){
            pos = scale.width() - pointer.width();
            index = spanItems.length - 2;
        }
        else {
            index = spanItems.index(this);
            pos = years[ykeys[index]].scaleX;
        }
        spanItems.eq(curSpan).removeClass('active');
        curSpan = index;
        spanItems.eq(curSpan).addClass('active');
        pointer.css({left: pos});
        moveCont(getStep(pos));
    }
})(jQuery);