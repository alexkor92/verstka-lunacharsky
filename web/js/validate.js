(function($, window, document, undefined){

    var pluginName = 'lunyValidate',
        defaults = {
            submit: function(){}
        },
        supportRequired = !('required' in document.createElement('input'));

    function isRequired(){
        return (this.hasAttribute('required') && this.getAttribute('required') !== 'false' && $.inArray(this.type, ['submit','reset']) == -1);
    }

    function isFilled(){
        if ($(this).val() === ''){
            this.className = 'error';
        }else{
            this.className = '';
        }
    }

    function Plugin(element, options){
        this.element = element;
        this.$el = $(element);
        this.settings = $.extend( {}, defaults, options );
        this.init();
    }

    Plugin.prototype = {
        init: function(){
            this.required = this.$el.find('input, textarea, select').filter(isRequired);
            this.required.on('change', isFilled);
            if (!supportRequired){
                this.$el.on('submit', this.settings.submit);
                return { initialised: true };
            }
            this.error = false;
            this.$el.on('submit', {object: this}, this.check);
            return { initialised: true };
        },
        check: function(e){
            var object = e.data.object;
            object.required.each(isFilled);
            object.error = object.required.hasClass('error');
            if (object.error){
                e.preventDefault();
            }else{
                object.settings.submit(e);
            }
        }
    };

    $.fn[pluginName] = function(options){
        this.each(function() {
            if ( !$.data( this, "plugin_" + pluginName ) ) {
                $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
            }
        });
        return this;
    }
})(jQuery, window, document);