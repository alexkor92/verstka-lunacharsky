(function($){
    var main = $('#clients-gloss');

    if (!main.length){
        return;
    }
    var menu = main.find('.cg-menu tr'),
        lang = menu.find('.lang'),
        tds = menu.children().not(lang),
        items = main.find('.items'),
        list = lang.find('.list'),
        langMethods = {
            open: function(){
                if (lang.hasClass('opened')){
                    return;
                }
                lang.addClass('opened');
                $(window).on('click', langMethods.windowClick);
            },
            close: function(){
                lang.removeClass('opened');
                $(window).off('click', langMethods.windowClick);
            },
            change: function(lang){
                tds.not('.hidden').addClass('hidden');
                tds.filter('.'+lang).removeClass('hidden');
                langMethods.close();
            },
            onRootClick: function(e){
                e.preventDefault();
                e.stopPropagation();
                langMethods.open();
            },
            onItemClick: function(e){
                e.preventDefault();
                e.stopPropagation();
                var $this = $(this);
                if (!$this.hasClass('active')){
                    langMethods.change($this.data('lang'));
                    list.find('.active').removeClass('active');
                    $this.addClass('active');
                    lang.find('.text').html($this.html());
                }
                langMethods.close();
            },
            windowClick: function(e){
                if (!lang.has(e.target).length && !lang.is(e.target)){
                    langMethods.close();
                }
            }
        },
        contentMethods = {
            onClick: function(e){
                e.preventDefault();
                var $this = $(this);
                if ($this.hasClass('current') || $this.hasClass('empty')){
                    return;
                }
                menu.find('.current').removeClass('current');
                $this.addClass('current');
                contentMethods.load($this.attr('href'));
            },
            load: function(url){
                items.addClass('loading');
                setTimeout(function(){
                    $.ajax({
                        url: url,
                        dataType: 'html',
                        success: contentMethods.onLoad,
                        complete: contentMethods.onComplete
                    });
                }, 2000);
            },
            onLoad: function(data){
                var height = {
                    old: items.height(),
                    cur: 0
                };
                items.css({
                    height: 'auto'
                });
                items.html(data);
                height.cur = items.height();
                if (height.cur < 190) height.cur = 190;
                items
                    .height(height.old);
                if (height.cur != height.old){
                    items
                        .animate({
                            height: height.cur
                        }, 200);
                }
            },
            onComplete: function(){
                items.removeClass('loading');
            }
        };

    lang.children('.current-lang').on('click', langMethods.onRootClick);
    list.on('click', 'a', langMethods.onItemClick);
    menu.on('click', 'a', contentMethods.onClick);
    items.height(items.height());
})(jQuery);