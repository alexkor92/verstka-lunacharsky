(function($){
    var perSlider = $('#sites-slider');

    if (!perSlider.length){
        return;
    }

    var pageNum = perSlider.data('pagenum'),
        url = perSlider.data('url'),
        arrows = perSlider.find('.arrow'),
        container = perSlider.find('.container'),
        pages = [],
        maxHeight = 0,
        lastPerson,
        prev = -1,
        current = 0;

    init();

    function init(){
        var tmp = perSlider.find('.page'),
            el,
            i;
        // Обрабатываем уже загруженные страницы
        if (tmp.length){
            tmp.each(function(index){
                pages[index] = {
                    el: this,
                    loaded: true
                };
                treatPage(index);
            });
            for (i = tmp.length; i < pageNum; i++){
                el = document.createElement('div');
                el.className = 'page loading';
                container.append(el);
                pages[i] = {
                    el: el,
                    loaded: false
                };
                $(el).hide();
            }
        }
        else{
            for (i = 0; i < pageNum; i++){
                el = document.createElement('div');
                el.className = 'page loading';
                container.append(el);
                pages[i] = {
                    el: el,
                    loaded: false
                };
                $(el).hide();
            }
            loadPage(current);
        }

        if (!pageNum || pageNum < 2){
            arrows.hide();
        }
        else{
            arrows.on('click', arrowClick);
        }
    }


    function arrowClick(){
        var $this = $(this),
            curEl = pages[current],
            nextEl,
            next = ($this.hasClass('left')) ? current - 1 : current + 1;
        if (next >= pageNum) next = 0;
        if (next < 0 ) next = pageNum - 1;
        $(curEl.el)
            .addClass('moving')
            .animate({
                left: (next > current) ? '-970px' : '970px'
            }, 300, removeMoving);
        nextEl = pages[next];
        $(nextEl.el)
            .addClass('moving')
            .css({
                left: (next > current) ? '970px' : '-970px'
            })
            .show()
            .animate({
                left: 0
            },300, removeMoving);

        if (!nextEl.loaded){
            loadPage(next);
        }
        else
        {
            container.animate({
                height: $(nextEl.el).height()
            }, 300);
        }
        prev = current;
        current = next;
    }

    // загрузка страницы слайдера
    function loadPage(id){
        $.ajax({
            url: url+'/'+id,
            dataType: 'html',
            type: 'GET',
            success: function(data){
                onLoadSuccess(data, id);
            }
        });
    }

    function onLoadSuccess(data, id){
        var page = pages[id];
        $(page.el).html(data).show().removeClass('loading');
        page.loaded = true;
        treatPage(id);
    }

    function treatPage(id){
        var page = pages[id],
            $this = $(page.el),
            height = $this.height();
        if (id !== current){
            $this.hide();
            return;
        }
        if (height > 0 && height < maxHeight) {
            maxHeight = $this.height();
            container.animate({
                height: height
            }, 300);
        }
        $this.addClass('moving')
            .css({
                left: (prev > current) ? '-970px' : '970px'
            }).animate({
                left: 0
            },300, removeMoving);
    }

    function removeMoving(){
        var $this = $(this),
            height = $this.height();
        if (height > maxHeight) {
            maxHeight = $this.height();
            container.animate({
                height: height
            }, 100);
        }
        if (!$this.is(pages[current].el)){
            $this.hide();
        }
        $this.removeClass('moving');
    }
})(jQuery);