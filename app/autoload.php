<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';

//$loader->add(array(
//'Knp\\Component'      => __DIR__.'/../vendor/knp-components/src',
//'Knp\\Bundle'         => __DIR__.'/../vendor/bundles'
//));
$loader->add('External\Petrovich', __DIR__.'/../vendor/external/petrovich/petrovich');

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
